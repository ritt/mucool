/********************************************************************\

  Name:         decay_ana_module.c
  Created by:   Thomas Prokscha
  Date:		01-Apr-2007

  Contents:     Analyzer module for muon decayhistograms

\********************************************************************/

/*-- Include files -------------------------------------------------*/

/* standard includes */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

/* midas includes */
#include "midas.h"
#include "v1190.h"
#include "experim.h"
#include "mucool_experim.h"

#ifdef HAVE_HBOOK
#include <cfortran.h>
#include <hbook.h>
#else
/* root includes */
#include <TH1F.h>
#include <TH2F.h>
#include <TTree.h>
#endif

/*-- ODB Parameters ------------------------------------------------*/
//extern RUNINFO	runinfo;
//extern EXP_PARAM	exp_param;
extern RUNINFO          runinfo;
extern TRIGGER_SETTINGS trigger_settings;
extern T0SHIFT_PARAM    t0shift_param;
DECAYANAMODULE_PARAM	decay_ana_param;
DECAYANAMODULE_PARAM_STR(decay_ana_param_str);

/*-- Module declaration --------------------------------------------*/
INT decay_init(void);
INT decay_ana(EVENT_HEADER*,void*);
INT decay_bor(INT run_number);
INT decay_eor(INT run_number);

ANA_MODULE decay_ana_module = {
  "DecayAnaModule",               /* module name           */
  "Thomas Prokscha",              /* author                */
  decay_ana,                      /* event routine         */
  decay_bor,                      /* BOR routine           */
  decay_eor,                      /* EOR routine           */
  decay_init,                     /* init routine          */
  NULL,                           /* exit routine          */
  &decay_ana_param,               /* parameter structure   */
  sizeof(decay_ana_param),        /* structure size        */
  (char **)decay_ana_param_str,   /* initial parameters    */
};

/*-- other global variables ----------------------------------------*/
//#define N_DECHIST  16
//tjp #ifndef HAVE_HBOOK
static TH1F* hDecHist[2*N_DECAY_HISTS+13];
static TH2F* CoincChan2[2] ;
static int T1indexN;
static int T2indexN;
static int T1index;
static int T2index;
//the following objects are from mana.c
extern TFolder   *gManaHistosFolder;
extern TObjArray *gHistoFolderStack;
//tjp #endif

static DWORD dataWindowMuonDecay;  //needed for post-pileup rejection

/*-- INIT routine --------------------------------------------------*/
INT decay_init(void)
/*!
 * <p> Function <b>decay_init</b>:
 *
 * <p> Creates decay histograms. Gets title and bin information from ODB.
 *
*/
{
extern char runname[256];
char title[256], name[256];
int   nbin, i;
float xlow, xhigh;

  T1indexN = 0;
  T2indexN = 0;
  T1index = 0;
  T2index = 0;
    
  for (i=0; i<2*N_DECAY_HISTS; i++){
    if ( i > N_DECAY_HISTS-1)
     sprintf(name,  "hDecay%03d", i+1+N_OFFSET_PPC_HISTOGRAMS-N_DECAY_HISTS); //histograms with cuts: offset in name
    else
     sprintf(name,  "hDecay%03d", i+1);
    sprintf(title, "%s Run %s_%04d", decay_ana_param.histotitles.titles[i],
            runname, runinfo.run_number);
    nbin  = decay_ana_param.histobinning.histonbin[i];
    xlow  = decay_ana_param.histobinning.histoxlow[i];
    xhigh = decay_ana_param.histobinning.histoxup[i];
    hDecHist[i] = H1_BOOK(name, title, nbin, xlow, xhigh);
    // special histogram for coincidence of T1 and T2.  Do it the easy way for now; add to database later?
    if ((i < N_DECAY_HISTS) && (T1indexN == 0) &&
          (title[0] == 'T' && title[1] == '1')) {
          //if ((i < N_DECAY_HISTS) && (decay_ana_param.histotitles.titles[i][0:1] == "T1")) {
          sprintf(title, "T1 with T2 Coincidence Run %s_%04d", runname, runinfo.run_number);
          hDecHist[2*N_DECAY_HISTS] = H1_BOOK("T1T2Coin", title, nbin, xlow, xhigh);
          T1indexN = i+POS01;
    } else if ((i < N_DECAY_HISTS) && (T2indexN == 0) &&
                 (title[0] == 'T' && title[1] == '2')) {
          // if ((i < N_DECAY_HISTS) && (decay_ana_param.histotitles.titles[i][0:1] == "T2")) {
          sprintf(title, "T2 with T1 Coincidence Run %s_%04d", runname, runinfo.run_number);
          hDecHist[2*N_DECAY_HISTS+1] = H1_BOOK("T2T1Coin", title, nbin, xlow, xhigh);
          sprintf(title, "T2 - T1  Run %s_%04d", runname, runinfo.run_number);
          hDecHist[2*N_DECAY_HISTS+2] = H1_BOOK("T2T1Diff", title, 101, -50.5, 50.5);
          T2indexN = i+POS01;
    } else if ((i > N_DECAY_HISTS-1) && (T1index == 0) &&
            (title[0] == 'T' && title[1] == '1')) {
            //if ((i < N_DECAY_HISTS) && (decay_ana_param.histotitles.titles[i][0:1] == "T1")) {
            sprintf(title, "T1 with T2 Coincidence No Pileup Run %s_%04d", runname, runinfo.run_number);
            hDecHist[2*N_DECAY_HISTS+3] = H1_BOOK("T1T2CoinNP", title, nbin, xlow, xhigh);
            T1index = i+POS01 - N_DECAY_HISTS;
    } else if ((i > N_DECAY_HISTS-1) && (T2index == 0) &&
                   (title[0] == 'T' && title[1] == '2')) {
            // if ((i < N_DECAY_HISTS) && (decay_ana_param.histotitles.titles[i][0:1] == "T2")) {
            sprintf(title, "T2 with T1 Coincidence No Pileup Run %s_%04d", runname, runinfo.run_number);
            hDecHist[2*N_DECAY_HISTS+4] = H1_BOOK("T2T1CoinNP", title, nbin, xlow, xhigh);
            sprintf(title, "T2 - T1  Run %s_%04d", runname, runinfo.run_number);
            hDecHist[2*N_DECAY_HISTS+5] = H1_BOOK("T2T1DiffNP", title, 101, -50.5, 50.5);
            T2index = i+POS01 - N_DECAY_HISTS;
    }

  }
    sprintf(title, "Entrance Det Pileup Time  Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+7] = H1_BOOK("PileupTimeEnt", title, nbin, xlow, xhigh);
    sprintf(title, "Scint Pileup Time  Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+8] = H1_BOOK("PileupTimeScint", title, nbin, xlow, xhigh);
    sprintf(title, "Scint Pileup Time Diff Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+9] = H1_BOOK("PileupTimeDiff", title, nbin, xlow, xhigh);
    sprintf(title, "Scint Coincidence Time Diff Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+12] = H1_BOOK("CoincTimeDiff", title, 2*nbin+1, -xhigh, xhigh);
   
    
    sprintf(title, "Pileup Channel  Run %s_%04d", runname, runinfo.run_number);
    xhigh = (float) N_DECAY_HISTS + 0.5;
    hDecHist[2*N_DECAY_HISTS+6] = H1_BOOK("PileupChan", title, N_DECAY_HISTS+1, -0.5, xhigh);
    sprintf(title, "Pileup Channel  Run %s_%04d", runname, runinfo.run_number);
    xhigh = (float) N_DECAY_HISTS + 0.5;
    hDecHist[2*N_DECAY_HISTS+6] = H1_BOOK("PileupChan", title, N_DECAY_HISTS+1, -0.5, xhigh);
    
    sprintf(title, "Number of Scint 1st Hits Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+10] = H1_BOOK("NScintHits", title, N_DECAY_HISTS, -0.5, xhigh);
    sprintf(title, "Coincidence Channels  Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+11] = H1_BOOK("CoincChan", title, N_DECAY_HISTS+1, -0.5, xhigh);
    CoincChan2[1] = H2_BOOK("CoincChan2", title,N_DECAY_HISTS+1, -0.5, xhigh,N_DECAY_HISTS+1, -0.5, xhigh);
    sprintf(title, "Multi Scint Hit Channels  Run %s_%04d", runname, runinfo.run_number);
    CoincChan2[0] = H2_BOOK("MultHitChan2", title,N_DECAY_HISTS+1, -0.5, xhigh,N_DECAY_HISTS+1, -0.5, xhigh);

    
  dataWindowMuonDecay = 0;
  
  return SUCCESS;
}

/*-- BOR routine ---------------------------------------------------*/

INT decay_bor(INT run_number)
/*!
 * <p> Function <b>decay_bor</b>:
 *
 * <p> Changes titles of decay histograms: <br>
 *     overwrites runname and run_number
 *
*/
{
int i;
char title[256];
extern char runname[256];

  for (i=0; i<2*N_DECAY_HISTS; i++){
    sprintf(title, "%s Run %s_%04d", decay_ana_param.histotitles.titles[i], 
            runname, runinfo.run_number);
    hDecHist[i]->SetTitle(title);
  }
    if (T1indexN > 0) {
        sprintf(title, "T1 with T2 Coincidence Run %s_%04d", runname, runinfo.run_number);
        hDecHist[2*N_DECAY_HISTS]->SetTitle(title);
    }
    if (T2indexN > 0) {
        sprintf(title, "T2 with T1 Coincidence Run %s_%04d", runname, runinfo.run_number);
        hDecHist[2*N_DECAY_HISTS+1]->SetTitle(title);
        sprintf(title, "T2 - T1  Run %s_%04d", runname, runinfo.run_number);
        hDecHist[2*N_DECAY_HISTS+2]->SetTitle(title);
    }
    if (T1index > 0) {
        sprintf(title, "T1 with T2 Coincidence No Pileup Run %s_%04d", runname, runinfo.run_number);
        hDecHist[2*N_DECAY_HISTS+3]->SetTitle(title);
    }
    if (T2index > 0) {
        sprintf(title, "T2 with T1 Coincidence No Pileup Run %s_%04d", runname, runinfo.run_number);
        hDecHist[2*N_DECAY_HISTS+4]->SetTitle(title);
        sprintf(title, "T2 - T1  Run %s_%04d", runname, runinfo.run_number);
        hDecHist[2*N_DECAY_HISTS+5]->SetTitle(title);
    }
    sprintf(title, "Pileup Channel  Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+6]->SetTitle(title);
    sprintf(title, "Entrance Det Pileup Time  Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+7]->SetTitle(title);
    sprintf(title, "Scint Pileup Time  Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+8]->SetTitle(title);
    sprintf(title, "Scint Pileup Time Diff Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+9]->SetTitle(title);
    sprintf(title, "Number of Scint 1st Hits Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+10]->SetTitle(title);
    sprintf(title, "Coincidence Channels  Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+11]->SetTitle(title);
    CoincChan2[1]->SetTitle(title);
    sprintf(title, "Scint Coincidence Time Diff Run %s_%04d", runname, runinfo.run_number);
    hDecHist[2*N_DECAY_HISTS+12]->SetTitle(title);
    sprintf(title, "Multi Scint Hit Channels  Run %s_%04d", runname, runinfo.run_number);
    CoincChan2[0]->SetTitle(title);
    
  // get data window for post-pileup rejection from ODB
  dataWindowMuonDecay = trigger_settings.data_window; 
  
  return SUCCESS;
}

/*-- EOR routine ---------------------------------------------------*/

INT decay_eor(INT run_number)
{

  return SUCCESS;
}

/*-- event routine -------------------------------------------------*/
/*!
 * <p> Function <b>decay_ana</b>:
 *
 * <p> locates TDC0 bank, fills decay time histograms
 *
*/
INT decay_ana(EVENT_HEADER *pheader, void *pevent)
{
INT       n, i, j, channel, counter[N_TDC_CHANNELS];
INT	  masterCH;
DWORD     *ptdc, tdc_data[N_TDC_CHANNELS][DATA_N_HITS];
BOOL	  foundM;      /* flag indicating that we got a start signal */
LEM_DATA  *lem_data;
INT        difftime_last;
int HitChan[N_DECAY_HISTS];
    
  /* look for TDC0 bank */
  n = bk_locate(pevent, "TDC0", &ptdc);
  if (n == 0 )
    return 1;

  // return if we don't have the NEW_EVENT_FLAG present
  if ( !(ptdc[0] & NEW_EVENT_MASK)) return 1;

  // return if EVENT_0_TYPE (Muons on MCP1)
  if ( ptdc[0] & (EVENT_0_TYPE<<16) ) return 1;

  masterCH    = MCOUNTER;
  foundM      = FALSE;
  memset(&counter, 0x00, sizeof(counter));
  memset(&tdc_data, 0x00, sizeof(tdc_data));

  for (i = 1; i < n; i++){ // i=0 is a control word, including the event length
    lem_data = (LEM_DATA *) &ptdc[i];
    channel    = lem_data->channel;
    if ( channel > N_TDC_CHANNELS-1 ) continue; //not understood how this can happen, but it can happen
                                  //after a HV break at the moderator (all channel get "many" hits, like
				  //a "flash" in the detectors
    tdc_data[channel][counter[channel]] = lem_data->data;
    counter[channel]++;

    switch ( channel ){
        case MCOUNTER:
         foundM = TRUE;
         break;

	default:
	 break;
    }
  }

  if ( !foundM ) return SUCCESS;
    
    int NScintHits = 0;

  // fill decay histograms without further cut
  for (i = POS01; i < N_DECAY_HISTS+POS01; i++){
    if (tdc_data[i][0] > 0){
     difftime_last = tdc_data[i][0] - tdc_data[masterCH][0];
     hDecHist[i-POS01]->Fill( (float) difftime_last + t0shift_param.deltat_t0[i-POS01], 1.);
     HitChan[NScintHits++] = i;
    }
  }
// characterize coincidences; fix displaced channel with (HitChan[i]==29? 19: HitChan[i])
  hDecHist[2*N_DECAY_HISTS+10]->Fill( (float) NScintHits, 1.0);
    if (NScintHits>1) {
        hDecHist[2*N_DECAY_HISTS+11]->Fill( (float) (HitChan[0]==29? 19: HitChan[0]) , 1.0);
    
      for (i=1; i<NScintHits; i++) {
          hDecHist[2*N_DECAY_HISTS+11]->Fill( (float) (HitChan[i]==29? 19: HitChan[i]) , 1.0);
          for (j=0; j<i; j++) {
              difftime_last = tdc_data[i][0] + t0shift_param.deltat_t0[HitChan[i]-POS01]
                - tdc_data[j][0] - t0shift_param.deltat_t0[HitChan[j]-POS01];
              
              hDecHist[2*N_DECAY_HISTS+12]->Fill((float) difftime_last, 1.0 );
              
              if (difftime_last) {  // fill coincidence channels plots
                  if (HitChan[i]==29 && (HitChan[j] > 19) )
                      CoincChan2[1]->Fill(19. ,(float) (HitChan[j]==29? 19: HitChan[j]) ,   1.0);
                  else
                       CoincChan2[1]->Fill((float) (HitChan[j]==29? 19: HitChan[j]),
                                      (float) (HitChan[i]==29? 19: HitChan[i]) , 1.0);
             } else {   //multi hits at different times so fill multihit plots
                 if (HitChan[i]==29 && (HitChan[j] > 19) )
                     CoincChan2[0]->Fill(19. ,(float) (HitChan[j]==29? 19: HitChan[j]) ,   1.0);
                 else
                     CoincChan2[0]->Fill((float) (HitChan[j]==29? 19: HitChan[j]),
                                         (float) (HitChan[i]==29? 19: HitChan[i]) , 1.0);
              }
          }
      }
    }
    
  // special histograms for T1 T2 coincidence
  if ((T1indexN > 0) && (T2indexN > 0) && (tdc_data[T1indexN][0] > 0) && (tdc_data[T2indexN][0] > 0)) {
        difftime_last = tdc_data[T1indexN][0] - tdc_data[masterCH][0];
        hDecHist[2*N_DECAY_HISTS]->Fill( (float) difftime_last + t0shift_param.deltat_t0[T1indexN-POS01], 1.);
        difftime_last = tdc_data[T2indexN][0] - tdc_data[masterCH][0];
        hDecHist[2*N_DECAY_HISTS+1]->Fill( (float) difftime_last + t0shift_param.deltat_t0[T2indexN-POS01], 1.);
        hDecHist[2*N_DECAY_HISTS+2]->Fill( (float) tdc_data[T2indexN][0] - tdc_data[T1indexN][0], 1.);
  }

  // check for post-pileup in masterCH: if post-pileup, return
  if ( tdc_data[masterCH][1] != 0 && 
      (tdc_data[masterCH][1] - tdc_data[masterCH][0]) < dataWindowMuonDecay) {
      difftime_last = tdc_data[masterCH][1] - tdc_data[masterCH][0];
      hDecHist[2*N_DECAY_HISTS+7]->Fill( difftime_last, 1.0);
      hDecHist[2*N_DECAY_HISTS+6]->Fill( 0., 1.);
      return SUCCESS;
  }
  // special histograms for T1 T2 coincidence
  if ((T1index > 0) && (T2index > 0) && (tdc_data[T1index][0] > 0) && (tdc_data[T2index][0] > 0)) {
        difftime_last = tdc_data[T1index][0] - tdc_data[masterCH][0];
        hDecHist[2*N_DECAY_HISTS+3]->Fill( (float) difftime_last + t0shift_param.deltat_t0[T1index-POS01], 1.);
        difftime_last = tdc_data[T2index][0] - tdc_data[masterCH][0];
        hDecHist[2*N_DECAY_HISTS+4]->Fill( (float) difftime_last + t0shift_param.deltat_t0[T2index-POS01], 1.);
        hDecHist[2*N_DECAY_HISTS+5]->Fill( (float) tdc_data[T2index][0] - tdc_data[T1index][0], 1.);
    }

  
  // check, if we had only 1 e+ detector fired, and that for this detector there was
  // no 2nd e+ during data window
    // TJP 11-Dec-2014: this is not what the code below does.  It makes histograms until it finds a channel with a second
    // hit and then it bails out, so the rest of the channels do not get filled.  It is rare for any channels except the
    // muon counter to have a second hit, so this probably has little effect unless a channel is oscillating or double hitting.

  for (i = POS01; i < N_DECAY_HISTS+POS01; i++){
    if(tdc_data[i][0] > 0){
     if (tdc_data[i][1] != 0){
         if (tdc_data[i][1] - tdc_data[masterCH][0] < dataWindowMuonDecay) {
             difftime_last = tdc_data[i][1] - tdc_data[masterCH][0];
             hDecHist[2*N_DECAY_HISTS+8]->Fill( difftime_last, 1.0);
             difftime_last = tdc_data[i][1] - tdc_data[i][0];
             hDecHist[2*N_DECAY_HISTS+9]->Fill( difftime_last, 1.0);
             hDecHist[2*N_DECAY_HISTS+6]->Fill( (float) (i-POS01+1) , 1.);
             
             // disable veto 16 Dec 2014 TJP
             // return SUCCESS;
         }
     }
     hDecHist[i-POS01+N_DECAY_HISTS]->Fill( (float) (tdc_data[i][0] - tdc_data[masterCH][0]) + t0shift_param.deltat_t0[0], 1.);
    }
  }

  return SUCCESS;
}
/* ----------------------------------------------------------------------------*\
	EOF decay_ana_module.c
\* ----------------------------------------------------------------------------*/
