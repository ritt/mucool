### Startup ###

* Log in to lem01.psi.ch with `$ ssh -X MuCool@lem01.psi.ch1`
* Check if "mhttpd" is running via `ps aux | grep mhttpd`
* If not running, start mhttpd via `mhttpd -D`
* Connect your browser to http://lem01.psi.ch

### Start a run ###

* Go to `Programs` page on MIDAS web site
* Start VME_FE if not running
* Start Analyzer if not running
* Start Logger if not running
* Start run from the status page

### Monitoring ###

* Check scalers by clicking on "Scaler" link on Status Page, or go to `Scaler` page at top menu
* `Ip` should be proton current
* `10kHzclock` should show ~30000 since the 10 kHz clock is read every 3 seconds
* `MCounter` is the muon entry counter
* `Pos01` - `Pos29` are positron counters
* RATE further down on the scaler page are normalized rates in Hz.
* SRAT are scaler rates normalized to proton current
* SSUM are scaler sums for the current run

### Viewing histograms ###

* Histograms are stored at `/data/MuCool/his/2014`, the file name contains the run number 
* Histograms ending with a numeric extension are saved every 5 minutes, histograms ending with .root are accumulated during the whole run
* To view a histo, open with with root: `root mucool14_his_0001.root`
* Open the browser in root: `root [1] TBrowser t`
* Navigate to the histo under `/data/MuCool/his/2014/mucool14_his_0001.root/histos;1/DecayAnaModule` and double click it

### Constant Fraction Discriminators ###

* The thresholds of the 4 x 8 channel CFD is shown on the MIDAS _CFD_ page, but the values cannot be changed there.
* To change the thresholds, follow this receipe:
    - Log on to lem01 (icon on desktop on 'muon' computer in counting house)
    - Start msc:
        * `[MuCool@lem01 ~]$ mscb/msc -d mscb267`
    - Connect to CFD (#1: channels 0-7, #2: channel 8-15, #3: channels 16-23, #4: channel 34-31): 
        * `> ping 1`
    - Check current seetings : 
        * `node1(0x1)> read`
    - Change threshold: 
        * `node1(0x1)> write 11 100` where _11_ is the variable belonging to the threshold of channel 1. For channel two use 12, for channel thee use 13 and so on.
    - Propagate change to EEPROM and hardware: 
        * ` node1(0x1)> flash` 
        * `node1(0x1)> reboot`
    - Change should also be visible on the MIDAS 'CFD' page
* The manual of the CFD can be found [here](https://bytebucket.org/ritt/mucool/raw/6854781802fbdc60190749185d934d947e41ffe6/manuals/CFD950VME.pdf)

### Extracting history data ###

History data (pressures, HVs, ...) can be extracted via the `mhist` tools, which has to be executed in the history directory `/data/MuCool/history`:

```
[MuCool@lem01 ~]$ cd /data/MuCool/history/
[MuCool@lem01 history]$ mhist
Available experiments on local computer:
0 : mucool
1 : nemu
2 : lemplug
Select number: 0
Available events:
0: System
1: HV
2: CFD
3: Pressures

Select event: 3

Available variables:
0: P1
1: P2
2: P3
3: P4
4: P5
5: P6

Select variable (0..5,-1 for all): -1

How many hours: 1

Interval [sec]: 600

0: P1
1: P2
2: P3
3: P4
4: P5
5: P6
Number of entries for event "Pressures" variable "P1" index 0 is 6 instead of 6
Number of entries for event "Pressures" variable "P2" index 0 is 6 instead of 6
Number of entries for event "Pressures" variable "P3" index 0 is 6 instead of 6
Number of entries for event "Pressures" variable "P4" index 0 is 6 instead of 6
Number of entries for event "Pressures" variable "P5" index 0 is 6 instead of 6
Number of entries for event "Pressures" variable "P6" index 0 is 6 instead of 6
Event 'Pressures', 6 variables, 6 entries
Time 	P1 	P2 	P3 	P4 	P5 	P6 	
Dec  7 12:06:45 2014		1038	1027	1126	10.71000003814697	10.55000019073486	1065
Dec  7 12:16:45 2014		1040	1027	1124	10.71000003814697	10.55000019073486	1065
Dec  7 12:26:45 2014		1039	1028	1127	10.71000003814697	10.55000019073486	1064
Dec  7 12:36:47 2014		1039	1026	1125	10.71000003814697	10.55000019073486	1065
Dec  7 12:46:47 2014		1038	1026	1125	10.71000003814697	10.55000019073486	1065
Dec  7 12:56:48 2014		1038	1027	1127	10.71000003814697	10.55000019073486	1064
```