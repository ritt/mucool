/********************************************************************\

  Name:         analyzer.c
  Created by:   Thomas Prokscha,        14-May-2004
                Andreas Suter,          08-March-2012     Adopted to MusrRoot
                Zaher Salman            18-March-2012     Aded LEM spin rotator

  Contents: System part of Analyzer code for LEM experiment.
                Sep/2006: save histograms every
                "HISTOGRAM_WRITE_PERIOD" s (300sec at the moment).

                The module creates the /Info structure in ODB that contains
                important information for the LEM experiment.
                Additional functions to write histogram files, histograms
                file header generation:

                write_histogram_file();
                get_sum_filename(char *filename);
                extract_summary_data();  // from .summ files
                update_run_header();

********************************************************************/

#include <vector>
using namespace std;

// standard includes
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

// midas includes
#include "midas.h"
#include "experim.h"
#include "mucool_experim.h"

// cernlib includes
#ifdef OS_WINNT
#define VISUAL_CPLUSPLUS
#endif
#ifdef __linux__
#define f2cFortran
#endif

#ifdef HAVE_HBOOK
#include <cfortran.h>
#include <hbook.h>
PAWC_DEFINE(8000000);
#else
// root includes
#include <TROOT.h>
#include <TMath.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TTree.h>
#include <TDirectory.h>
#include <TFile.h>
//as #include <TLemRunHeader.h>
#include <TMusrRunHeader.h>
#endif

//-- Globals -------------------------------------------------------

// The analyzer name (client name) as seen by other MIDAS clients
const char *analyzer_name = "Analyzer";

// analyzer_loop is called with this interval in ms (0 to disable)
INT  analyzer_loop_period = 1000;

// default ODB size
INT  odb_size = DEFAULT_ODB_SIZE;

char   runname[256];
INT    versionCounter;
//! Required ODB structures
RUNINFO           runinfo;
EXP_EDIT          exp_edit;
EXP_PARAM         exp_param;
SCALER_SETTINGS   scaler_settings;
RATE_BANK         rate_bank;
RATE_BANK_STR(rate_bank_str);
SRAT_BANK         srat_bank;
SRAT_BANK_STR(srat_bank_str);
TRIGGER_SETTINGS  trigger_settings;

extern DECAYANAMODULE_PARAM decay_ana_param;
T0SHIFT_PARAM        t0shift_param;

extern TFolder          *gManaHistosFolder;
static TFolder          *gRunHeader = NULL;
//as static TLemRunHeader    *header = NULL;
static TMusrRunHeader   *header = NULL;
static TObjArray        Slist(0);

static DWORD last_histogram_write;
static DWORD last_update_info_write;
#define HISTOGRAM_WRITE_PERIOD   300  // write histograms every 300s to file
static INT   write_flag;
static INT   sum_count;

static float rotation_angle; // spin rotation angle hotlink variable

//-- Module declarations -------------------------------------------
//! Define analyser modules which are from external source code.
//! ana_module tof_ana_module  = tof_ana_module.c
//! ana_module scaler_rate_sum = scaler_rate_sum.c
extern ANA_MODULE decay_ana_module;
extern ANA_MODULE scaler_rate_sum;

ANA_MODULE *trigger_module[] = {
  &decay_ana_module,
  NULL
};

ANA_MODULE *scaler_module[] = {
  &scaler_rate_sum,
  NULL
};

//-- Bank definitions ----------------------------------------------
/**
 * <p>Define the list of MIDAS banks to be used. The analyser can
 * either read MIDAS banks sent by other MIDAS clients (from the
 * frontends, for example), or create new BANKS which can be
 * used for subsequent analysis steps.
 */
BANK_LIST trigger_bank_list[] = {
  // online banks
  { "TDC0", TID_DWORD},
  { "" },
};

BANK_LIST scaler_bank_list[] = {
  // online banks
  { "SCL0", TID_DWORD}, //!< SCL0 is the bank created by the readout of VME scaler modules

  // calculated banks
  { "SSUM", TID_DOUBLE}, //!< calculated bank, sum of scaler events from SCL0 bank
  { "RATE", TID_STRUCT, sizeof(rate_bank), (char **)rate_bank_str }, //!< calculated bank, get the rate of single scaler channels
  { "SRAT", TID_STRUCT, sizeof(srat_bank), (char **)srat_bank_str },//!< calculated bank,  normalised rate of single scaler channels (counts/Ip)
  { "RAAV", TID_INT},//!< calculated bank, averaged rate of Ip
  { "" },
};

//-- Event request list --------------------------------------------
ANALYZE_REQUEST analyze_request[] = {
 { "Trigger",             //!< equipment name
   {1,                    //!< event ID
    TRIGGER_ALL,          //!< trigger mask
    GET_ALL,              //!< get all events, GET_SOME
    "SYSTEM",             //!< event buffer
    TRUE,                 //!< enabled
    "", "",},
    NULL,                 //!< analyzer routine
    trigger_module,       //!< module list
    trigger_bank_list,    //!< bank list
    1000,                 //!< RWNT buffer size
    TRUE,                 //!< Use tests for this event
  },

  { "Scaler",             //!< equipment name
    {2,                   //!< event ID
    TRIGGER_ALL,          //!< trigger mask
    GET_ALL,              //!< get all events
    "SYSTEM",             //!< event buffer
    TRUE,                 //!< enabled
    "", "",},
    NULL,                 //!< analyzer routine
    scaler_module,        //!< module list
    scaler_bank_list,     //!< bank list
    100,                  //!< RWNT buffer size
  },

  { "" }
};

//-- functions ---------------------------------------------------
/**
 * display scaler in analyzer window if
 * running online; function defined in module scaler_rate_sum.c
 */
INT  write_histogram_file();
void get_sum_filename(char *filename);
void extract_summary_data();
void update_run_header();
extern void disp_scaler(INT n);

//-- Analyzer Init -------------------------------------------------
INT analyzer_init()
{
  HNDLE hDB, hKey;
  char  str[80];
  char  fname[256], exp_name[32];
  char lazy_fname[128];
  INT   size, status;
  struct tm *tms; // for getting the year from the binary date
  time_t now;
  char  timestr[32];
  BOOL  flag_Bpar,flag_spinrot;

  RUNINFO_STR(runinfo_str);
  EXP_PARAM_STR(exp_param_str);
  EXP_EDIT_STR(exp_edit_str);
  SCALER_SETTINGS_STR(scaler_settings_str);
  T0SHIFT_PARAM_STR(t0shift_param_str);


  //----------------------------------------------------------------
  // open ODB structures
  //----------------------------------------------------------------

  cm_get_experiment_database(&hDB, NULL);

  db_create_record(hDB, 0, "/Runinfo", strcomb(runinfo_str));
  db_find_key(hDB, 0, "/Runinfo", &hKey);
  if (db_open_record(hDB, hKey, &runinfo, sizeof(runinfo), MODE_READ, NULL, NULL) != DB_SUCCESS) {
    cm_msg(MERROR, "analyzer_init", "Cannot open \"/Runinfo\" tree in ODB");
    return 0;
  }

  db_create_record(hDB, 0, "/Experiment/Run Parameters", strcomb(exp_param_str));
  db_find_key(hDB, 0, "/Experiment/Run Parameters", &hKey);
  if (db_open_record(hDB, hKey, &exp_param, sizeof(exp_param), MODE_READ, NULL, NULL) != DB_SUCCESS) {
    cm_msg(MERROR, "analyzer_init", "Cannot open \"/Experiment/Run Parameters\" tree in ODB");
    return 0;
  }

  sprintf(str, "/Experiment/Name");
  size = sizeof(exp_name);
  if (db_get_value(hDB, 0, str, exp_name, &size, TID_STRING, TRUE) != DB_SUCCESS ) {
     sprintf(exp_name, "nemu");
  }

  db_create_record(hDB, 0, "/Experiment/Edit on start", strcomb(exp_edit_str));
  db_find_key(hDB, 0, "/Equipment/Trigger/Settings", &hKey);
  if (db_open_record(hDB, hKey, &trigger_settings, sizeof(trigger_settings), MODE_READ,
     NULL, NULL) != DB_SUCCESS) {
    cm_msg(MERROR, "analyzer_init", "Cannot open \"/Equipment/Trigger/Settings\" tree in ODB");
    return 0;
  }

  db_create_record(hDB, 0, "/Equipment/Scaler/Settings", strcomb(scaler_settings_str));
  db_find_key(hDB, 0, "/Equipment/Scaler/Settings", &hKey);
  if (db_open_record(hDB, hKey, &scaler_settings, sizeof(scaler_settings), MODE_READ,
      NULL, NULL) != DB_SUCCESS) {
    cm_msg(MERROR, "analyzer_init", "Cannot open \"/Equipment/Scaler/Settings\" tree in ODB");
    return 0;
  }

  // create t0Shift structure
  sprintf(str, "/%s/Parameters/t0Shift", analyzer_name);
  db_create_record(hDB, 0, str, strcomb(t0shift_param_str));
  db_find_key(hDB, 0, str, &hKey);
  if (db_open_record(hDB, hKey, &t0shift_param, sizeof(t0shift_param),
      MODE_READ, NULL, NULL) != DB_SUCCESS) {
    cm_msg(MERROR, "analyzer_init", "Cannot open \"%s\" tree in ODB", str);
    return 0;
  }
  //----------------------------------------------------------------------
  // reset SRAT bank
  //----------------------------------------------------------------------

  if ( db_find_key(hDB, 0, "/Equipment/Scaler/Variables/SRAT", &hKey) == DB_SUCCESS ) {
    memset(&srat_bank, 0, sizeof(srat_bank));
    db_set_record(hDB, hKey, &srat_bank, sizeof(srat_bank), 0);
  }

  //---------------------------------------------------------------------
  //
  // change filenames to mucool%02d_%04d where %02d is replaced by the year
  //   modulo 100 (2001 becomes 01, for example).
  //
  //   File names are presently in odb keys
  //
  //   /Analyzer/Output/Filename                    ascii dump ??
  //   /Analyzer/Output/Histo Dump Filename         hsn file
  //   /Logger/ODB Dump file                        odb file
  //   /Logger/Channels/0/Settings/Filename         mid file
  //
  //   summ and rz files are generated in programs ../util/write_summary.c and
  //   ../util/data2ntp.c. Filenames to be changed there.
  //---------------------------------------------------------------------

  if ( runinfo.online_mode == 1) {
   time(&now);
   tms = localtime(&now);

   sprintf(str, "/%s/Output/Filename", analyzer_name);
   size = sizeof(fname);
   /* check if entry exists, if yes, change */
   if ( db_get_value(hDB, 0, str, fname, &size, TID_STRING, TRUE) == DB_SUCCESS ) {
        sprintf(fname, "mucool%02d_%%04d.asc", tms->tm_year%100);
        db_set_value(hDB, 0, str, &fname, sizeof(fname), 1, TID_STRING);
   }

   sprintf(str, "/%s/Output/Histo Dump Filename", analyzer_name);
   size = sizeof(fname);

#ifdef HAVE_HBOOK
   if ( db_get_value(hDB, 0, str, fname, &size, TID_STRING, TRUE) == DB_SUCCESS ) {
        // tms->tm_year yield  "Year - 1900"
        sprintf(fname, "../his/%04d/mucool%02d_%%04d.hsn", tms->tm_year+1900, tms->tm_year%100);
        db_set_value(hDB, 0, str, &fname, sizeof(fname), 1, TID_STRING);
   }
#else
   if ( db_get_value(hDB, 0, str, fname, &size, TID_STRING, TRUE) == DB_SUCCESS ) {
        sprintf(fname, "../his/%04d/mucool%02d_his_%%04d.root", tms->tm_year+1900, tms->tm_year%100);
        db_set_value(hDB, 0, str, &fname, sizeof(fname), 1, TID_STRING);
   }
#endif

  // Overwrite logger defaults for data directories and
  // file names.
   sprintf(str, "/Logger/Data dir");
   size = sizeof(fname);
   if ( db_get_value(hDB, 0, str, fname, &size, TID_STRING, FALSE) == DB_SUCCESS ) {
        sprintf(fname, "/data/%s/dlog", exp_name);
        db_set_value(hDB, 0, str, &fname, sizeof(fname), 1, TID_STRING);
   }

   sprintf(str, "/Logger/Message file");
   size = sizeof(fname);
   if ( db_get_value(hDB, 0, str, fname, &size, TID_STRING, FALSE) == DB_SUCCESS ) {
        sprintf(fname, "/data/%s/midas.log", exp_name);
        db_set_value(hDB, 0, str, &fname, sizeof(fname), 1, TID_STRING);
   }

   sprintf(str, "/Logger/ODB Dump File");
   size = sizeof(fname);
   if ( db_get_value(hDB, 0, str, fname, &size, TID_STRING, FALSE) == DB_SUCCESS ) {
        sprintf(fname, "/data/%s/odb/%04d/mucool%02d_%%04d.odb", exp_name, tms->tm_year+1900, tms->tm_year%100);
        db_set_value(hDB, 0, str, &fname, sizeof(fname), 1, TID_STRING);
   }

   sprintf(str, "/Logger/Channels/0/Settings/Filename");
   size = sizeof(fname);
   if ( db_get_value(hDB, 0, str, fname, &size, TID_STRING, FALSE) == DB_SUCCESS ) {
        sprintf(fname, "mucool%02d_%%04d.root", tms->tm_year%100);
        db_set_value(hDB, 0, str, &fname, sizeof(fname), 1, TID_STRING);
   }

   sprintf(str, "/Logger/History Dir");
   sprintf(fname, "/data/%s/history", exp_name);
   db_set_value(hDB, 0, str, &fname, sizeof(fname), 1, TID_STRING);

   sprintf(str, "/Logger/History Path");
   sprintf(fname, "");
   db_set_value(hDB, 0, str, &fname, sizeof(fname), 1, TID_STRING);

   sprintf(str, "/Logger/Elog Dir");
   //sprintf(fname, "/home/%s/elog/LEM_Experiment", exp_name);
   sprintf(fname, "/home/MuCool/elog/LEM_Experiment");
   db_set_value(hDB, 0, str, &fname, sizeof(fname), 1, TID_STRING);

// for lazylogger, use filesize of 128 to agree with string lengths there
/*   sprintf(str, "/Lazy/FTP/Settings/Filename format");
   size = sizeof(lazy_fname);
   if ( db_get_value(hDB, 0, str, lazy_fname, &size, TID_STRING, FALSE) == DB_SUCCESS ) {
        sprintf(lazy_fname, "mucool%02d_%%04d.root", tms->tm_year%100);
        db_set_value(hDB, 0, str, &lazy_fname, size, 1, TID_STRING);
   }*/
  } /* if (runinfo.online_mode == 1) */

  // to create Run Name in histograms in analyzer modules
  sprintf(str, "/%s/Output/Filename", analyzer_name);
  size = sizeof(fname);
  db_get_value(hDB, 0, str, fname, &size, TID_STRING, TRUE);
  strcpy(runname, fname);
  runname[5] = 0; // truncate run number and extension

  timestr[0] = 0;
  sprintf(timestr, "%s", ctime(&now));
  timestr[24] = 0; // delete new line \n
  db_set_value(hDB, 0, "/Experiment/Run Parameters/Scaler Update time", &timestr, sizeof(timestr), 1, TID_STRING);

  // root file header related things
  gRunHeader = gROOT->GetRootFolder()->AddFolder("RunHeader", "MuCool Run Header Info");
  gROOT->GetListOfBrowsables()->Add(gRunHeader, "RunHeader");
//as  header = new TLemRunHeader();
  header = new TMusrRunHeader(true);
  header->FillFolder(gRunHeader);
//as  gRunHeader->Add(header); //add header to RunInfo folder
  gRunHeader->Add(&Slist);
  Slist.SetName("RunSummary");

  last_histogram_write = last_update_info_write = sum_count = 0;
  write_flag = 1;

  // Register callback for messages to allow for SMS/E-Mail send in case of an Alarm
  // cm_msg_register(receive_message);

  return SUCCESS;
}

//-- Analyzer Exit -------------------------------------------------

INT analyzer_exit()
{
  return CM_SUCCESS;
}

//-- Begin of Run --------------------------------------------------

INT ana_begin_of_run(INT run_number, char *error)
{
  HNDLE hDB, hkey;

  cm_get_experiment_database(&hDB, NULL);

  //----------------------------------------------------------------------
  //  reset SRAT bank
  //----------------------------------------------------------------------

  if ( db_find_key(hDB, 0, "/Equipment/Scaler/Variables/SRAT", &hkey) == DB_SUCCESS ) {
        memset(&srat_bank, 0, sizeof(srat_bank));
        db_set_record(hDB, hkey, &srat_bank, sizeof(srat_bank), 0);
  }

  //-- display scaler in analyzer window when running online -------------
  if ( runinfo.online_mode == 1) {

    /*
        a bit strange: runinfo.state is still STATE_STOPPED when arriving here,
        although other ODB parameters like run number and Start time are already
        set properly for Run start; problem could be that the change of the ODB value
        for
        runinfo.state is done at the end of the cm_transition() function, maybe
        too late for analyzer programs which already reads the runinfo.state value.
        To avoid this problem I force runinfo.state to have the right value for
        the analyzer/scaler functions.
    */
    runinfo.state = STATE_RUNNING;
    disp_scaler(N_SCALER);
  }

  versionCounter = 0;

  // update Run header
  update_run_header();

  write_flag = 1;
  sum_count  = 0;

  return CM_SUCCESS;
}

//-- End of Run ----------------------------------------------------

INT ana_end_of_run(INT run_number, char *error)
{
  FILE   *f;
  time_t now;
  char   str[256], logdir[256], cmd[256], file_name[256];
  char   year[5];
  INT    size, len;
  double n;
  HNDLE  hDB, hkey;
  BOOL   flag;

  cm_get_experiment_database(&hDB, NULL);

  // update run log if run was written and running online
  size = sizeof(flag);
  db_get_value(hDB, 0, "/Logger/Write data", &flag, &size, TID_BOOL, FALSE);
  if (flag && runinfo.online_mode == 1) {
    // update run log
    size = sizeof(str);
    str[0] = 0;
    file_name[0] = 0;
    year[0] = 0;
    db_get_value(hDB, 0, "/Logger/Data Dir", str, &size, TID_STRING, FALSE);
    if (str[0] != 0)
      if (str[strlen(str)-1] != DIR_SEPARATOR)
        strcat(str, DIR_SEPARATOR_STR);
    strcpy(logdir, str);  /* save copy of logger dir */
    strcat(str, "runlog.txt");
    strcpy(file_name, str);
    f = fopen(file_name, "a");

    time(&now);
    strcpy(str, ctime(&now));
    year[0] = str[20];
    year[1] = str[21];
    year[2] = str[22];
    year[3] = str[23];
    year[4] = 0;
    str[10] = 0;

    fprintf(f, "%5d\t%s %s", runinfo.run_number, str, year);

    strcpy(str, runinfo.start_time);
    str[19] = 0;
    fprintf(f, "::%s --- ", str+11);

    strcpy(str, ctime(&now));
    str[19] = 0;
    fprintf(f, "%s\t", str+11);

    size = sizeof(n);
    db_get_value(hDB, 0, "/Equipment/Trigger/Statistics/Events sent", &n, &size, TID_DOUBLE, FALSE);

    fprintf(f, "%6.1lfk\t", n/1000);
    fprintf(f, "%s\n", exp_param.comment);

    fclose(f);

    //------------------
    // copy additionally runlog file to /logger/Data dir;
    // elog looks there for the run log file
    //------------------

    cmd[0]= 0;
    sprintf(cmd, "cp %s %s", file_name, logdir);
    system(cmd);

    runinfo.state = STATE_STOPPED;
    runinfo.stop_time_binary = now;
    strcpy(runinfo.stop_time, ctime(&now));

    // delete \n in runinfo.stop_time if present
    len = strlen(runinfo.stop_time);
    if (runinfo.stop_time[len-1] == '\n' )
        runinfo.stop_time[len-1] = 0;

    //-------- update ODB ----------------------
    //
    //  also a bit strange here: the ODB dump does not contain the correct Stop time
    //  of a run; it appears to be written too late to the ODB, i.e. a f t e r saving
    //  the ODB.
    //  I force setting the right values to ODB here in analyzer.
    //------------------------------------------

    db_set_value(hDB, 0, "/Runinfo/State", &runinfo.state, sizeof(runinfo.state), 1, TID_INT);
    db_set_value(hDB, 0, "/Runinfo/Stop time", &runinfo.stop_time,
                 sizeof(runinfo.stop_time), 1, TID_STRING);
    db_set_value(hDB, 0, "/Runinfo/Stop time binary", &runinfo.stop_time_binary,
                 sizeof(runinfo.stop_time_binary), 1, TID_DWORD);

    disp_scaler(N_SCALER);
    ss_printf(0,50,"Rates at EOR are a v e r a g e d rates !");
  }

  // update run header
  update_run_header();

  return CM_SUCCESS;
}

//-- Pause Run -----------------------------------------------------

INT ana_pause_run(INT run_number, char *error)
{
  runinfo.state = STATE_PAUSED;
  disp_scaler(N_SCALER);
  return CM_SUCCESS;
}

//-- Resume Run ----------------------------------------------------

INT ana_resume_run(INT run_number, char *error)
{
  HNDLE hDB;

  cm_get_experiment_database(&hDB, NULL);

  runinfo.state = STATE_RUNNING;
  disp_scaler(N_SCALER);
  return CM_SUCCESS;
}

//-- Analyzer Loop -------------------------------------------------

INT analyzer_loop()
{
  DWORD nowtime, difftime;

  nowtime = ss_time();
  difftime = nowtime - last_update_info_write;

  if ( runinfo.state == STATE_PAUSED ) return CM_SUCCESS;

  if ( runinfo.state == STATE_RUNNING ) {
    write_flag = 0;
    difftime = nowtime - last_histogram_write;
    if ( difftime > HISTOGRAM_WRITE_PERIOD ) {
      update_run_header();
      write_histogram_file();
      last_histogram_write = nowtime;
      if ( sum_count == 1 ) { // extract summary file when we reach this point
        Slist.Delete();
        extract_summary_data(); // the 2nd time
      }
      sum_count++;
    }
  }

  if ( runinfo.state == STATE_STOPPED && !write_flag ) {
    ss_sleep(3000); // wait 3sec for final summary file
    Slist.Delete();
    extract_summary_data();
    write_histogram_file();
    write_flag = 1;
  }

  return CM_SUCCESS;
}

// ------------------------------------------------------------------
/**
 * <p>
 */
INT write_histogram_file()
{
  HNDLE hDB, hkey;
  char  fname[256], str[256], file_name[256], save_file_name[256], cmd[256];
  INT   size;

  cm_get_experiment_database(&hDB, NULL);
  sprintf(str, "/%s/Output/Histo Dump Filename", analyzer_name);
  size = sizeof(fname);
  db_get_value(hDB, 0, str, fname, &size, TID_STRING, TRUE);

  if (strchr(fname, '%') != NULL)
    sprintf(file_name, fname, runinfo.run_number);
  else
    strcpy(file_name, fname);

  db_find_key(hDB, 0, "/Logger/Data dir", &hkey);
  if (hkey) {
    size = sizeof(str);
    db_get_data(hDB, hkey, str, &size, TID_STRING);
    if (str[strlen(str) - 1] != DIR_SEPARATOR)
      strcat(str, DIR_SEPARATOR_STR);
    strcat(str, file_name);
    strcpy(file_name, str);
  }

  strcpy(save_file_name, file_name);
  //
  // before May-10, 2013: save histogram as  lemYY_his_RunNr.V.root
  //
  // May-10, 2013: changed to lemYY_his_RunNr.root.V
  //
  // *strstr(file_name, ".root") = 0;
  sprintf(str, ".%d", versionCounter);
  strcat(file_name, str);
  // strcat(file_name, ".root");
  versionCounter++;

  // the following is adapted from SaveRootHistograms() in mana.c
  TDirectory *savedir = gDirectory;
  TFile *outf = new TFile(file_name, "RECREATE", "Midas Analyzer Histograms");
  if (outf == 0) {
    cm_msg(MERROR, "SaveRootHistograms", "Cannot create output file %s", file_name);
    return 0;
  }
  outf->cd();
  gManaHistosFolder->Write();
  if (header->FillFolder(gRunHeader))
    gRunHeader->Write();
  else
    cm_msg(MERROR, "write_histogram_file", "analyzer error: couldn't fill the run header information!");
  outf->Close();
  delete outf;
  // restore current directory
  savedir->cd();
  //
  // copy lemYY_his_RunNr.V.root to lemYY_his_RunNr.root
  //
  sprintf(cmd,"cp %s %s", file_name, save_file_name);
  system(cmd);
  //cm_msg(MINFO, "analyzer_loop", "saved histograms in %s", file_name);

  return CM_SUCCESS;
}

//-----------------------------------------------------------------
/**
 * <p>Create filename for run summary files of the Nemu experiment.
 *
 * \param filename pointer to the file name
 */
void get_sum_filename(char *filename)
{
  struct tm *tms;
  time_t now;
  char   dir[256], filebody[256];

  dir[0]   = 0;
  filebody[0] = 0;

//   if ( getenv("MIDAS_DATA") ) {
//         strcpy(dir, getenv("MIDAS_DATA"));
//         strcat(dir, DIR_SEPARATOR_STR);
//         strcat(dir, "summ");
//         strcat(dir, DIR_SEPARATOR_STR);
//   } else {
// #if defined (OS_UNIX)
//     strcpy(dir, getenv("HOME"));
// #elif defined (OS_WINNT)
//     strcpy(dir, getenv("HOMEPATH"));
// #endif
//     strcat(dir, DIR_SEPARATOR_STR);
//     cm_msg(MINFO, "get_sum_filename",
//            "MIDAS_DATA path not defined, use home directory %s instead\n", dir);
//   }

  strcpy(dir, "/data/MuCool/summ/");
  time(&now);
  tms = localtime(&now);
  // tms->tm_year yields "Year - 1900"
  strcpy(filename, dir);
  sprintf(filebody, "%04d/mucool%02d_%04d.summ", tms->tm_year+1900, tms->tm_year%100, runinfo.run_number);
  strcat(filename, filebody);
}

//-----------------------------------------------------------------
/**
 * <p>
 */
void extract_summary_data()
{
  INT        i;
  char       fileName[256], line[256], enumline[256];
  TObjString *s;

  get_sum_filename(fileName);

  i = 0;
  FILE *fp = fopen(fileName,"r");
  if ( fp == NULL ) {
    printf("File %s does not exist!\n", fileName);
    return;
  }

  while (fgets(line,256,fp)) {
    sprintf(enumline, "%04d ", i);
    strcat(enumline, line);
    s = new TObjString(enumline);
    Slist.Add(s);
    i++;
  }

  fclose(fp);
}

//-----------------------------------------------------------------
/**
 * <p>Write TMusrRunHeader for root histogram file
 */
void update_run_header()
{
  HNDLE  hDB, hKey;
  INT    size, i, ival;
  vector<int> ivec;
  char   str[256], str1[256], *p_str;
  double ip_sum, scaler_clock_sum, tdc_clock_sum, n_events, dval;
  double mcounter_sum, mcounter_clean_sum, mcounter_good_sum, pos_sum[N_DECAY_HISTS];
  long   time_bin;

  // get the main ODB handle
  cm_get_experiment_database(&hDB, NULL);

  //----------------------------
  // RunInfo (required)
  //----------------------------

  // 1st write all the required RunInfo entries

  header->Set("RunInfo/Generic Validator URL", "http://lmu.web.psi.ch/facilities/software/MusrRoot/validation/MusrRoot.xsd");
  header->Set("RunInfo/Specific Validator URL", "http://lmu.web.psi.ch/facilities/software/MusrRoot/validation/MusrRootLEM.xsd");
  header->Set("RunInfo/Generator", "mucool_analyzer");

  // compose the file name
  size = sizeof(str);
  db_find_key(hDB, 0, "/Analyzer/Output/Histo Dump Filename", &hKey);
  db_get_data(hDB, hKey, &str, &size, TID_STRING);
  // filter out the last part of the string spearted by "/", str should have the format <something>/lem<yy>_his_%04d.root
  i = 0;
  p_str = 0;
  do {
    if (str[i] == '/')
      p_str = &str[i];
    i++;
  } while (str[i] != '\0');
  if (p_str == 0) {
    cm_msg(MERROR, "update_run_header", "Couldn't obtain run file name template! ODB corrupted?");
    return;
  }
  p_str++;
  // replace "%04d" through the run number
  strcpy(str1, p_str);
  p_str = strstr(str1, "%");
  if (p_str == 0) {
    cm_msg(MERROR, "update_run_header", "Wrong run file name template! ODB corrupted?");
    return;
  }
  *p_str = '\0';
  sprintf(str, "%04d.root", runinfo.run_number);
  strcat(str1, str);
  header->Set("RunInfo/File Name", str1);

  header->Set("RunInfo/Run Title", exp_param.comment);
  header->Set("RunInfo/Run Number", runinfo.run_number);

  // handle start and stop time
  time_bin = (long)runinfo.start_time_binary; // for 64bit this conversion is needed
  struct tm *tm = localtime((time_t*)&time_bin);
  strftime(str, sizeof(str), "%F %T", tm);
  header->Set("RunInfo/Run Start Time", str);
  time_bin = (long)runinfo.stop_time_binary;
  tm = localtime((time_t*)&time_bin);
  strftime(str, sizeof(str), "%F %T", tm);
  header->Set("RunInfo/Run Stop Time", str);
  if (runinfo.stop_time_binary < runinfo.start_time_binary) // ongoing run
    ival = -1;
  else
    ival = (int)runinfo.stop_time_binary - (int)runinfo.start_time_binary;

  TMusrRunPhysicalQuantity prop;
  prop.Set("Run Duration", ival, "sec");
  header->Set("RunInfo/Run Duration", prop);

  header->Set("RunInfo/Laboratory", "PSI");
  header->Set("RunInfo/Instrument", "MuCool");

  prop.Set("Muon Beam Momentum", 28.1, "MeV/c");
  header->Set("RunInfo/Muon Beam Momentum", prop);

  header->Set("RunInfo/Muon Species", "positive muon");
  header->Set("RunInfo/Muon Source", "Target E - Low Energy Muons");
  header->Set("RunInfo/Setup", "n/a");
  header->Set("RunInfo/Comment", "n/a");
  header->Set("RunInfo/Sample Name", "n/a");

  prop.Set("Sample Temperature", 0, 0, "K");
  header->Set("RunInfo/Sample Temperature", prop);
  prop.Set("Sample Magnetic Field", MRH_UNDEFINED, 0, 0, "G");
  header->Set("RunInfo/Sample Magnetic Field", prop);

  header->Set("RunInfo/No of Histos", N_DECAY_HISTS);

  prop.Set("Time Resolution", 0.1953125, "ns", "TDC CAEN V1190");
  header->Set("RunInfo/Time Resolution", prop);

  ivec.push_back(0);
  ivec.push_back(N_OFFSET_PPC_HISTOGRAMS);
  header->Set("RunInfo/RedGreen Offsets", ivec);

  //----------------------------
  // DetectorInfo  (required)
  //----------------------------
  // In the future, this part should probably being filled in a more abstract manner, especially if it comes to
  // real red/green mode experiments and histo fillings.

  // NPP's
  for (i=0; i<N_DECAY_HISTS; i++) {
    // Name
    sprintf(str, "DetectorInfo/Detector%03d", i+1);
    snprintf(str1, sizeof(str1), "%s/Name", str);
    header->Set(str1, decay_ana_param.histotitles.titles[i]);
    // Histo Number
    snprintf(str1, sizeof(str1), "%s/Histo Number", str);
    header->Set(str1, i+1);
    // Histo Length
    snprintf(str1, sizeof(str1), "%s/Histo Length", str);
    header->Set(str1, decay_ana_param.histobinning.histonbin[i]);
    // Time Zero Bin
    snprintf(str1, sizeof(str1), "%s/Time Zero Bin", str);
    dval = (double) t0shift_param.t0[i];
    header->Set(str1, dval);
    // First Good Bin
    snprintf(str1, sizeof(str1), "%s/First Good Bin", str);
    ival = (int) ceil(dval);
    header->Set(str1, ival);
    // Last Good Bin
    snprintf(str1, sizeof(str1), "%s/Last Good Bin", str);
    header->Set(str1, decay_ana_param.histobinning.histonbin[i]-1);
  }

  // PPC's
  for (i=0; i<N_DECAY_HISTS; i++) {
    // Name
    sprintf(str, "DetectorInfo/Detector%03d", i+1+N_OFFSET_PPC_HISTOGRAMS);
    snprintf(str1, sizeof(str1), "%s/Name", str);
    header->Set(str1, decay_ana_param.histotitles.titles[i+N_DECAY_HISTS]);
    // Histo Number
    snprintf(str1, sizeof(str1), "%s/Histo Number", str);
    header->Set(str1, i+1+N_OFFSET_PPC_HISTOGRAMS);
    // Histo Length
    snprintf(str1, sizeof(str1), "%s/Histo Length", str);
    header->Set(str1, decay_ana_param.histobinning.histonbin[i+N_DECAY_HISTS]);
    // Time Zero Bin
    snprintf(str1, sizeof(str1), "%s/Time Zero Bin", str);
    dval = (double) t0shift_param.t0[i];
    header->Set(str1, dval);
    // First Good Bin
    snprintf(str1, sizeof(str1), "%s/First Good Bin", str);
    ival = (int) ceil(dval);
    header->Set(str1, ival);
    // Last Good Bin
    snprintf(str1, sizeof(str1), "%s/Last Good Bin", str);
    header->Set(str1, decay_ana_param.histobinning.histonbin[i+N_DECAY_HISTS]-1);
  }

  //-----------------------------------
  // SampleEnvironmentInfo  (required)
  //-----------------------------------

  // 1st required entries for SampleEnvironmentInfo

  header->Set("SampleEnvironmentInfo/Cryo", "n/a");

  // 2nd LEM specific entries for SampleEnvironmentInfo

  // nothing to be set yet


  //-----------------------------------------
  // MagneticFieldEnvironmentInfo (required)
  //-----------------------------------------

  // 1st required entries for MagneticFieldEnvironmentInfo

  header->Set("MagneticFieldEnvironmentInfo/Magnet Name", "n/a");

  // 2nd LEM specific entries for MagneticFieldEnvironmentInfo

  // nothing to be set yet

  //----------------------------
  // BeamlineInfo (required)
  //----------------------------

  // 1st required entries for BeamlineInfo

  header->Set("BeamlineInfo/Name", "piE1");

  // 2nd LEM specific entries for MagneticFieldEnvironmentInfo

  // header->Set("BeamlineInfo/Beamline Settings", info.beamline_settings);

  //---------------------------------------------
  // ScalerInfo (NOT required, i.e LEM specific)
  //--------------------------------------------

  size = sizeof(double);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[0]", &ip_sum, &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[1]", &scaler_clock_sum, &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[2]", &mcounter_sum, &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[3]", &pos_sum[0], &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[4]", &pos_sum[1], &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[5]", &pos_sum[2], &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[6]", &pos_sum[3], &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[7]", &pos_sum[4], &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[8]", &pos_sum[5], &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[9]", &pos_sum[6], &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Scaler/Variables/SSUM[10]", &pos_sum[7], &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Trigger/VME_Statistics/Mcounter clean", &mcounter_clean_sum, &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Trigger/VME_Statistics/Mcounter good" , &mcounter_good_sum, &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Trigger/VME_Statistics/channelCounts[1]" , &tdc_clock_sum, &size, TID_DOUBLE, FALSE);
  db_get_value(hDB, 0, "/Equipment/Trigger/VME_Statistics/MuonEvents" , &n_events, &size, TID_DOUBLE, FALSE);

  header->Set("ScalerInfo/Sum Ip", ip_sum);
  header->Set("ScalerInfo/Sum Clock (Scaler)", scaler_clock_sum);
  header->Set("ScalerInfo/Sum Clock (TDC)", tdc_clock_sum);
  header->Set("ScalerInfo/Sum Slow Muon Events", n_events);
  header->Set("ScalerInfo/Sum Mcounter",       mcounter_sum);
  header->Set("ScalerInfo/Sum Mcounter_Clean", mcounter_clean_sum);
  header->Set("ScalerInfo/Sum Mcounter_Good",  mcounter_good_sum);

  TIntVector posSumVec;
  for (unsigned int i=0; i<8; i++)
    posSumVec.push_back((int)pos_sum[i]);
  header->Set("ScalerInfo/Sum Positrons", posSumVec);
}
//-- end -------------------------------------------------------------------------
