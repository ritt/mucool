void
coinc (int run=0)
{
  TString plotOpt[3] = {"","same","same"};
  int histColor[3] = {1, 2, 4}; // black, red, blue

//  gSystem->Exec("source /Users/phillips/Code/muCool/ana/update.sh");
  gStyle->SetOptStat (1111);
  gStyle->SetOptFit (1111);
  gStyle->SetStatFontSize (0.05);

  TFile *f[200];
  TFolder *fol[200];
  TH1F *h[200][30];
  TH1F *time[200][30];
  TCanvas *c1 = new TCanvas ("c1", "c1", 600, 600);
  c1->Divide (2,2);
    
    char name[50];
    sprintf (name, "/Users/phillips/data/muCool/2014/mucool14_his_%4.4d.root", run);
    f[run] = new TFile (name);
    fol[run] = (TFolder *) f[run]->Get ("histos");
    char name1[50];
    
    sprintf (name1, "NScintHits");
    TH1F* Hist1 = (TH1F *) fol[run]->FindObjectAny (name1);
    c1->cd(1)->SetLogy ();
    //Hist1->Rebin(10);
    Hist1->Draw ();

    sprintf (name1, "CoincChan");
    TH1F* Hist2 = (TH1F *) fol[run]->FindObjectAny (name1);
    c1->cd(2);
   // Hist2->Rebin(10);
    Hist2->Draw ();
    
    sprintf (name1, "CoincTimeDiff");
    TH1F* Hist3 = (TH1F *) fol[run]->FindObjectAny (name1);
    //Hist3->GetXaxis()->SetRangeUser(150,250);
    c1->cd(3)->SetLogy ();
    Hist3->Draw ();

    sprintf (name1, "CoincChan2");
    TH2F* Hist4 = (TH2F *) fol[run]->FindObjectAny (name1);
    c1->cd(4)->SetLogz ();
    Hist4->Draw ("col");
    
}

