int 
stopping_distribution (int i=0)
{

  gSystem->Exec("source /home/muon/muCOOL/data/ana/update.sh");
//  gStyle->SetOptDate (1);
  gStyle->SetOptStat (11);
  gStyle->SetOptFit (11);
  gStyle->SetTitleFontSize (0.07);
  gStyle->SetStatFontSize (0.07);

  TFile *f[200];
  TFolder *fol[200];
  TH1F *h[200][30];
  TH1F *time[200][30];
// TCanvas *c1 = new TCanvas ("c1", "c1", 3000, 1800);
  TCanvas *c1;
  double count[30];
  double x[30];
 

  // define t0 as bin 2061 

//  for (int i = 22; i < 36; i++)
//   {
  c1 = new TCanvas ("c1", "c1", 1000, 800);
  // c1->Divide (3, 1);
  char name[1000];
  sprintf (name, "../his/2014/mucool14_his_%4.4d.root", i);


  f[i] = new TFile (name);
  fol[i] = (TFolder *) f[i]->Get ("histos");

  for (int j = 1; j < 27; j++)
    {
      char name1[50];
      sprintf (name1, "hDecay%3.3d", j);
      h[i][j] = (TH1F *) fol[i]->FindObjectAny (name1);
      count[j]=h[i][j]->GetEntries();
    }

  TGraph *g = new TGraph (21);

 // c1->cd(1);
 //  c1->cd(1)->SetCanvasSize(1500,1000);

  for(int jj=1;jj<22;jj++){
    g->SetPoint(jj,jj,count[jj+5]);
  }
  
  // const int n=30;

  g->DrawClone("ALP");

 //  c1->cd(2);

 // TText *xlabel = new TText();
 // xlabel-> SetNDC();
 // xlabel -> SetTextSize(0.06);
 // xlabel -> SetTextAlign(22);
 // xlabel -> DrawText(0.5, 0.92, " << Detector Rates >>");
 // xlabel -> DrawText(0.3, 0.82 , "Exit Detector");
 // xlabel -> DrawText(0.3, 0.76, "Upstream");
 // xlabel -> DrawText(0.3, 0.70 , "Downstream");
 // xlabel -> SetTextColor(2);
 // xlabel -> DrawText(0.3, 0.64, "T1");
 // xlabel -> DrawText(0.3, 0.58 , "T2");
 // xlabel -> SetTextColor(1);
 // xlabel -> DrawText(0.3, 0.52, "Scint 01");
 // xlabel -> DrawText(0.3, 0.46 , "Scint 02");
 // xlabel -> DrawText(0.3, 0.40, "Scint 03");
 // xlabel -> DrawText(0.3, 0.34 , "Scint 04");
 // xlabel -> DrawText(0.3, 0.28, "Scint 05");
 // xlabel -> DrawText(0.3, 0.22 , "Scint 06");
 // xlabel -> DrawText(0.3, 0.16, "Scint 07");
 // xlabel -> DrawText(0.3, 0.10, "Scint 08");

 // char dummy[1000];
 // for(int jj=1;jj<14;jj++){
 //   sprintf (dummy, "%i", count[jj]);
 // // sprintf (dummy, "%6.6d", count[i]);
 //  if(jj==4){
 // xlabel -> SetTextColor(2);
 //  }
 // xlabel -> DrawText(0.7, 0.82-(jj-1)*0.06, dummy);
 //  if(jj==5){
 // xlabel -> SetTextColor(1);
 //  }
 //  }

 //   c1->cd(3);
 //  TText *xlabel = new TText();
 // xlabel-> SetNDC();
 // xlabel -> SetTextSize(0.06);
 // xlabel -> SetTextAlign(22);
 // xlabel -> DrawText(0.5, 0.92, "<< Detector Rates >>");
 // xlabel -> DrawText(0.3, 0.82, "Scint 09");
 // xlabel -> DrawText(0.3, 0.76 ,"Scint 10");
 // xlabel -> SetTextColor(2);
 // xlabel -> DrawText(0.3, 0.70, "Scint 11");
 // xlabel -> SetTextColor(1);
 // xlabel -> DrawText(0.3, 0.64 ,"Scint 12");
 // xlabel -> DrawText(0.3, 0.58, "Scint 13");
 // xlabel -> DrawText(0.3, 0.52 ,"Scint 14");
 // xlabel -> DrawText(0.3, 0.46, "Scint 15");
 // xlabel -> DrawText(0.3, 0.40, "Scint 16");
 // xlabel -> DrawText(0.3, 0.34, "Scint 17");
 // xlabel -> DrawText(0.3, 0.28, "Scint 18");
 // xlabel -> DrawText(0.3, 0.22, "Scint 19");
 // xlabel -> DrawText(0.3, 0.16, "Scint 20");
 // xlabel -> DrawText(0.3, 0.10, "Scint 21");

 // for(int jj=14;jj<27;jj++){
 //  sprintf (dummy, "%i", count[jj]);
 // // sprintf (dummy, "%6.6d", count[i]);
 //  if(jj==16){
 // xlabel -> SetTextColor(2);
 //  }
 //  xlabel -> DrawText(0.7, 0.82-(jj-14)*0.06, dummy);
 //  if(jj==16){
 // xlabel -> SetTextColor(1);
 //  }
 //  }

  c1->Update();



}
