void
normplot_multi ()
{
  // Select voltage/pressure/cell (0=short, 1=long)/impurity (0=nothing, 1=H2, 2=O2)/impurity pressure combinations that you like to plot
  const int numPlots = 6;
  float plotCmd[numPlots][5] = {{-600,11,1,0,0}, {+600,11,1,0,0}, {0,11,1,0,0}, {-600,11,1,2,0.02}, {+600,11,1,2,0.02}, {-600,0,1,0,0}};
  // Adjust time [ns] where you want the histograms to be normalized to 1
  float normTime = 0.;

  TNtuple *runInfo = new TNtuple("runInfo","Run parameters","run:volt:press:duration:cell:impurity:impPress");
  runInfo->Fill (148, -600,  0, 471, 1, 0, 0.00);
  runInfo->Fill (147, +600, 11, 120, 1, 2, 0.02);
  runInfo->Fill (145, -600, 11,  61, 1, 2, 0.02);
  runInfo->Fill (144, -600, 11,  81, 1, 2, 0.02);
  runInfo->Fill (143, -450, 11,  65, 1, 2, 0.01); 
  runInfo->Fill (142, -500,  5, 121, 1, 2, 0.01);
  runInfo->Fill (141, -500,  5,  20, 1, 2, 0.1);  
  
  runInfo->Fill (140, -500,  5, 144, 1, 1, 0.02); 
  runInfo->Fill (139, -200,  5,  56, 1, 1, 0.01); 
  runInfo->Fill (138, +500,  5, 120, 1, 1, 0.01); 
  runInfo->Fill (137,    0,  5, 349, 1, 1, 0.01);
  runInfo->Fill (136, -500,  5,  60, 1, 1, 0.01);
  runInfo->Fill (135, +500,  5, 120, 1, 1, 0.01);
  runInfo->Fill (134, -500,  5, 120, 1, 1, 0.01);
  runInfo->Fill (133, -500,  5,  57, 1, 1, 0.1); 
  runInfo->Fill (132, -200,  5,  45, 1, 1, 0.1);
  runInfo->Fill (131, -200,  5,  34, 1, 1, 1);  

  runInfo->Fill (129, +400, 11, 120, 1, 0, 0); 
  runInfo->Fill (128, +600, 11, 119, 1, 0, 0); 
  runInfo->Fill (127, +600, 11,  15, 1, 0, 0); 
  runInfo->Fill (126,    0, 11, 122, 1, 0, 0);
  runInfo->Fill (125,    0, 11,   4, 1, 0, 0);
  runInfo->Fill (124, -400, 11, 356, 1, 0, 0);
  runInfo->Fill (123, -600, 11, 180, 1, 0, 0);
  runInfo->Fill (121, +500, 5,   84, 1, 0, 0); 
  runInfo->Fill (120, +100, 5,  120, 1, 0, 0);
  runInfo->Fill (119, +200, 5,  120, 1, 0, 0);
  runInfo->Fill (118, -200, 5,  121, 1, 0, 0);
  runInfo->Fill (117, -500, 5,  266, 1, 0, 0);
  runInfo->Fill (116, -100, 5,  127, 1, 0, 0);
  runInfo->Fill (115, +100, 5,  127, 1, 0, 0);     
  runInfo->Fill (114, +200, 5,  120, 1, 0, 0);    
  runInfo->Fill (113, -200, 5,  125, 1, 0, 0);   
  runInfo->Fill (112, -100, 5,  120, 1, 0, 0);
  runInfo->Fill (111, +500, 5,  121, 1, 0, 0);  
  runInfo->Fill (110, -500, 5,  127, 1, 0, 0);  
  runInfo->Fill (109, -500, 5,  120, 1, 0, 0);  
  runInfo->Fill (108, +350, 5,   34, 1, 0, 0);  
  runInfo->Fill (106,    0, 5,  120, 1, 0, 0);  
  runInfo->Fill (105, +350, 5,  210, 1, 0, 0);  
  runInfo->Fill (104, -350, 5,  180, 1, 0, 0);  
  runInfo->Fill (103, -350, 5,   80, 1, 0, 0);  
  runInfo->Fill (102,    0, 5,  120, 1, 0, 0); 
  runInfo->Fill (100, +300, 11, 110, 0, 0, 0);
  runInfo->Fill ( 99, -300, 11,  80, 0, 0, 0);
  runInfo->Fill ( 98, -300, 11,  17, 0, 0, 0);
  runInfo->Fill ( 97, -300, 11,  24, 0, 0, 0);
  runInfo->Fill ( 96, +450, 11,  89, 0, 0, 0);
  runInfo->Fill ( 95,    0, 11, 184, 0, 0, 0);
  runInfo->Fill ( 93, +450, 11, 150, 0, 0, 0);
  runInfo->Fill ( 92, -450, 11, 258, 0, 0, 0);
  runInfo->Fill ( 91,    0,  5,  60, 0, 0, 0);
  runInfo->Fill ( 90, +300,  5, 120, 0, 0, 0);
  runInfo->Fill ( 89, -300,  5, 128, 0, 0, 0);
  runInfo->Fill ( 88, +300,  5, 131, 0, 0, 0);
  runInfo->Fill ( 86, -300,  5, 120, 0, 0, 0);
  runInfo->Fill ( 85, -450,  5, 206, 0, 0, 0);
  runInfo->Fill ( 84, -450,  5,  63, 0, 0, 0);
  runInfo->Fill ( 83, +450,  5, 120, 0, 0, 0);
  runInfo->Fill ( 82,    0,  5, 118, 0, 0, 0);
  runInfo->Fill ( 81,    0,  5, 120, 0, 0, 0);
  runInfo->Fill ( 80, +450,  5, 120, 0, 0, 0);
  runInfo->Fill ( 79, -450,  5, 120, 0, 0, 0);
  runInfo->Fill ( 76,    0,  8,  74, 0, 0, 0);
  runInfo->Fill ( 74, +300,  8, 114, 0, 0, 0);
  runInfo->Fill ( 72, -300,  8, 251, 0, 0, 0);
  runInfo->Fill ( 71,    0,  8, 122, 0, 0, 0);
  runInfo->Fill ( 70, +450,  8, 121, 0, 0, 0);
  runInfo->Fill ( 69, +450,  8,   8, 0, 0, 0);
  runInfo->Fill ( 68, -450,  8, 120, 0, 0, 0);
  runInfo->Fill ( 67,    0,  8, 119, 0, 0, 0);
  runInfo->Fill ( 66, +450,  8, 125, 0, 0, 0);  
  runInfo->Fill ( 65, -450,  8, 127, 0, 0, 0);
  // some -400 V, 8 mbar data before run 65

  TString plotOpt[10] = {"","same","same","same","same","same","same","same","same","same"};
  int histColor[10] = {1, 2, 4, 3,  6, 7, 8, 9, 11, 12}; // black, red, blue, green, yellow

  gSystem->Exec("source /home/muon/muCOOL/data/ana/update.sh");
  gStyle->SetOptStat (11);
  gStyle->SetOptFit (1111);
  gStyle->SetStatFontSize (0.05);

  TFile *f[200];
  TFolder *fol[200];
  TH1F *histAdd[numPlots][30];
  TH1F *time[numPlots][30];

  TCanvas *c1 = new TCanvas ("c1", "c1", 1800, 1200);
  c1->Divide (7, 4);
   
  //TF1 *bgfit = new TF1 ("bgfit", "[0]+[1]*exp(-x/2198.)", 10000, 11000);
  TF1 *bgfit = new TF1 ("bgfit", "[0]", 10500, 11000);
  bgfit->SetParameter (0,1);
  bgfit->SetParameter (1,10);

  double val = 1;
  int i = 0;
  int numAdd = 0;
  float totTime = 0;

  float run, volt, press, duration, cell, impurity, impPress;
  runInfo->SetBranchAddress("run",&run);
  runInfo->SetBranchAddress("volt",&volt);
  runInfo->SetBranchAddress("press",&press); 
  runInfo->SetBranchAddress("duration",&duration); 
  runInfo->SetBranchAddress("cell",&cell);
  runInfo->SetBranchAddress("impurity",&impurity);
  runInfo->SetBranchAddress("impPress",&impPress);

  for (int m = 0; m < numPlots; m++)
  {
    numAdd = 0;
    totTime = 0;
    for (int iInfo = 0; iInfo<runInfo->GetEntries(); iInfo++)
    {
      runInfo->GetEntry(iInfo);
 
      if (volt==plotCmd[m][0] && press==plotCmd[m][1] && cell==plotCmd[m][2] && impurity==plotCmd[m][3] && impPress==plotCmd[m][4])
      {
        numAdd += 1;
	totTime += duration;
        i = run;
        char name[50];
        sprintf (name, "../his/2014/mucool14_his_%4.4d.root", i);
        cout << name << endl;
        f[i] = new TFile (name);
        fol[i] = (TFolder *) f[i]->Get ("histos");

        for (int j = 2; j < 28; j++)
        {
          char name1[50];
          sprintf (name1, "hDecay%3.3d", j);
          //sprintf (name1, "hDecay1%2.2d", j);
          if (j==17) {
            //sprintf (name1, "hDecay027", j);
            sprintf (name1, "hDecay127", j);
          }
          if (j==27) {
            sprintf (name1, "T1T2Coin", j);
          }
      
	  if (numAdd == 1){
             histAdd[m][j] = (TH1F *) fol[i]->FindObjectAny (name1);
	  }
	  else {
	    histAdd[m][j]->Add( (TH1F *) fol[i]->FindObjectAny (name1) );
	  }
        }
      }
    }
    cout << "Added " << numAdd << " histograms for " << plotCmd[m][0] << " V / " << plotCmd[m][1] << " mbar / cell=" << plotCmd[m][2] << " / gas=" << plotCmd[m][3] << ", " << plotCmd[m][4] << " mbar. Total measurement time: " << totTime/60. << " h" << endl;
    
    for (int j = 2; j < 28; j++)
    {
      time[m][j] = new TH1F (histAdd[m][j]->GetName (), histAdd[m][j]->GetTitle (), 1200, -1000, 11000);

      // converting TDC bins to ns and fill the h[i][j] to time[i][j]          
      for (int k = 1; k < 66601; k++)
	{
	  time[m][j]->Fill ((k - 2061) * 0.1953125, histAdd[m][j]->GetBinContent (k));
	}

      c1->cd (j-1);
      //c1->cd (j)->SetLogy ();

      //Fit the early part of the spectra with a flat background
      //time[m][j]->Fit ("bgfit", "REMNQ");
      val = (time[m][j]->Integral (time[m][j]->FindBin(10800),time[m][j]->FindBin(10900)))/10.;

      // normalize all the time[i][j] with exp(-t/2.198) - muon lifetime
      for (int l = 1; l < 1201; l++)
	{
	  time[m][j]->SetBinContent (l,time[m][j]->GetBinContent (l) / exp (-time[m][j]->GetBinCenter (l) / 2198.));
	  //time[m][j]->SetBinContent (l,(time[m][j]->GetBinContent (l) - bgfit->GetParameter (0)) / exp (-time[m][j]->GetBinCenter (l) / 2198.));
	  //time[m][j]->SetBinContent (l,(time[m][j]->GetBinContent (l) - val) / exp (-time[m][j]->GetBinCenter (l) / 2198.));
	}
      
      time[m][j]->Rebin (8);
      val = time[m][j]->GetBinContent(time[m][j]->FindBin(normTime));
      //cout << "Normalization value: " << val << endl;
      time[m][j]->Scale(1/val);
      //time[m][j]->Scale(120/totTime);
      time[m][j]->SetLineColor(histColor[m]);
      time[m][j]->Draw (plotOpt[m]);
      time[m][j]->GetYaxis()->SetRangeUser(0,5);
      time[m][j]->GetXaxis()->SetRangeUser(-500,4000); 
    }
  }

  c1->cd(27);
  TLegend* legend = new TLegend(0.0, 0.0, 1.0, 1.0);
  legend->SetFillColor(0);
  for (int m = 0; m < numPlots; m++)
  {
    char cellStr[5];
    if (plotCmd[m][2]==0 ){
      sprintf(cellStr,"%s","short");
    }
    else {
       sprintf(cellStr,"%s","long");
    }
    char impStr[5];
    if (plotCmd[m][3]==0 ){
      sprintf(impStr,"%s","none");
    }
    else if(plotCmd[m][3]==1 ){
       sprintf(impStr,"%s","H2");
    } 
    else if(plotCmd[m][3]==2 ){
       sprintf(impStr,"%s","O2");
    }     
    char str[100];
    sprintf(str, "#splitline{%d V / %d mbar / %s}{%s %3.2f mbar}", plotCmd[m][0], plotCmd[m][1], cellStr, impStr, plotCmd[m][4]);
    legend->AddEntry(time[m][10],str,"l");
  }
  legend->SetLineColor(kWhite);
  legend->Draw();

}
