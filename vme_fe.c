/********************************************************************\

  Name:         vme_fe.c
  Created by:   Thomas Prokscha            21-Sep-2005,       PSI
                                           22-Jan-2010,       PSI

                Copyright (c) 2009 by Paul Scherrer Institut, 5232 Villigen PSI, Switzerland 

  Contents:     Midas frontend for periodic readout of the
                Struck SIS3820 VME multiscaler and test
                readout of a CAEN V1190B 64-channel multihit
                TDC.
                It uses the sis3820.c library which communicates with the VME
                module using a SIS3100/1100 VME-PCI interface.

                January 2010: extend to 64 channels to cope with new APD spectrometer.
                              Add red/green mode.

                For the V1190 readout:
                -----------------------
                #define IP          0    proton current
		#define CLOCK       1
		#define MCOUNTER    2
		#define POS01       3
		#define POS02       4
		#define POS03       5
		#define POS04       6
		#define POS05       7
		#define POS06       8
		#define POS07       9
		#define POS08      10
		#define POS09      11
		#define POS10      12
		#define POS11      13
		#define POS12      14
		#define POS13      15
		#define POS14      16
		#define POS15      17
		#define POS16      18
		#define POS17      19
		#define POS18      20
		#define POS19      21
		#define POS20      22
		#define POS21      23
		#define POS22      24
		#define POS23      25
		#define POS24      26
		#define POS25      27
		#define POS26      28
		#define POS27      29
		#define POS28      30
		#define POS29      31

                - read TDC in frontend_loop(), scan TDC output buffer for
                  possible events; call evaluate_event() if a candidate for a "good"
                  event is found
                - build events in evaluate_event(), put data into a ring buffer
                - poll on read/write pointer position in ring buffer; if positions
                  are different read data from ring buffer, else do nothing
                - 0.2ns resolution
                - set ALM_FULL bit to 1024 (register 0x1022)
                - check if ALM_FULL bit is set (0x2) (register 0x1002)
                - if yes read the TDC output buffer completely in BLT32 mode, if not skip
                - after read check if ALM_FULL bit is still set: if yes
                - repeat read, if not, return
                - one event types: Ev0 MCOUNTER-e+  (muSR setup)
                                   For all event types the e+ are now always active in TDC

      - if one adds a 1MHz clock one has to poll in order to
        get all events, but >80% CPU load, about 0.5% loss of data;
        -- reading periodically every millisec leads to a BUFFER FULL at TDC
        -- poll and ss_sleep(1ms): about 1% data loss due to output buffer full,
           low CPU load
      - 200kHz can be read periodically with 1ms period

      - For time-of-flight (TOF) measurements with the beam counter (BC) we have the problem
        that in a sequence of BC-MCP1/2/TD-e+ the "master" detector is after the BC and before the positrons.
        The easiest way to solve the problem of finding all correlated events in the software trigger is to
        have all signals a f t e r the master. This means that BC has to be delayed. This will be implemented
        after version 1718 from 14-Mar-2006, 15:37. As an example, let's have a look on BC-MCP1:
        -- all the pileup checking is done with the original TDC times
        -- to get a BC-MCP1 "trigger" (a valid TOF within the TOF gate):
           in original timing the corresponding detector flags are set if we have "clean" (no pre-pileup)
           signals. If the detector pattern requested by the Event definition is found start to "evaluate"
           the event, i.e. check if the timing between detector hits is ok. For this we will use the modified
           times
           t_BC   --> t'_BC   = t_BC + trigger_settings.tof_bc_window (delay BC time by TOF window length)
           t_MCP1 --> t'_MCP1 = t_MCP1 + trigger_settings.event_0_settings.mcp1_delay
           In evaluate_fast_muon() search the first t'_BC hit after t'_MCP1 which falls within the TOF window
           If this is a "clean" we have an event;
            to be continued...
      - further improvement to avoid losses of BC events:
        -- do not use time exceeding anymore; an event gets evaluated after the first "clean" (no pre-pileup)
           master detector (MCP1 or TD or MCP2) is found, and if additional event-specific detector hits are
           registered; in this case the event evaluation starts at the earliest at t = t_Master + t_dataWindow.
        -- keep reverse timing for BC

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <math.h>

//from /lib/modules/<KernelVersion>/build/include/linux
#include <include/linux/version.h>

#include "midas.h"
#include "mcstd.h"

#include "sis3820.h"
#include "v1190.h"

//! experiment specific ODB structures
#include "experim.h"       //!< created by ODBedit, command "make"

//! experiment specific definition for frontend and analyzer
#include "mucool_experim.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------- Globals --------------------------------*/

//! The frontend name (client name) as seen by other MIDAS clients
char *frontend_name = "VME_FE";

//! The frontend file name, don't change it
char *frontend_file_name = __FILE__;

//! frontend_loop is called periodically if this variable is TRUE
BOOL frontend_call_loop = TRUE;

//! a frontend status page is displayed with this frequency in ms
INT display_period = 3000;

//! maximum event size produced by this frontend
INT max_event_size = 128000;

//! maximum event size for fragmented events (EQ_FRAGMENTED)
INT max_event_size_frag = 5*1024*1024;

//! buffer size to hold events
INT event_buffer_size = 60*10000;

/* ----------------  ODB structures ---------------------------*/
//! The following structures are defined in experim.h that is
//! created by entering 'make' in ODBedit.
//!
//! ODB record values can be easily accessed by db_open_record
//! which creates a hot-link to the structure;
//! structure_name.keyvalue
//!
//EXP_PARAM        exp_param;        //!< /Experiment/Run parameters
TRIGGER_SETTINGS trigger_settings; //!< /Equipment/Trigger/Settings
SCALER_SETTINGS  scaler_settings;  //!< /Equipment/Scaler/Settings/
//!
//! Not defined in experim.h:
//!
RUNINFO          runinfo;          //!< /Runinfo ODB key, defined in from midas.h

//! Local definitions to connect to VME module
#define SIS3820_ADDRESS_0 0x38000000     //!< VME address of 1st scaler module
#define SIS3820_ADDRESS_1 0x39000000     //!< VME address of 2nd scaler module
#define V1190_ADDRESS     0xEE000000     //!< the VME address of the CAEN V1190 TDC
#define MAX_NUMBER_LWORDS 1024           //!< max number of block data transfer */
#define ALMOST_FULL_LEVEL 1024
#define DATABUFFER_SIZE   131072         //!< set size of FiFo (ring) buffer, 2^17
#define DIFF_TOLERANCE    -12200         //!< this is the allowed TDC time difference to the preceding event
                                         //!< 12200 ~ 524288 - 512000 (2^19-1/10kHz)
//! Event flags
#define MCOUNTER_EVENT    (1<<0)
#define POSITRON_EVENT    (1<<1)
// #define M2_EVENT          (1<<2)
// #define BC_EVENT          (1<<3)
// #define M1_EVENT          (1<<4)
// #define M1_POSITRON_EVENT (1<<5)

//! timing flags
#define MASTERLAST         1   //! the "master" detector (MCP1, TD/MCP2) is the latest event
#define MASTERFIRST        2   //! the "master" detector is the first of all events; useful for BC-MCP1 TOF

//! other global variables
//! -----------------------
//! the following structure keeps event information to
//! avoid loss of data due a reset in the TDC read buffer:
//! the TDC read buffer holds 32768 time data; when it
//! restarts at index 0 there usually are data at indices
//! 32768-i from the previous reading; the TDC_EVAL_EVENT
//! structure is used to keep this information
typedef struct {
 INT   eventType;
 INT   eventSize;
 INT   clockIndex;
 INT   tdc_data[N_TDC_CHANNELS*DATA_N_HITS];   //!< this array is actually the Midas event
 INT   set_t0;                            //!< flag indicating if t0 has been set
 INT   event_time;                        //!< absolute time in event after reset of structure
 INT   foundChannel[N_TDC_CHANNELS];      //!< count appearance of channel i
 INT   tdc_event[N_TDC_CHANNELS][N_HITS]; //!< temporary storage for TDC data when scanning output buffer
                                          //!< each channel may have up to N_HITS hits
 INT   pileup[N_TDC_CHANNELS][N_HITS];    //!< save information for each hit if it had a pre pileup or not
 INT   indexMuon, indexMuonSet;
 DWORD thisTimeMuon, lastTimeMuon;
 DWORD doEvaluateMuonEvent;     //!< bit pattern for trigger condition:
                                //!< MCOUNTER = 0x01, Pos = 0x02, M2 = 0x04, BC = 0x08
} TDC_EVAL_EVENT;

typedef struct{
 U_LONG endMasterEvent0;        //!< U_LONG==unsigned long long, GNU C-compiler 64bit integer
 INT    lastMasterEvent0Pileup;
} TDC_PILEUP;

static VME_STATS      vme_stats;
static TDC_EVAL_EVENT tdc_eval_event;
static TDC_PILEUP     tdc_pileup;
static INT            hdev;           //!< handle for VME-PCI device
static DWORD   *databuffer;           //!< ring buffer for TDC data
static DWORD   *p_read, *p_write;     //!< read and write pointers for data ring buffer
static DWORD   eventDefinedMuon;      //!< bit pattern containing trigger mode muon decay (MCounter-Positron)
static U_LONG  runTime;               //!< absolute run time in TDC LSB units
static U_LONG  nextGoodTime;          //!< absolute run time when new events are accepted (for on/off mode)
static DWORD   dataWindowMuon;
static INT     t0_offset;
static BOOL    flag_scaler_address_0, flag_scaler_address_1; //!< flags to indicate status of VME scalers
INT            mode;                  //!< scaler mode, see scaler_mode()
static DWORD   lasttime;

/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
INT read_trigger_event(char *pevent, INT off);
INT poll_event(INT source, INT count, BOOL test);
INT read_scaler_event(char *pevent, INT off);
INT do_channel(INT ch, INT time);
INT do_master_channel(INT ch);
U_LONG pileup_end(INT pileupWindow);
INT    evaluate_muon_event();
BOOL   check_tof_and_pileup(INT timing, INT tofWindow, INT firstCh, INT lastCh, INT masterChannel, INT masterIndex,
                            INT *channel, INT *ind);
BOOL   check_positron(INT decWindow, INT coincWindow, INT firstCh, INT lastCh, INT masterChannel, INT masterIndex,
                      INT *channel, INT *ind);

void scaler_mode(u_int32_t module_address);
int  init_sis3820(u_int32_t module_address);
int  init_v1190(u_int32_t module_address);
//#define TESTOUT
#ifdef TESTOUT
FILE *fp;
#endif

/*-- Equipment list ------------------------------------------------*/
#undef USE_INT

EQUIPMENT equipment[] = {

  { "Trigger",            /* equipment name */
   { 1, 0,                 /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_POLLED,            /* equipment type */
    LAM_SOURCE(0,0xFFFFFF),/* event source crate 0, all stations */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    RO_RUNNING | RO_ODB,  /* read only when running */
    100,                  /* poll 100ms */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub events */
    0,                    /* don't log history */
    "", "", "",},
    read_trigger_event,   /* readout routine */
  },

  { "Scaler",             /* equipment name */
    { 2, 0,               /* event ID, trigger mask */
    "SYSTEM",             /* event buffer */
    EQ_PERIODIC,          /* equipment type, MAN trigger disabled */
    0,                    /* event source */
    "MIDAS",              /* format */
    TRUE,                 /* enabled */
    RO_ALWAYS |
    RO_TRANSITIONS |      /* read when running and on transitions */
    RO_ODB,               /* and update ODB */
    3000,                 /* read every 3 sec */
    0,                    /* stop run after this event limit */
    0,                    /* number of sub events */
    0,                    /* log history */
    "", "", "",},
    read_scaler_event,    /* readout routine */
  },

  { "" }
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.

\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
 HNDLE hDB, hkey;
 char  vme_device[32];
 EXP_PARAM_STR(exp_param_str);
 TRIGGER_SETTINGS_STR(trigger_settings_str);
 SCALER_SETTINGS_STR(scaler_settings_str);
 VME_STATS_STR(vme_stats_str);
 // ------------------------ ODB ------------------------------------------
 //
 //    open (or create, if not exist) ODB structures;
 //
   cm_get_experiment_database(&hDB, NULL);

   db_create_record(hDB, 0, "/Experiment/Run Parameters", strcomb(exp_param_str));
   db_create_record(hDB, 0, "/Equipment/Trigger/Settings", strcomb(trigger_settings_str));
   db_create_record(hDB, 0, "/Equipment/Scaler/Settings", strcomb(scaler_settings_str));
   db_create_record(hDB, 0, "/Equipment/Trigger/Vme_Statistics", strcomb(vme_stats_str));
//    db_create_record(hDB, 0, "/Equipment/Beamline/Settings", strcomb(beamline_settings_str));
//    db_create_record(hDB, 0, "/Equipment/Beamline/Variables", strcomb(beamline_event_str));

   //! create hot links to ODB structures
   if ( db_find_key(hDB, 0, "/Equipment/Trigger/Settings", &hkey))
     db_open_record(hDB, hkey, &trigger_settings, sizeof(trigger_settings), MODE_READ, NULL, NULL);

   if ( db_find_key(hDB, 0, "/Equipment/Trigger/Vme_Statistics", &hkey))
     db_open_record(hDB, hkey, &vme_stats, sizeof(vme_stats), MODE_WRITE, NULL, NULL);

   if ( db_find_key(hDB, 0, "/Runinfo", &hkey))
     db_open_record(hDB, hkey, &runinfo, sizeof(runinfo), MODE_READ, NULL, NULL);

   memset(&vme_stats, 0x00, sizeof(vme_stats));
   memset(&tdc_eval_event, 0x00, sizeof(tdc_eval_event));
   memset(&tdc_pileup, 0x00, sizeof(tdc_pileup));
   databuffer = malloc(DATABUFFER_SIZE * sizeof(DWORD));//allocate memory for data buffer
   p_read  = &databuffer[0];
   p_write = &databuffer[0];
   eventDefinedMuon = 0xffff;
   flag_scaler_address_0 = TRUE;
//    flag_scaler_address_1 = TRUE;

   /*--- VME initialization ---*/
#ifdef HAVE_TEST_RUN
   hdev = 0;
#else
   cm_msg(MINFO, "frontend_init","LINUX_VERSION = 0x%x",LINUX_VERSION_CODE);
   cm_yield(0);
   if ( LINUX_VERSION_CODE < 0x20600 ) // for sis1100 drivers < V2, Linux kernel 2.4
     sprintf(vme_device, "/dev/sis1100");
   else                                // for sis1100 driver V2.02, Linux kernel 2.6.9, V2.04 for kernel 2.6.18
     sprintf(vme_device, "/dev/sis1100_00remote");

   if ( ( hdev = open(vme_device, O_RDWR, 0)) < 0 )
   {
     cm_msg(MERROR, "frontend_init", "Error on opening VME device %s", vme_device);
     cm_yield(0);
     return FE_ERR_HW;
   }
#endif

   //SIS3820 scaler initialization
   if ( init_sis3820(SIS3820_ADDRESS_0) == FE_ERR_HW)
        flag_scaler_address_0 = FALSE;

//    if ( init_sis3820(SIS3820_ADDRESS_1) == FE_ERR_HW)
        flag_scaler_address_1 = FALSE;

   cm_msg(MINFO,"frontend_init","scaler flags, address 0x%8.8x = %d, 0x%8.8x = %d", 
   SIS3820_ADDRESS_0, flag_scaler_address_0, SIS3820_ADDRESS_1, flag_scaler_address_1);
   cm_yield(0);

   // V1190 TDC initialisation
   if ( init_v1190(V1190_ADDRESS) == FE_ERR_HW)
        return FE_ERR_HW;

   lasttime              = ss_millitime();
   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
 int status;

  /* reset SIS3820 */
  if ( flag_scaler_address_0){
   status = sis3820_key_reset( hdev, SIS3820_ADDRESS_0);
   if ( status !=0){
        cm_msg(MERROR, "frontend_exit",
                       "Error on access to SIS3820_0, key reset failed, return code %x\n", status);
        cm_yield(0);
   }
  }
/*  if ( flag_scaler_address_1){
   status = sis3820_key_reset( hdev, SIS3820_ADDRESS_1);
   if ( status !=0){
        cm_msg(MERROR, "frontend_exit",
                       "Error on access to SIS3820_1, key reset failed, return code %x\n", status);
        cm_yield(0);
   }
  }*/
  /* close VME device */
  if ( hdev >= 0 )
        close (hdev);

  free (databuffer);

  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  int    status, i;
  WORD   ch_pattern[4];
  DWORD  pattern_1stChip, pattern_2ndChip;

  // ------------------------------------------------------------
  // on TDC enable all channes except channel with Ip (proton current)
  //
  memset(&ch_pattern, 0x00, sizeof(ch_pattern));
  pattern_1stChip = pattern_2ndChip = 0;
 
  for (i = 0; i<N_TDC_CHANNELS; i++){
    if (i == IP) continue; 
    if (i < 32)
      pattern_1stChip = pattern_1stChip | (1<<i); //clock always enabled
    else
      pattern_2ndChip = pattern_2ndChip | (1<<(i%32));
  }

  cm_msg(MINFO,"begin_of_run","pattern_1stChip = 0x%08x", pattern_1stChip);
  cm_msg(MINFO,"begin_of_run","pattern_2ndChip = 0x%08x", pattern_2ndChip);
  cm_yield(0);
  ch_pattern[0] = pattern_1stChip & 0xFFFF;
  ch_pattern[1] = (pattern_1stChip & 0xFFFF0000)>>16;
  ch_pattern[2] = pattern_2ndChip & 0xFFFF;
  ch_pattern[3] = (pattern_2ndChip & 0xFFFF0000)>>16;

#ifndef HAVE_TEST_RUN
  status = vme_A32D16_write(hdev, V1190_ADDRESS+V1190_OPCODE_ADDRESS, 0x4600); // pattern for 1st TDC chip
  for ( i = 0; i<2; i++){
   ss_sleep(20);
   status = vme_A32D16_write(hdev, V1190_ADDRESS+V1190_OPCODE_ADDRESS, ch_pattern[i]);
   cm_msg(MINFO, "begin_of_run", "V1190 1st Chip channel pattern:  pattern = 0x%04x", ch_pattern[i]);
   cm_yield(0);
  }
  if ( status != 0 ){
   cm_msg(MERROR, "begin_of_run", "Error writing channel pattern to TDC V1190, return code %x\n", status);
   cm_yield(0);
   return FE_ERR_HW;
  }

  ss_sleep(50);
  status = vme_A32D16_write(hdev, V1190_ADDRESS+V1190_OPCODE_ADDRESS, 0x4601); // pattern for 2nd TDC chip
  for ( i = 2; i<4; i++){
   ss_sleep(20);
   status = vme_A32D16_write(hdev, V1190_ADDRESS+V1190_OPCODE_ADDRESS, ch_pattern[i]);
   cm_msg(MINFO, "begin_of_run", "V1190 2nd Chip channel pattern:  pattern = 0x%04x", ch_pattern[i]);
   cm_yield(0);
  }
  if ( status != 0 ){
   cm_msg(MERROR, "begin_of_run", "Error writing channel pattern to TDC V1190, return code %x\n", status);
   cm_yield(0);
   return FE_ERR_HW;
  }

  ss_sleep(20);

  // -----------------------------------------------------------------------
  // clear TDC
  //
  status = vme_A32D16_write(hdev, V1190_ADDRESS+V1190_SOFTWARE_CLEAR, 0x0);
  if ( status !=0){
        cm_msg(MERROR, "begin_of_run",
         "Error on resetting TDC V1190, return code %x\n", status);
        cm_yield(0);
        return FE_ERR_HW;
  }
#endif
  // -----------------------------------------------------------------------
  // reset 1st Scaler module SIS3820_0
  //
  // disable SIS3820
  if ( flag_scaler_address_0){
    status = sis3820_key_disable( hdev, SIS3820_ADDRESS_0);
    if ( status !=0){
        cm_msg(MERROR, "begin_of_run",
               "Error on access to SIS3820_0, key disable failed, return code %x\n", status);
        cm_yield(0);
        return FE_ERR_HW;
    }
    // reset SIS3820
    status = sis3820_key_reset( hdev, SIS3820_ADDRESS_0);
    if ( status !=0){
        cm_msg(MERROR, "begin_of_run",
               "Error on access to SIS3820_0, key reset failed, return code %x\n", status);
        cm_yield(0);
        return FE_ERR_HW;
    }
    // check/change scaler mode
    scaler_mode(SIS3820_ADDRESS_0);
    // enable SIS3820
    status = sis3820_key_enable( hdev, SIS3820_ADDRESS_0);
    if ( status !=0){
        cm_msg(MERROR, "begin_of_run",
         "Error on access to SIS3820_0, key enable failed, return code %x\n", status);
        cm_yield(0);
        return FE_ERR_HW;
    }
  }
  // -----------------------------------------------------------------------
  // reset 2nd Scaler module SIS3820_1
  //
  // disable SIS3820
//   if ( flag_scaler_address_1){
//     status = sis3820_key_disable( hdev, SIS3820_ADDRESS_1);
//     if ( status !=0){
//         cm_msg(MERROR, "begin_of_run",
//                "Error on access to SIS3820_1, key disable failed, return code %x\n", status);
//         cm_yield(0);
//         return FE_ERR_HW;
//     }
//     // reset SIS3820
//     status = sis3820_key_reset( hdev, SIS3820_ADDRESS_1);
//     if ( status !=0){
//         cm_msg(MERROR, "begin_of_run",
//                "Error on access to SIS3820_1, key reset failed, return code %x\n", status);
//         cm_yield(0);
//         return FE_ERR_HW;
//     }
//     // check/change scaler mode
//     scaler_mode(SIS3820_ADDRESS_1);
//     // enable SIS3820
//     status = sis3820_key_enable( hdev, SIS3820_ADDRESS_1);
//     if ( status !=0){
//         cm_msg(MERROR, "begin_of_run",
//          "Error on access to SIS3820_1, key enable failed, return code %x\n", status);
//         cm_yield(0);
//         return FE_ERR_HW;
//     }
//   }

  // event definition
  eventDefinedMuon      = 0x8000;
  t0_offset             = trigger_settings.t0_offset;
  dataWindowMuon        = trigger_settings.data_window;
  if (trigger_settings.positrons_active)
     eventDefinedMuon   = MCOUNTER_EVENT | POSITRON_EVENT;
  else
     eventDefinedMuon   = MCOUNTER_EVENT;

  cm_msg(MINFO,"begin_of_run","Muon event_defined = 0x%04x, master channel muon= %d",
         eventDefinedMuon, MCOUNTER);
  cm_yield(0);

  // reset array and pointers
  memset(&vme_stats, 0x00, sizeof(vme_stats));
  memset(&tdc_eval_event, 0x00, sizeof(tdc_eval_event));
  memset(&tdc_pileup, 0x00, sizeof(tdc_pileup));
  memset(databuffer, 0x00, sizeof(databuffer));
  p_read   = &databuffer[0]; // set read data pointer to beginning of data buffer
  p_write  = &databuffer[0]; // set write data pointer to beginning of data buffer
  runTime      = t0_offset;
  nextGoodTime = 0;
  vme_stats.last_TDCtime = -1; //signal that last_TDCime has not been set
#ifdef TESTOUT
  fp = fopen( "/data/MuCool/fe.log", "w");
  if ( fp == NULL ) return FE_ERR_HW;
#endif
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
 INT return_code;

  /* disable all channels if set in ODB */
  if ( trigger_settings.tdc_disable_atstartup ){
   return_code = vme_A32D16_write(hdev, V1190_ADDRESS+V1190_OPCODE_ADDRESS, V1190_DISABLE_ALL_CHANNEL);
   cm_msg(MINFO, "end_of_run", "V1190 Disabled all channels:   return_code = 0x%08x", return_code);
   cm_yield(0);
  }

  cm_msg(MINFO, "end_of_run","total number of written muon events      = %15.0f", vme_stats.MuonEvents);
  cm_msg(MINFO, "end_of_run","total number of read events              = %15.0f", vme_stats.readcounts);
  cm_msg(MINFO, "end_of_run","TDC error counts                         = %15.0f", vme_stats.tdc_error_counts);
  cm_msg(MINFO, "end_of_run","p_read = 0x%x, p_write = 0x%x", p_read, p_write);
  cm_msg(MINFO, "end_of_run","total Run time in TDC bins (double)      = %18.0f", vme_stats.run_time);
  cm_msg(MINFO, "end_of_run","total Run time 10kHz clock               = %18.5f", vme_stats.channelCounts[1]/10000.);
  cm_yield(0);
#ifdef TESTOUT
  fclose(fp);
#endif

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
  int status;

  // clear TDC
  status = vme_A32D16_write(hdev, V1190_ADDRESS+V1190_SOFTWARE_CLEAR, 0x0);
  if ( status !=0){
        cm_msg(MERROR, "resume_run",
         "Error on resetting TDC V1190, return code %x\n", status);
        cm_yield(0);
        return FE_ERR_HW;
  }

  if ( flag_scaler_address_0){
   sis3820_key_disable( hdev, SIS3820_ADDRESS_0);
   sis3820_key_reset( hdev, SIS3820_ADDRESS_0);
   sis3820_key_enable( hdev, SIS3820_ADDRESS_0);
  }
/*  if ( flag_scaler_address_1){
   sis3820_key_disable( hdev, SIS3820_ADDRESS_1);
   sis3820_key_reset( hdev, SIS3820_ADDRESS_1);
   sis3820_key_enable( hdev, SIS3820_ADDRESS_1);
  }*/
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

/*!
 * <p><b>frontend_loop()</b> is called whenever the frontend is idle
 *    or once between every event if the variable
 *    <b><i>frontend_call_loop = true </i></b> in the
 *    frontend code. This means that the frontend has to be
 *    re-compiled if one wants to use <b>frontend_loop()</b>.
 *
 * <p>It performs the readout of the V1190 TDC and builds the events
 *    to be sent in read_trigger_event();
 *
 *    A 10kHz clock is used to check if hits belong to one
 *    TDC cycle of 100musec. Events may look like
 *
 *    1-1-1-4-1-1 = clock-clock-clock-MCOUNTER-clock-clock --> no valid events
 *    1-4-4-1-1    = clock-MCOUNTER-MCOUNTER-clock-clock --> no valid event
 *    1-26-27-30-29-28-31-1 = clock-M2F-M2R-Y1-X2-X1-Y2 --> no valid event, MCOUNTER missing
 *    4-26-27-28-29-30-31-1 --> valid event
 *    4-4-26-27-28-29-30-31-1 --> valid event
 *    4-26-27-28-29-30-31-26-27-28-29-30-31-4 --> valid event, pileup times are sent with data buffer
 *
 *    Read complete TDC output buffer of 32k events. At low rates a VME bus error
 *    (return code = 0x211) indicates the end of data. This stops reading data from
 *    the TDC. At high rates (MHz) this does not work anymore since there are always
 *    data present in the output buffer. In this case a TDC
 *    TDC error may occur (bit 29 is set) that can be used to tag the start of new
 *    data.
 *
*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  u_int16_t  data;
  u_int32_t  blt_data[MAX_NUMBER_LWORDS] ;
  u_int32_t  *output_buffer; //[V1190_OUTPUT_BUFFER_SIZE];
  u_int32_t  get_lwords, sum_lwords;
  INT        return_code;
  INT        loop_counter, i, j, k;
  INT        tdc_time, channel, diffTimeMuon, difftime;
  V1190_DATA *v1190_data;

#ifdef HAVE_TEST_RUN
  return SUCCESS;
#endif

  if ( runinfo.state != STATE_RUNNING ) // don't read TDC if we are not running
        return SUCCESS;

  vme_stats.frontend_loop_counts++; // for test purposes only
  diffTimeMuon = 0;

if ( !trigger_settings.simulation_flag ){
  //------------------------------------------------------------------------------------
  // check fill level of TDC output buffer; return if ALMOST_FULL_BIT is not yet set
  //
  return_code =  vme_A32D16_read(hdev, V1190_ADDRESS+V1190_STATUS_REGISTER, &data );
  if ( !(data & V1190_ALMFULL_BIT)) return SUCCESS;

  vme_stats.frontend_loop_readtdc++;
  //cm_msg(MINFO, "frontend_loop","p_read = 0x%x, p_write = 0x%x", p_read, p_write);
}
  //--------------------------------------------------------------------------------------
  // read complete TDC output buffer; break if no more data are present in output buffer
  //
  sum_lwords = 0;
  output_buffer = malloc(V1190_OUTPUT_BUFFER_SIZE * sizeof(u_int32_t));

// ----------------------------
// simulate pure, uncorrelated background
//
if ( trigger_settings.simulation_flag ){
#define BINNING 0.1953125 // TDC bin in ns
  double rate[4], ran; // rates 1/ns
  U_LONG time[4];
  DWORD  mincounter, tdctime; // times in TDC channels, assume 0.1953125ns LSB

  rate[0] = 1.e-5; // 10kHz clock
  rate[1] = trigger_settings.simulation_startrate;  // counts/ns
  rate[2] = trigger_settings.simulation_stoprate;   // counts/ns
  rate[3] = trigger_settings.simulation_stoprate;   // counts/ns
  ss_sleep(10);
  memset(&time, 0x00, sizeof(time));
  sum_lwords = V1190_OUTPUT_BUFFER_SIZE/2;
  for ( i=0; i<sum_lwords; i++ ){
    if ( i == 0 ){
        ran = (double) rand()/ (double) RAND_MAX;
        time[0] = ran*1.e+5/BINNING; // next occurence of 10kHz
        for (j=1; j<4; j++){
            ran = (double) rand()/ (double) RAND_MAX;
            time[j] = (-1./rate[j]*log(1.- ran))/BINNING;
        }
    }

    for ( j = 0; j < 4; j++){// find the next event
      mincounter = 0;
      for ( k =0; k < 4; k++){
       if ( j!=k && time[j] <= time[k]) mincounter++;
     }
      if ( mincounter == 3) break;
    }

    switch (j){
     case 0:
        channel = CLOCK;
        tdctime = time[0]%TDC_BIT_RANGE;
        tdctime = tdctime | (channel<<19);
        time[0] = time[0] + (U_LONG) 1.e+5/BINNING; // the next 10kHz clock time
        break;

     case 1:
        channel = MCOUNTER;
        tdctime = time[1]%TDC_BIT_RANGE;
        tdctime = tdctime | (channel<<19);
        ran = (double) rand()/(double) RAND_MAX;
        time[1] = time[1] + (U_LONG) (-1./rate[j]*log(1. - ran))/BINNING;;
        break;

     case 2:
        channel = POS01;
        tdctime = time[2]%TDC_BIT_RANGE;
        tdctime = tdctime | (channel<<19);
        ran = (double) rand()/(double) RAND_MAX;
        time[2] = time[2] + (U_LONG) (-1./rate[j]*log(1. - ran))/BINNING;;
        break;

     case 3:
        channel = POS02;
        tdctime = time[3]%TDC_BIT_RANGE;
        tdctime = tdctime | (channel<<19);
        ran = (double) rand()/(double) RAND_MAX;
        time[3] = time[3] + (U_LONG) (-1./rate[j]*log(1. - ran))/BINNING;;
        break;

     default:
        break;
    }
    //fprintf(fp,"i=%6d, times = %12u %12u %12u %12u\n",i,time[0],time[1],time[2],time[3]);
    output_buffer[i] = tdctime;
  }
}
// -----------------------------
// read TDC data
else {
  for ( i = 0; i < 32; i++){
   return_code =  vme_A32BLT32_read(hdev, V1190_ADDRESS, blt_data, MAX_NUMBER_LWORDS, &get_lwords);
   memcpy(output_buffer + sum_lwords, &blt_data, get_lwords * sizeof(u_int32_t));
   sum_lwords += get_lwords;
   if ( return_code == 0x211) // VME Bus error indicating that output buffer is empty
        break;                // this does not work at rates > 1MHz; new events in output buffer
  }
}
  //-----------------------------------------------------------------------------------------
  // scan output buffer and search events; if a possible event is found evaluate_event()
  // is executed
  //
  for ( i = 0; i < sum_lwords; i++ ){
    v1190_data = (V1190_DATA *) &output_buffer[i];
    tdc_time  = v1190_data->data;
    channel   = v1190_data->channel;
    if ( vme_stats.last_TDCtime == -1 ) //at BOR last_TDCtime is set to -1
      vme_stats.last_TDCtime = tdc_time;

    if ( v1190_data->buffer == 4 ){ //this means a TDC error occured, bit 29 is set; this can happen
                                    //if input rate is > 1MHz; in this case the error condition
                                    //above (return_code == 0x211) does not work
#ifdef TESTOUT
        fprintf(fp,"found TDC error\n");
#endif
        vme_stats.tdc_error_counts++;
        memset(&tdc_eval_event, 0x00, sizeof(tdc_eval_event));  // reset structure for next event
        continue;
    }

    if ( tdc_eval_event.foundChannel[channel] > N_HITS-1 ){     // didn't yet find an event
#ifdef TESTOUT
        fprintf(fp,"Event reset due to channel %d overflow\n", channel);
#endif
        memset(&tdc_eval_event, 0x00, sizeof(tdc_eval_event));  // reset structure for next event to avoid
        continue;                                               // buffer overflow
    }

    vme_stats.channelCounts[channel]++;
    do_channel(channel, tdc_time); //update channel statistics, copy TDC times in temporary buffer 
                                   //tdc_event[ch][index] to be used in evaluate_event():

    //check pre-pileups for "master" channels, set event flags, add delays to TDC times
    switch (channel){
     // the "master" detector, muon counter
     case MCOUNTER:
      //add time delay
      tdc_eval_event.tdc_event[MCOUNTER][tdc_eval_event.foundChannel[MCOUNTER]-1] +=   // incremented by do_channel()
        trigger_settings.mcounter_delay;
      do_master_channel(MCOUNTER); //to determine pre-pileup

      // set detector flag only if "clean" signal, i.e. no pre-pileup, and if event enabled
      // in case of MCOUNTER not used as "master" tdc_eval_event.pileup[MCOUNTER][j]=0 for all j
      if ( tdc_eval_event.pileup[MCOUNTER][tdc_eval_event.foundChannel[MCOUNTER]-1] == 0 &&
           (eventDefinedMuon & MCOUNTER_EVENT) ) // incremented by do_channel()
          tdc_eval_event.doEvaluateMuonEvent = tdc_eval_event.doEvaluateMuonEvent | MCOUNTER_EVENT;
      break;
     case POS01: case POS02: case POS03: case POS04: case POS05: case POS06: case POS07: case POS08:
     case POS09: case POS10:
     case POS11: case POS12: case POS13: case POS14: case POS15: case POS16: case POS17: case POS18:
     case POS19: case POS20:
     case POS21: case POS22: case POS23: case POS24: case POS25: case POS26: case POS27: case POS28:
     case POS29:
      tdc_eval_event.tdc_event[channel][tdc_eval_event.foundChannel[channel]-1] +=
         trigger_settings.positron_delay;

      if ( tdc_eval_event.foundChannel[channel] == 1 && (eventDefinedMuon & POSITRON_EVENT) )
           tdc_eval_event.doEvaluateMuonEvent |= POSITRON_EVENT;
      break;

     case CLOCK:
      break;

     default:
      break;
  }

  diffTimeMuon = tdc_eval_event.thisTimeMuon - tdc_eval_event.lastTimeMuon;
  // 28-Mar-2006: the following corrections on diffTimeFast/SlowMuon are still needed,
  // -----------  since tdc_eval_event.t0 is obsolete
  //
  // the following is needed to avoid segmentation fault of the frontend in case of
  // enabled events 0 and 1 (M1,TD/M2); due to non-ascening order of time stamps in
  // TDC it may happen that we have a new event like
  // 1 (clock)  336 = t0  event time 0
  // 2 (MCP1)   312       event time -24 --> 524264, difftime 0
  // 2 (MCP1)  1012       event time 676 --> difftime to last MCP1 -523588 instead of 700
  // in this case the MCP1
  //
  if ( diffTimeMuon < 0 && abs(diffTimeMuon) >= t0_offset)
     diffTimeMuon += TDC_BIT_RANGE;
  else if ( diffTimeMuon < 0 && abs(diffTimeMuon) < t0_offset )
     diffTimeMuon += t0_offset;

#ifdef TESTOUT
  fprintf(fp,
   "raw data = %6d %3d %7d 0x%08x, vmeTime = %10.5e, difftime = %8d, ev.time = %8d, pu-flag = %2d, 0x%04x\n",
    i, channel, tdc_time, output_buffer[i],
    vme_stats.run_time,
    diffTimeMuon, tdc_eval_event.event_time,
    tdc_eval_event.pileup[channel][tdc_eval_event.foundChannel[channel]-1],
    tdc_eval_event.doEvaluateMuonEvent);
#endif

   if ( (tdc_eval_event.doEvaluateMuonEvent == eventDefinedMuon) &&
        (diffTimeMuon > (dataWindowMuon + t0_offset)) ){
    evaluate_muon_event();
    i--;                                // we have to check if the current hit is a candidate for a new event
    vme_stats.channelCounts[channel]--; // avoid double counting
   }
   else if (channel == CLOCK ){
      //check for successice clock events; if present reset tdc_eval_event structure
      if ( i - tdc_eval_event.clockIndex == 1 && i !=1 ){
       memset(&tdc_eval_event, 0x00, sizeof(tdc_eval_event));
       //continue;
      }
      if ( i == sum_lwords - 1)          // two handle successive clock events correctly when new
         tdc_eval_event.clockIndex = -1; // output buffer is scanned
      else
         tdc_eval_event.clockIndex = i;
   }

   // removed all the time exceeding handling which appeared here at versions <= 1738
  }

  free(output_buffer);

  return SUCCESS;
}
/*-----------------------------------------------------------------------------------------*/
/*!
 * do_channel(INT ch, INT time):
 *
 * update channel statistics, copy TDC times in temporary buffer
 * tdc_event[ch][index] to be used in evaluate_event():
*/
INT do_channel(INT ch, INT TDCtime)
{
 INT diff_toLastEvent;

 //introduce a trigger_settings.t0_offset to handle events like:
 //  MCP1  380
 //  e+_I  370
 //  e+_O  375
 // without special handling this would lead to a diff_time = diff_time + TDC_BIT_RANGE
 // which is a big number and can be greater than the data gate - in which case the event
 // would be rejected
 if ( tdc_eval_event.set_t0 == 0) { //a new event evaluation has started;
    tdc_eval_event.event_time = t0_offset;
    tdc_eval_event.set_t0++;
 }

 diff_toLastEvent = TDCtime - vme_stats.last_TDCtime;
 if ( diff_toLastEvent < DIFF_TOLERANCE )
      diff_toLastEvent += TDC_BIT_RANGE; //only add TDC_BIT_RANGE if time difference is > abs(DIFF_TOLERANCE)
                                         //allow for "small" negative time differences, i.e. for event sequences
                                         //as shown above
 vme_stats.last_TDCtime     = TDCtime;
 tdc_eval_event.event_time += diff_toLastEvent;
 //calculate the absolute run time in TDC LSB units
 runTime  += (U_LONG) diff_toLastEvent;
 vme_stats.run_time  = (double) runTime; //copy to ODB structure

 tdc_eval_event.thisTimeMuon = tdc_eval_event.thisTimeMuon = tdc_eval_event.event_time;

 tdc_eval_event.tdc_event[ch][tdc_eval_event.foundChannel[ch]] = tdc_eval_event.event_time;
 tdc_eval_event.foundChannel[ch]++;

 return SUCCESS;
}
/*-----------------------------------------------------------------------------------------*/
INT do_master_channel(INT ch)
{
  //check pre-pileup
  //the following works because we're using unsigned integers:
  //for a clean signal, tdc_pileup.endMasterEvent0 - runTime < 0, but this becomes
  //"a large number > 0" if we are dealing with unsigned integers only.
   if ( (tdc_pileup.endMasterEvent0 - runTime) < (U_LONG) dataWindowMuon ) // we had a pre-pileup
        tdc_eval_event.pileup[ch][tdc_eval_event.foundChannel[ch]-1] = 1;
   else if ( (tdc_pileup.endMasterEvent0 - runTime) > (U_LONG) dataWindowMuon){ // this is a "clean" signal
        vme_stats.mcounter_clean++;                                                 // i.e. no pre-pileup
        vme_stats.mcounter_good++; //this could be a potential "good" event
        if (tdc_eval_event.doEvaluateMuonEvent != eventDefinedMuon ){
            tdc_eval_event.lastTimeMuon         = tdc_eval_event.thisTimeMuon;
            tdc_eval_event.indexMuon            = tdc_eval_event.foundChannel[ch]-1;
            tdc_eval_event.indexMuonSet++;
        }
   }
   else{ // this means tdc_pileup.endMasterEvent0 - runTime = dataWindowMuon; event "re-evaluated"
        tdc_eval_event.pileup[ch][tdc_eval_event.foundChannel[ch]-1] = tdc_pileup.lastMasterEvent0Pileup;
        // for mcounter_good event determination across two tdc_eval_event's
        if (tdc_pileup.lastMasterEvent0Pileup == 0) tdc_eval_event.indexMuonSet++;
   }

   // check for post-pileup to count "mcounter_good" event (no pre- and post-pileup)
   // - check, if we had a pile-up
   // - then check, if previous event was a clean event; in this case lower mcounter_good;
   //   in case of subsequent pileup events don't decrease mcounter_good
   // - the "tdc_eval_event.indexMuonSet > 0" condition is needed to get the correct
   //   counting across two event evaluations
   if (tdc_eval_event.pileup[ch][tdc_eval_event.foundChannel[ch]-1] == 1){
    if ((tdc_eval_event.foundChannel[ch]-1 - tdc_eval_event.indexMuon == 1) &&
         tdc_eval_event.indexMuonSet > 0)
         vme_stats.mcounter_good--;
   }
   tdc_pileup.lastMasterEvent0Pileup = tdc_eval_event.pileup[ch][tdc_eval_event.foundChannel[ch]-1];
   tdc_pileup.endMasterEvent0        = pileup_end(dataWindowMuon);

#ifdef TESTOUT
  fprintf(fp,"runTime = %12d, PUend = %12d, pu = %04d, clean = %6d, good = %6d\n",
         (int) runTime,
         (int) tdc_pileup.endMasterEvent0,
         tdc_eval_event.pileup[ch][tdc_eval_event.foundChannel[ch]-1],
         (int) vme_stats.mcounter_clean, (int) vme_stats.mcounter_good);
#endif
 return 0;
}
/*-----------------------------------------------------------------------------------------*/
/*!
 * pileup_end(INT pileupWindow):
 *
 * calculate the end of pileup gate in absolute run time (in TDC LSB units)
 *
*/
U_LONG pileup_end(INT pileupWindow)
{
 U_LONG end_time;

 end_time = runTime + (U_LONG) pileupWindow;
 return end_time;
}
/*-----------------------------------------------------------------------------------------*/
/*!
 * evaluate_muon_event():
 *
 * check events, if trigger condition is met, build events, move them to ring buffer
 * which is read in the read_trigger_event() function
 *
*/
INT evaluate_muon_event()
{
 INT   i, j, k, diff_time, decChannel, decInd, masterIndex;
 BOOL  good_event, pos_event;
 DWORD dataWindow;
 LEM_DATA *lem_data;

#ifdef TESTOUT
 fprintf(fp,"evaluate_fast_muon_event, mcounter = %10.0f, mcounter_clean = %10.0f\n",
            vme_stats.channelCounts[MCOUNTER], vme_stats.mcounter_clean);
#endif

 good_event  = pos_event = FALSE;
 dataWindow  = trigger_settings.data_window;
 masterIndex = tdc_eval_event.indexMuon;
 decChannel  = decInd = -1; //initialize

 //check positron coincidence with MCOUNTER: negative coincidence window means single positron counters
 pos_event = check_positron(dataWindow, -50, POS01, POS29, MCOUNTER, masterIndex, &decChannel, &decInd);

 switch (eventDefinedMuon){
  case (MCOUNTER_EVENT):
   good_event = TRUE;
   break;

  case (MCOUNTER_EVENT | POSITRON_EVENT ):
   good_event = pos_event;
   break;

  default:
   break;
 }

 // ----------------------
 // if no event found, reset arrays and return
 if ( !good_event ){
  memset(&tdc_eval_event, 0x00, sizeof(tdc_eval_event));
  return SUCCESS;
 }
 // -----------------------
 // we have a valid event
 // build event in the form
 // ch2:1st Hit (1st MCOUNTER clean)
 // ch2:2nd Hit
 // ...
 // ch6:1st Hit
 // ...
 k = 0;
 for ( i=2; i<N_TDC_CHANNELS; i++ ){// i=0 is Ip, i=1 10kHz clock
  switch (i){
    case POS01: case POS02: case POS03: case POS04: case POS05: case POS06: case POS07: case POS08:
    case POS09: case POS10:
    case POS11: case POS12: case POS13: case POS14: case POS15: case POS16: case POS17: case POS18:
    case POS19: case POS20:
    case POS21: case POS22: case POS23: case POS24: case POS25: case POS26: case POS27: case POS28:
    case POS29:
     if ( decChannel == -1 ) continue;
     break;

    default:
     break;
  }

  for ( j=0; j<tdc_eval_event.foundChannel[i]; j++){
     switch (i){
       case MCOUNTER:
        if ( j < masterIndex) // this is not a "clean" signal
          continue;
        break;

       case POS01: case POS02: case POS03: case POS04: case POS05: case POS06: case POS07: case POS08:
       case POS09: case POS10:
       case POS11: case POS12: case POS13: case POS14: case POS15: case POS16: case POS17: case POS18:
       case POS19: case POS20:
       case POS21: case POS22: case POS23: case POS24: case POS25: case POS26: case POS27: case POS28:
       case POS29: 
       // don't put e+ data to data buffer if they appeared earlier than masterchannel
        if ( i == decChannel && j < decInd ) // the first e+ event in the data of i=decChannel
             continue;                             // will be the first coincidence found
        if ( (tdc_eval_event.tdc_event[MCOUNTER][masterIndex] - tdc_eval_event.tdc_event[i][j]) > 0)
             continue;
        break;

       default:
        break;
      }
      k = ++tdc_eval_event.eventSize;
      if ( k > DATA_N_HITS){ //max. DATA_N_HITS hits to output data stream
      tdc_eval_event.eventSize -= 1;
      break;
      }
      lem_data = (LEM_DATA *) &tdc_eval_event.tdc_data[k];
      lem_data->data    = tdc_eval_event.tdc_event[i][j];
      lem_data->channel = i;
//      tdc_eval_event.tdc_data[k] =
//           ((tdc_eval_event.tdc_data[k] | i)<<24 ) | tdc_eval_event.tdc_event[i][j]; //copy channel
  }
 }
 tdc_eval_event.tdc_data[0] =
        (tdc_eval_event.tdc_data[0] | NEW_EVENT_MASK) |  // bits 24-30 set indicating new event
        ((tdc_eval_event.tdc_data[0] | EVENT_2_TYPE)<<16) |
        tdc_eval_event.eventSize;

 for ( j = 0; j < tdc_eval_event.eventSize+1; j++){
   if (p_write < &databuffer[DATABUFFER_SIZE]){
        *p_write++ = tdc_eval_event.tdc_data[j];
   }
   else{ //jump to beginning of ring buffer
        p_write = &databuffer[0];
        *p_write++ = tdc_eval_event.tdc_data[j];
   }
#ifdef TESTOUT
   lem_data = (LEM_DATA *) &tdc_eval_event.tdc_data[j];
   fprintf(fp,"tdc data = %02d, %8d, channel %4d, hex = 0x%08x, ch = %4d, index = %4d\n",
      j,
      lem_data->data,
      lem_data->channel,
      tdc_eval_event.tdc_data[j],
      bcChannel, bcInd);
#endif
  }

  vme_stats.MuonEvents++;
  memset(&tdc_eval_event, 0x00, sizeof(tdc_eval_event));

  return SUCCESS;
}
/* -------------------------------------------------------------------- */
/*!
 * <pre>
 * check_tof_and_pileup():
 *
 * check events: is there a signal in the tofWIndow relative to masteChannel?
 * if yes: check if hit is "clean", i.e. there was no other signal within pileupWindow
 *
 * N O T E: this function only checks the pre-pileup for "TOF" and "Decay" hits, i.e.
 *          BC and e+ hits. The pre-pileup for the master channels is implicitly done
 *          in the loop over the TDC output buffer (currently done in frontend_loop())
 *          where it is required that two starts of master channel events are separated
 *          by at least diffTimeMuon or diffTimeFastMuon (which is 10musec at the moment).
 *
 * </pre>
 *
*/
BOOL check_tof_and_pileup(INT timing, INT tofWindow, INT firstCh, INT lastCh, INT masterChannel, INT masterIndex,
                          INT *channel, INT *ind)
{
 INT i, j, k, l;
 INT difftime_now, difftime_last;

 if ( timing == MASTERLAST )
    difftime_now = difftime_last = 0;
 else
    difftime_now = difftime_last = 2 * tofWindow;
 *channel = *ind = -1;
 for ( i = firstCh; i <= lastCh; i++){
     //the following is for "MasterLast" timing; may lead to loss of some events
    if ( timing == MASTERLAST){
     for ( j = 0; j < tdc_eval_event.foundChannel[i]; j++){ // get the most distant hit within the TOF window
         difftime_now = tdc_eval_event.tdc_event[masterChannel][masterIndex] - tdc_eval_event.tdc_event[i][j];

         if ( difftime_now < tofWindow && difftime_now > 0){
           if ( difftime_now > difftime_last ){
              difftime_last = difftime_now;
              *channel = i;
              *ind     = j;
           }
        }
     }
    }
    else{
    // for "MasterFirst" timing: each "good" hit is assumed to appear after the master detector hit
     for ( j = 0; j < tdc_eval_event.foundChannel[i]; j++){ // get the nearest hit within the TOF window
         difftime_now = tdc_eval_event.tdc_event[i][j] - tdc_eval_event.tdc_event[masterChannel][masterIndex];

         if ( difftime_now < tofWindow && difftime_now > 0){
           if ( difftime_now < difftime_last ){
              difftime_last = difftime_now;
              *channel = i;
              *ind     = j;
           }
        }
     }
   }
 }
 if ( *channel == -1 ) return FALSE; // we didn't find an tof/decay event

 // ----------------------
 // check pileup;
 //
 if ( tdc_eval_event.pileup[*channel][*ind] == 1 ){
#ifdef TESTOUT
  fprintf(fp, " TOF pileup at channel %d, hit %d\n", *channel, *ind);
#endif
  return FALSE;
 }
 return TRUE; // if we come to this point we found an event and no pre-pileup
}
/* -------------------------------------------------------------------- */
/*!
 * <pre>
 * check_positron(decWindow, coincWindow, firstCh, lastCh, masterchannel, channel, ind):
 *
 * check events: is there a e+ (coincidence) signal in the decWindow relative to masterChannel?
 *               find the nearest e+ event with respect to the masterChannel[0] event.
 *               if coincWindow < 0: single e+ counters are considered
 *               if coincWindow >=0: look for coincidences between the two channels firstCh and
                                     lastCh
 * N O T E: post- pileup checking for positrons; this data will be written to the Midas event
 *
 * </pre>
 *
*/
BOOL check_positron(INT decWindow, INT coincWindow, INT firstCh, INT lastCh, INT masterChannel, INT masterIndex,
                    INT *channel, INT *ind)
{
 INT i, j;
 INT difftime_now, difftime_last, difftime;
 INT n_coincidence;

 difftime_now = difftime_last = 2*decWindow; // initialise variables
 *channel = *ind = -1;

 if ( coincWindow < 0 ){
    // the following is for single e+ counters
    for ( i = firstCh; i <= lastCh; i++){
      for ( j = 0; j < tdc_eval_event.foundChannel[i]; j++){ // get the nearest hit within the decay window
         difftime_now = tdc_eval_event.tdc_event[i][j] - tdc_eval_event.tdc_event[masterChannel][masterIndex];

         if ( difftime_now < decWindow && difftime_now > 0){
           if ( difftime_now < difftime_last ){
              difftime_last = difftime_now;
              *channel = i;
              *ind     = j;
           }
        }
      }
    }
 }
 else{// search for coincidences
    if ( (tdc_eval_event.foundChannel[firstCh] == 0) || (tdc_eval_event.foundChannel[lastCh] == 0) ) return FALSE;

    // check now for coincidences;
    // here: assume that corresponding detectors have the ID's
    // firstCh,   lastCh
    // search the next nearest e+ coincidence with respect to the masterChannel
    // firstCh makes the timing
    for ( i = 0; i < tdc_eval_event.foundChannel[firstCh]; i++ ){
      for ( j = 0; j < tdc_eval_event.foundChannel[lastCh]; j++ ){
         difftime = abs(tdc_eval_event.tdc_event[firstCh][i] - tdc_eval_event.tdc_event[lastCh][j]);

        if ( difftime < coincWindow ){
           difftime_now = tdc_eval_event.tdc_event[firstCh][i] - tdc_eval_event.tdc_event[masterChannel][masterIndex];
           if ( difftime_now < decWindow && difftime_now > 0){
             if ( difftime_now < difftime_last ){
              difftime_last = difftime_now;
              *channel = firstCh;
              *ind     = i;
             }
           }
        }
      }
    }
 }

 if ( *channel == -1 ) return FALSE; // we didn't find a decay event

 return TRUE; // if we come to this point we found a positron event
}
/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   //frontend_loop();
   if ( vme_stats.poll_counts == 0 ) ss_sleep(100); //if count==1 first call of poll_event from mfe.c
   vme_stats.poll_counts++;
   if ( (p_read != p_write)  && !test ){
     //fprintf(fp, "p_read = 0x%x, p_write = 0x%x\n", p_read, p_write);
     return 1;
   }
   return 0;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  switch(cmd)
    {
    case CMD_INTERRUPT_ENABLE:
      break;
    case CMD_INTERRUPT_DISABLE:
      break;
    case CMD_INTERRUPT_ATTACH:
      break;
    case CMD_INTERRUPT_DETACH:
      break;
    }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/
/*!
 * <p><b>read_trigger_event()</b> is the event read out function
 *    to get the V1190 TDC data.
 *
*/
INT read_trigger_event(char *pevent, INT off)
{
INT   i;
DWORD *pdata;
DWORD ndata;
/*
  //  SUPER event does not work properly, has to be checked...
  //init bank structure, Super event
  if ( off == 0 ){
    // FIRST event of Super event
    bk_init(pevent);
    bk_create(pevent, "TDC0", TID_DWORD, &pdata);
  }
  else if ( off == -1 ){
    bk_close(pevent, pdata);  //close the super event if off=-1, from mfe.c
    return bk_size(pevent);
  }
*/
  //init bank structure
  bk_init(pevent);
  bk_create(pevent, "TDC0", TID_DWORD, (void**)&pdata);

  // read data from output buffer;
  if (p_read == &databuffer[DATABUFFER_SIZE]) //we are 4 bytes off the end of the data buffer, jump
    p_read = &databuffer[0];                  //to the beginning of the data buffer

  if ( ( *p_read & NEW_EVENT_MASK) != NEW_EVENT_MASK ) return 0; //causes event rate to be 0
                                                                //this happens if the read pointer is
                                                                //out of sync to the write pointer
  ndata = (*p_read & 0x0000FFFF) + 1; //the first entry in the data buffer contains the event size - 1
  //fprintf(fp,"eventSize = %d, p_read=0x%08x, p_write=0x%08x\n",ndata, p_read, p_write);
  for (i=0; i < ndata; i++){
   if (p_read < &databuffer[DATABUFFER_SIZE]){
      *pdata++ = *p_read;
      *p_read = 0;
      p_read++;
   }
   else{ //jump to beginning of ring buffer
       p_read = &databuffer[0];
       *pdata++ = *p_read;
       *p_read = 0;
       p_read++;
   }
  }

  vme_stats.readcounts++;
/*
  if (off == 0){
  // Compute the proper event length on the FIRST event in the Super Event
  // N_TDC correspond to the !! N_TDC DWORD above !!
  // sizeof(BANK_HEADER) + sizeof(BANK) will make the 16 bytes header
  // sizeof(DWORD) is defined by the TID_DWORD in bk_create()
     return N_TDC * sizeof(DWORD) + sizeof(BANK_HEADER) + sizeof(BANK);
  }
  else{
  // Return the data section size only
  // sizeof(DWORD) is defined by the TID_DWORD in bk_create()

      return N_TDC * sizeof(DWORD);
  }
*/
  bk_close(pevent, pdata);
  return bk_size(pevent);
}

/*! -----------------------------------------------------------------
 * <p> Read out of scaler modules.
 * </p>
 *
 * <p> If HAVE_TEST_RUN is defined scaler entries
 *     of about 100 are thrown. If not defined
 *     use the function <b>sis3820_read_scaler</b>
 *     to read out the VME scaler module.
 *
 * <p><b>Return:</b>
 *   - bank size of scaler event
 *
 *
*/
INT read_scaler_event(char *pevent, INT off)
{
int status;
u_int32_t data[100], channel;
DWORD *pdata, i;

  /* init bank structure */
  bk_init(pevent);

  /* create SCL0 bank */
  bk_create(pevent, "SCL0", TID_DWORD, (void**)&pdata);

  /* read out scaler module */
#ifdef HAVE_TEST_RUN
  for (i = 0; i<N_SCALER ; i++){
  /*---------------------------------------------------------
    simulate the case when user bits and channel number are sent
    in the first 8 bits, scaler data in the remaining 24 bits
    ---------------------------------------------------------
  */
    if ( scaler_settings.input_mode_3 )
        channel = i << 24;
    else
        channel = 0;
    *pdata++ = (100 + rand()%40-20) | channel;
  }
#else
   //read 1st scaler
   if ( flag_scaler_address_0){
    status = sis3820_scaler_read( hdev, SIS3820_ADDRESS_0, A32BLT32, N_SCALER_MODULE, data);
    if ( status != 0 ){
        for ( i = 0; i<N_SCALER_MODULE; i++)
                *pdata++ = -1;
        cm_msg(MERROR, "read_scaler_event", "Error reading data from VME Scaler SIS3820!");
        cm_yield(0);
    }
    else
        for ( i = 0; i<N_SCALER_MODULE; i++)
                *pdata++ = data[i];
   }
   else //scaler disabled or not present
        for ( i = 0; i<N_SCALER_MODULE; i++)
                *pdata++ = 0;

   //read 2nd scaler
/*   if ( flag_scaler_address_1){
    status = sis3820_scaler_read( hdev, SIS3820_ADDRESS_1, A32BLT32, N_SCALER_MODULE, data);
    if ( status != 0 ){
        for ( i = 0; i<N_SCALER_MODULE; i++)
                *pdata++ = -1;
        cm_msg(MERROR, "read_scaler_event", "Error reading data from VME Scaler SIS3820!");
        cm_yield(0);
    }
    else
        for ( i = 0; i<N_SCALER_MODULE; i++)
                *pdata++ = data[i];
   }
   else //scaler disabled or not present
        for ( i = 0; i<N_SCALER_MODULE; i++)
                *pdata++ = 0;*/
     
#endif

  bk_close(pevent, pdata);
  return bk_size(pevent);
}

/* --------------  other functions  -----------
void scaler_mode(u_int32_t module_address )
int  init_sis3820(u_int32_t module_address)
int  init_v1190(u_int32_t module_address)
*/

/*! <p> scaler_mode is called at begin_of_run.
 *      It reads the ODB record <b><i>/Equipment/Scaler/Settings</i></b>
 *      and checks if the variable
 *      <b><i>scaler_settings.input_mode_3</i></b> is set.
 *      If yes the SIS3820 will be set to<br><b><i>
 *      SIS3820_CONTROL_INPUT_MODE3 + SIS3820_SCALER_DATA_FORMAT_24BIT</i></b><br>
 *      otherwise it will be set to <br><b><i>
 *      SIS3820_CONTROL_INPUT_MODE0 + SIS3820_SCALER_DATA_FORMAT_32BIT</i></b><br>
 *
 *   <p> No interrupt handling at the moment.
 *   <p>
*/
void scaler_mode(u_int32_t module_address)
{
  INT size, status;
  u_int32_t control;
  HNDLE hDB, hkey;

  cm_get_experiment_database(&hDB, NULL);

  status  = 0;
  control = 0;

  size = sizeof(scaler_settings);
  db_find_key(hDB, 0, "/Equipment/Scaler/Settings", &hkey);
  db_get_record(hDB, hkey, &scaler_settings, &size, 0);

  if (scaler_settings.input_mode_3)
        control = SIS3820_CONTROL_INPUT_MODE3 + SIS3820_SCALER_DATA_FORMAT_24BIT;
  else
        control = SIS3820_CONTROL_INPUT_MODE0 + SIS3820_SCALER_DATA_FORMAT_32BIT;

  status  = sis3820_operation_mode_write( hdev, module_address, control);
  if ( status !=0) {
    cm_msg(MERROR, "scaler_mode", "Error on setting mode of SIS3820, return code %x\n", status);
    cm_yield(0);
  }

  if (scaler_settings.reference_ch1)
        status = sis3820_control_write( hdev, module_address, CTRL_REFERENCE_CH1_ENABLE);
  else
        status = sis3820_control_write( hdev, module_address, CTRL_REFERENCE_CH1_DISABLE);

  if ( status !=0) {
    cm_msg(MERROR, "scaler_mode", "Error on setting control status of SIS3820, return code %x\n", status);
    cm_yield(0);
  }

  /* read control status and operation mode */
   status = sis3820_control_read( hdev, module_address, &control);
   if ( status !=0)
        cm_msg(MERROR, "scaler_mode", "Error on reading control status of SIS3820, return code %x\n", status);
   else
        cm_msg(MINFO,"scaler_mode","SIS3820 control status reads: 0x%8.8x \n",control);
   cm_yield(0);

   status =sis3820_operation_mode_read( hdev, module_address, &control);
   if ( status != 0)
        cm_msg(MERROR, "scaler_mode", "Error on reading operation mode of SIS3820, return code %x\n", status);
   else
        cm_msg(MINFO,"scaler_mode","SIS3820 operation mode reads: 0x%8.8x \n",control);
   cm_yield(0);

}

/*! <p> <b>init_sis3820</b> is called during frontend_init for
 *      hardware initialization.
 *      <ul>
 *      <li> open the VME_DEVICE (SIS3100/1100 VME-PCI interface)</li>
 *      <li> resets the module (sis3820_key_reset)</li>
 *      <li> reads module and firmware ID (sis3820_modid_read)</li>
 *      <li> reads control status register (sis3820_control_read)</li>
 *      <li> reads operation mode register (sis3820_operation_mode_read)</li>
 *      <li> enables SIS3820 for counting (sis3820_key_enable)
 *      </ul>
 *
 *   <p><b>Return:</b>
 *   - FE_SUCCESS everything ok
 *   - FE_ERR_HW  error accessing the SIS3820 modeul
 *   <p>
*/
int init_sis3820(u_int32_t module_address)
{
 int status;
 u_int32_t modid, control, mode;

 /* reset SIS3820 */
  status = sis3820_key_reset( hdev, module_address);
  if ( status !=0){
        cm_msg(MERROR, "frontend_init",
               "Error on access to SIS3820 0x%8.8x, key reset failed, return code %x\n", module_address, status);
        cm_yield(0);
        return FE_ERR_HW;
  }

  status = 0;
  status += sis3820_modid_read( hdev, module_address, &modid);
  status += sis3820_control_read( hdev, module_address, &control);
  status += sis3820_operation_mode_read( hdev, module_address, &mode);
  if ( status !=0){
        cm_msg(MERROR, "frontend_init",
               "Error on access to SIS3820, couldn't get module info, return code %x\n", status);
        cm_yield(0);
        return FE_ERR_HW;
  }
  cm_msg(MINFO,"frontend_init", "SIS3820 modid and firmware = 0x%8.8x", modid);
  cm_msg(MINFO,"frontend_init", "SIS3820 control status     = 0x%8.8x", control);
  cm_msg(MINFO,"frontend_init", "SIS3820 operation mode     = 0x%8.8x", mode);
  cm_yield(0);

  /* enable SIS3820 for counting, default mode (scaler, 32 bit) */
  status = sis3820_key_enable( hdev, module_address);
  if ( status !=0){
        cm_msg(MERROR, "frontend_init",
               "Error on access to SIS3820, key enable failed, return code %x\n", status);
        cm_yield(0);
        return FE_ERR_HW;
  }
  cm_msg(MINFO, "frontent_init","SIS3820 enabled for counting");
  cm_yield(0);

  return FE_SUCCESS;
}
/*----------------------------------------------------------------------*/
/*!
 * - reset V1190 to its default settings: write 0 to V1190_MODULE_RESET
 * - set leading edge (default): write V1190_EDGE_LEADING_MODE to V1190_OPCODE_ADDRESS/V1190_SET_DETECTION
 * - set 200ps: write V1190_LSB_200PS to V1190_OPCODE_ADDRESS/V1190_SET_TR_LEAD_LSB
 * - set continuous mode: write V1190_CONT_STOR to V1190_OPCODE_ADDRESS
 * - set Almost Full Level to ALMOST_FULL_LEVEL
 * - enable VME Bus error to indicate that Output Buffer is empty: write 0x21 to V1190_CONTROL_REGISTER
*/
int init_v1190(u_int32_t module_address)
{
u_int16_t data;
int return_code;

#ifdef HAVE_TEST_RUN
 return FE_SUCCESS;
#endif
 /* reset module to its default configuration */
 data = 0x0;
 return_code =  vme_A32D16_write(hdev, module_address+V1190_MODULE_RESET, data) ;
 cm_msg(MINFO, "init_v1190","V1190 Module Reset:   return_code = 0x%08x", return_code );
 cm_yield(0);
 if ( return_code != 0x0 ) return FE_ERR_HW;
 sleep(1);

 /* set leading edge mode */
 return_code = vme_A32D16_write(hdev, module_address+V1190_OPCODE_ADDRESS, V1190_SET_DETECTION);
 cm_msg(MINFO, "init_v1190","V1190 Set_Detection:   return_code = 0x%08x", return_code );
 cm_yield(0);
 if ( return_code != 0x0 ) return FE_ERR_HW;
 return_code = vme_A32D16_write(hdev, module_address+V1190_OPCODE_ADDRESS, V1190_EDGE_LEADING_MODE);
 cm_msg(MINFO, "init_v1190", "V1190 Set_to_Leading_Edge:   return_code = 0x%08x", return_code );
 cm_yield(0);
 if ( return_code != 0x0 ) return FE_ERR_HW;
 sleep(1);

  /* set 200ps LSB */
 return_code = vme_A32D16_write(hdev, module_address+V1190_OPCODE_ADDRESS, V1190_SET_TR_LEAD_LSB);
 cm_msg(MINFO, "init_v1190", "V1190 Set_LSB:   return_code = 0x%08x", return_code );
 cm_yield(0);
 if ( return_code != 0x0 ) return FE_ERR_HW;
 return_code = vme_A32D16_write(hdev, module_address+V1190_OPCODE_ADDRESS, V1190_LSB_200PS);
 cm_msg(MINFO, "init_v1190", "V1190 Set_LSB_200ps:   return_code = 0x%08x", return_code );
 cm_yield(0);
 if ( return_code != 0x0 ) return FE_ERR_HW;
 sleep(1);

 /* disable all channels if set in ODB */
 if ( trigger_settings.tdc_disable_atstartup ){
   return_code = vme_A32D16_write(hdev, module_address+V1190_OPCODE_ADDRESS, V1190_DISABLE_ALL_CHANNEL);
   cm_msg(MINFO, "init_v1190", "V1190 Disable all channels:   return_code = 0x%08x", return_code);
   cm_yield(0);
   sleep(1);
 }

 /* set continuous storage mode */
 return_code = vme_A32D16_write(hdev, module_address+V1190_OPCODE_ADDRESS, V1190_CONT_STOR);
 cm_msg(MINFO, "init_v1190","V1190 Set_Continuous storage:   return_code = 0x%08x", return_code );
 cm_yield(0);

 if ( return_code != 0x0 ) return FE_ERR_HW;
 sleep(1);

 /* set almost full level */
 return_code =  vme_A32D16_write(hdev, module_address+V1190_ALMOST_FULL_LEVEL, ALMOST_FULL_LEVEL);
 cm_msg(MINFO, "init_v1190","V1190 Set_Almost_Full_Level:   return_code = 0x%08x", return_code );
 cm_yield(0);

 /* enable VME Bus error to indicate that Output Buffer is empty */
 data = 0x21;
 return_code =  vme_A32D16_write(hdev, module_address+V1190_CONTROL_REGISTER, data);
 cm_msg(MINFO, "init_v1190","V1190 Control_Register:   return_code = 0x%08x", return_code );
 cm_yield(0);

 /* read module status */
 return_code =  vme_A32D16_read(hdev, module_address+V1190_ALMOST_FULL_LEVEL, &data ) ;
 cm_msg(MINFO, "init_v1190", "V1190 Almost Full Level:   return_code = 0x%08x", return_code);
 cm_msg(MINFO, "init_v1190", "V1190 Almost Full Level:   data = 0x%04x", data );
 cm_yield(0);

 return_code =  vme_A32D16_read(hdev, module_address+V1190_STATUS_REGISTER, &data ) ;
 cm_msg(MINFO, "init_v1190", "V1190 Modul status:   return_code = 0x%08x", return_code);
 cm_msg(MINFO, "init_v1190", "V1190 Modul status:   data = 0x%04x", data );
 cm_yield(0);

 return_code =  vme_A32D16_read(hdev, module_address+V1190_CONTROL_REGISTER, &data ) ;
 cm_msg(MINFO, "init_v1190", "V1190 Control Register:   return_code = 0x%08x", return_code);
 cm_msg(MINFO, "init_v1190", "V1190 Control Register:   data = 0x%04x", data );
 cm_yield(0);

 if ( return_code != 0x0 ) return FE_ERR_HW;

 return FE_SUCCESS;
}

/* -----------------------------------------------------------------------
        E O F           vme_fe.c
  ------------------------------------------------------------------------
*/
