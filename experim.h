/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Fri Nov 14 19:01:16 2014

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
  char      comment[136];
  char      scaler_update_time[32];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) const char *_name[] = {\
"[.]",\
"comment = STRING : [136] T e s t",\
"Scaler Update time = STRING : [32] ",\
"",\
NULL }

#define EXP_EDIT_DEFINED

typedef struct {
  char      comment[136];
} EXP_EDIT;

#define EXP_EDIT_STR(_name) const char *_name[] = {\
"[.]",\
"comment = LINK : [35] /Experiment/Run Parameters/comment",\
"",\
NULL }

#ifndef EXCL_DECAYANAMODULE

#define DECAYANAMODULE_PARAM_DEFINED

typedef struct {
  struct {
    char      titles[58][32];
  } histotitles;
  struct {
    INT       histonbin[58];
    float     histoxlow[58];
    float     histoxup[58];
  } histobinning;
} DECAYANAMODULE_PARAM;

#define DECAYANAMODULE_PARAM_STR(_name) const char *_name[] = {\
"[HistoTitles]",\
"Titles = STRING[58] :",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ npp",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"[32] e+ ppc",\
"",\
"[HistoBinning]",\
"HistoNbin = INT[58] :",\
"[0] 66601",\
"[1] 66601",\
"[2] 66601",\
"[3] 66601",\
"[4] 66601",\
"[5] 66601",\
"[6] 66601",\
"[7] 66601",\
"[8] 66601",\
"[9] 66601",\
"[10] 66601",\
"[11] 66601",\
"[12] 66601",\
"[13] 66601",\
"[14] 66601",\
"[15] 66601",\
"[16] 66601",\
"[17] 66601",\
"[18] 66601",\
"[19] 66601",\
"[20] 66601",\
"[21] 66601",\
"[22] 66601",\
"[23] 66601",\
"[24] 66601",\
"[25] 66601",\
"[26] 66601",\
"[27] 66601",\
"[28] 66601",\
"[29] 66601",\
"[30] 66601",\
"[31] 66601",\
"[32] 66601",\
"[33] 66601",\
"[34] 66601",\
"[35] 66601",\
"[36] 66601",\
"[37] 66601",\
"[38] 66601",\
"[39] 66601",\
"[40] 66601",\
"[41] 66601",\
"[42] 66601",\
"[43] 66601",\
"[44] 66601",\
"[45] 66601",\
"[46] 66601",\
"[47] 66601",\
"[48] 66601",\
"[49] 66601",\
"[50] 66601",\
"[51] 66601",\
"[52] 66601",\
"[53] 66601",\
"[54] 66601",\
"[55] 66601",\
"[56] 66601",\
"[57] 66601",\
"HistoxLow = FLOAT[58] :",\
"[0] -0.5",\
"[1] -0.5",\
"[2] -0.5",\
"[3] -0.5",\
"[4] -0.5",\
"[5] -0.5",\
"[6] -0.5",\
"[7] -0.5",\
"[8] -0.5",\
"[9] -0.5",\
"[10] -0.5",\
"[11] -0.5",\
"[12] -0.5",\
"[13] -0.5",\
"[14] -0.5",\
"[15] -0.5",\
"[16] -0.5",\
"[17] -0.5",\
"[18] -0.5",\
"[19] -0.5",\
"[20] -0.5",\
"[21] -0.5",\
"[22] -0.5",\
"[23] -0.5",\
"[24] -0.5",\
"[25] -0.5",\
"[26] -0.5",\
"[27] -0.5",\
"[28] -0.5",\
"[29] -0.5",\
"[30] -0.5",\
"[31] -0.5",\
"[32] -0.5",\
"[33] -0.5",\
"[34] -0.5",\
"[35] -0.5",\
"[36] -0.5",\
"[37] -0.5",\
"[38] -0.5",\
"[39] -0.5",\
"[40] -0.5",\
"[41] -0.5",\
"[42] -0.5",\
"[43] -0.5",\
"[44] -0.5",\
"[45] -0.5",\
"[46] -0.5",\
"[47] -0.5",\
"[48] -0.5",\
"[49] -0.5",\
"[50] -0.5",\
"[51] -0.5",\
"[52] -0.5",\
"[53] -0.5",\
"[54] -0.5",\
"[55] -0.5",\
"[56] -0.5",\
"[57] -0.5",\
"HistoxUp = FLOAT[58] :",\
"[0] 66600.5",\
"[1] 66600.5",\
"[2] 66600.5",\
"[3] 66600.5",\
"[4] 66600.5",\
"[5] 66600.5",\
"[6] 66600.5",\
"[7] 66600.5",\
"[8] 66600.5",\
"[9] 66600.5",\
"[10] 66600.5",\
"[11] 66600.5",\
"[12] 66600.5",\
"[13] 66600.5",\
"[14] 66600.5",\
"[15] 66600.5",\
"[16] 66600.5",\
"[17] 66600.5",\
"[18] 66600.5",\
"[19] 66600.5",\
"[20] 66600.5",\
"[21] 66600.5",\
"[22] 66600.5",\
"[23] 66600.5",\
"[24] 66600.5",\
"[25] 66600.5",\
"[26] 66600.5",\
"[27] 66600.5",\
"[28] 66600.5",\
"[29] 66600.5",\
"[30] 66600.5",\
"[31] 66600.5",\
"[32] 66600.5",\
"[33] 66600.5",\
"[34] 66600.5",\
"[35] 66600.5",\
"[36] 66600.5",\
"[37] 66600.5",\
"[38] 66600.5",\
"[39] 66600.5",\
"[40] 66600.5",\
"[41] 66600.5",\
"[42] 66600.5",\
"[43] 66600.5",\
"[44] 66600.5",\
"[45] 66600.5",\
"[46] 66600.5",\
"[47] 66600.5",\
"[48] 66600.5",\
"[49] 66600.5",\
"[50] 66600.5",\
"[51] 66600.5",\
"[52] 66600.5",\
"[53] 66600.5",\
"[54] 66600.5",\
"[55] 66600.5",\
"[56] 66600.5",\
"[57] 66600.5",\
"",\
NULL }

#endif

#ifndef EXCL_SCALERSUMRATE

#define SCALERSUMRATE_PARAM_DEFINED

typedef struct {
  INT       channel_norm_time;
  INT       rate_norm_time;
  INT       ip_channel;
  INT       n_rate_average;
} SCALERSUMRATE_PARAM;

#define SCALERSUMRATE_PARAM_STR(_name) const char *_name[] = {\
"[.]",\
"Channel_norm_time = INT : 1",\
"Rate_norm_time = INT : 10000",\
"Ip_Channel = INT : 0",\
"n_Rate_Average = INT : 300",\
"",\
NULL }

#endif

#ifndef EXCL_T0SHIFT

#define T0SHIFT_PARAM_DEFINED

typedef struct {
  float     t0[58];
  float     deltat_t0[58];
} T0SHIFT_PARAM;

#define T0SHIFT_PARAM_STR(_name) const char *_name[] = {\
"[.]",\
"t0 = FLOAT[58] :",\
"[0] 2000",\
"[1] 2000",\
"[2] 2000",\
"[3] 2000",\
"[4] 2000",\
"[5] 2000",\
"[6] 2000",\
"[7] 2000",\
"[8] 2000",\
"[9] 2000",\
"[10] 2000",\
"[11] 2000",\
"[12] 2000",\
"[13] 2000",\
"[14] 2000",\
"[15] 2000",\
"[16] 2000",\
"[17] 2000",\
"[18] 2000",\
"[19] 2000",\
"[20] 2000",\
"[21] 2000",\
"[22] 2000",\
"[23] 2000",\
"[24] 2000",\
"[25] 2000",\
"[26] 2000",\
"[27] 2000",\
"[28] 2000",\
"[29] 2000",\
"[30] 2000",\
"[31] 2000",\
"[32] 2000",\
"[33] 2000",\
"[34] 2000",\
"[35] 2000",\
"[36] 2000",\
"[37] 2000",\
"[38] 2000",\
"[39] 2000",\
"[40] 2000",\
"[41] 2000",\
"[42] 2000",\
"[43] 2000",\
"[44] 2000",\
"[45] 2000",\
"[46] 2000",\
"[47] 2000",\
"[48] 2000",\
"[49] 2000",\
"[50] 2000",\
"[51] 2000",\
"[52] 2000",\
"[53] 2000",\
"[54] 2000",\
"[55] 2000",\
"[56] 2000",\
"[57] 2000",\
"Deltat t0 = FLOAT[58] :",\
"[0] 10",\
"[1] 10",\
"[2] 10",\
"[3] 10",\
"[4] 10",\
"[5] 10",\
"[6] 10",\
"[7] 10",\
"[8] 10",\
"[9] 10",\
"[10] 10",\
"[11] 10",\
"[12] 10",\
"[13] 10",\
"[14] 10",\
"[15] 10",\
"[16] 10",\
"[17] 10",\
"[18] 10",\
"[19] 10",\
"[20] 10",\
"[21] 10",\
"[22] 10",\
"[23] 10",\
"[24] 10",\
"[25] 10",\
"[26] 10",\
"[27] 10",\
"[28] 10",\
"[29] 10",\
"[30] 10",\
"[31] 10",\
"[32] 10",\
"[33] 10",\
"[34] 10",\
"[35] 10",\
"[36] 10",\
"[37] 10",\
"[38] 10",\
"[39] 10",\
"[40] 10",\
"[41] 10",\
"[42] 10",\
"[43] 10",\
"[44] 10",\
"[45] 10",\
"[46] 10",\
"[47] 10",\
"[48] 10",\
"[49] 10",\
"[50] 10",\
"[51] 10",\
"[52] 10",\
"[53] 10",\
"[54] 10",\
"[55] 10",\
"[56] 10",\
"[57] 10",\
"",\
NULL }

#endif

#ifndef EXCL_TRIGGER

#define TRIGGER_SETTINGS_DEFINED

typedef struct {
  INT       t0_offset;
  BOOL      tdc_disable_atstartup;
  DWORD     data_window;
  INT       mcounter_delay;
  INT       positron_delay;
  BOOL      positrons_active;
  BOOL      simulation_flag;
  double    simulation_startrate;
  double    simulation_stoprate;
} TRIGGER_SETTINGS;

#define TRIGGER_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"T0_Offset = INT : 550",\
"TDC_Disable_AtStartup = BOOL : y",\
"Data_Window = DWORD : 66560",\
"MCounter_Delay = INT : 0",\
"Positron_Delay = INT : 2000",\
"Positrons_Active = BOOL : y",\
"Simulation_Flag = BOOL : n",\
"Simulation_StartRate = DOUBLE : 1e-05",\
"Simulation_StopRate = DOUBLE : 2.5e-05",\
"",\
NULL }

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 2",\
"Source = INT : 16777215",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lem01",\
"Frontend name = STRING : [32] VME_FE",\
"Frontend file name = STRING : [256] vme_fe.c",\
"Status = STRING : [256] VME_FE@lem01",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#define TRIGGER_VME_STATISTICS_DEFINED

typedef struct {
  double    runtime;
  INT       lasttdctime;
  double    muonevents;
  double    mcounter_clean;
  double    mcounter_good;
  double    channelcounts[64];
  double    frontend_loop_counts;
  double    frontend_loop_tdc;
  double    poll_counts;
  double    readcounts;
  double    tdcerrorcounts;
} TRIGGER_VME_STATISTICS;

#define TRIGGER_VME_STATISTICS_STR(_name) const char *_name[] = {\
"[.]",\
"RunTime = DOUBLE : 0",\
"LastTDCTime = INT : 0",\
"MuonEvents = DOUBLE : 0",\
"Mcounter Clean = DOUBLE : 0",\
"Mcounter Good = DOUBLE : 0",\
"channelCounts = DOUBLE[64] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"Frontend_Loop_Counts = DOUBLE : 0",\
"Frontend_Loop_TDC = DOUBLE : 0",\
"Poll_Counts = DOUBLE : 1",\
"ReadCounts = DOUBLE : 0",\
"TDCErrorCounts = DOUBLE : 0",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define RATE_BANK_DEFINED

typedef struct {
  INT       ip;
  INT       clock;
  INT       mcounter;
  INT       posrate[29];
  INT       mucool;
} RATE_BANK;

#define RATE_BANK_STR(_name) const char *_name[] = {\
"[.]",\
"Ip = INT : 0",\
"Clock = INT : 0",\
"Mcounter = INT : 0",\
"PosRate = INT[29] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"MuCool = INT : 0",\
"",\
NULL }

#define SRAT_BANK_DEFINED

typedef struct {
  INT       ip;
  INT       clock;
  INT       mcounter;
  INT       posrate[29];
  INT       mucool;
} SRAT_BANK;

#define SRAT_BANK_STR(_name) const char *_name[] = {\
"[.]",\
"Ip = INT : 0",\
"Clock = INT : 0",\
"Mcounter = INT : 0",\
"PosRate = INT[29] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"MuCool = INT : 0",\
"",\
NULL }

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 511",\
"Period = INT : 3000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lem01",\
"Frontend name = STRING : [32] VME_FE",\
"Frontend file name = STRING : [256] vme_fe.c",\
"Status = STRING : [256] VME_FE@lem01",\
"Status color = STRING : [32] #00FF00",\
"Hidden = BOOL : n",\
"",\
NULL }

#define SCALER_SETTINGS_DEFINED

typedef struct {
  INT       input_mode_3;
  INT       reference_ch1;
  char      names_scl0[32][32];
  char      names_ssum[32][32];
} SCALER_SETTINGS;

#define SCALER_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"input_mode_3 = INT : 0",\
"reference_ch1 = INT : 0",\
"Names SCL0 = STRING[32] :",\
"[32] Ip",\
"[32] 10kHzclock",\
"[32] MCounter",\
"[32] Pos01",\
"[32] Pos02",\
"[32] Pos03",\
"[32] Pos04",\
"[32] Pos05",\
"[32] Pos06",\
"[32] Pos07",\
"[32] Pos08",\
"[32] Pos09",\
"[32] Pos10",\
"[32] Pos11",\
"[32] Pos12",\
"[32] Pos13",\
"[32] Pos14",\
"[32] Pos15",\
"[32] Pos16",\
"[32] Pos17",\
"[32] Pos18",\
"[32] Pos19",\
"[32] Pos20",\
"[32] Pos21",\
"[32] Pos22",\
"[32] Pos23",\
"[32] Pos24",\
"[32] Pos25",\
"[32] Pos26",\
"[32] Pos27",\
"[32] Pos28",\
"[32] Pos29",\
"Names SSUM = STRING[32] :",\
"[32] Ip",\
"[32] 10kHzclock",\
"[32] MCounter",\
"[32] Pos01",\
"[32] Pos02",\
"[32] Pos03",\
"[32] Pos04",\
"[32] Pos05",\
"[32] Pos06",\
"[32] Pos07",\
"[32] Pos08",\
"[32] Pos09",\
"[32] Pos10",\
"[32] Pos11",\
"[32] Pos12",\
"[32] Pos13",\
"[32] Pos14",\
"[32] Pos15",\
"[32] Pos16",\
"[32] Pos17",\
"[32] Pos18",\
"[32] Pos19",\
"[32] Pos20",\
"[32] Pos21",\
"[32] Pos22",\
"[32] Pos23",\
"[32] Pos24",\
"[32] Pos25",\
"[32] Pos26",\
"[32] Pos27",\
"[32] Pos28",\
"[32] Pos29",\
"",\
NULL }

#endif

