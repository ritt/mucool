/********************************************************************\

  Name:         scaler_rate_sum.c
  Created by:   Thomas Prokscha, use scaler.c module of sample experiment
                by Stefan Ritt as template.

  Contents:     sis3820 scaler handling module. It looks
                for  SCL0 bank and accumulates scalers into an
                SSUM bank; rates are written to RATE bank.

  $Id$

\********************************************************************/

/*-- Include files -------------------------------------------------*/

/* standard includes */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

/* midas includes */
#include "midas.h"
#include "experim.h"
#include "mucool_experim.h"

/*-- ODB Parameters ------------------------------------------------*/
extern RUNINFO          runinfo;
extern EXP_PARAM        exp_param;
extern SCALER_SETTINGS  scaler_settings;
SCALERSUMRATE_PARAM     scaler_param;
SCALERSUMRATE_PARAM_STR(scaler_param_str);

/*-- Module declaration --------------------------------------------*/
INT scaler_init(void);
INT scaler_sum(EVENT_HEADER*,void*);
INT scaler_clear(INT run_number);
INT scaler_eor(INT run_number);

ANA_MODULE scaler_rate_sum = {
  "ScalerSumRate",               /* module name           */
  "Thomas Prokscha",             /* author                */
  scaler_sum,                    /* event routine         */
  scaler_clear,                  /* BOR routine           */
  scaler_eor,                    /* EOR routine           */
  scaler_init,                   /* init routine          */
  NULL,                          /* exit routine          */
  &scaler_param,                 /* parameter structure   */
  sizeof(scaler_param),          /* structure size        */
  (char **)scaler_param_str,     /* initial parameters    */
};

/*-- accumulated scalers -------------------------------------------*/
static double   scaler[N_SCALER];
static INT      rate[N_SCALER];  /* rates */
static double   last_mucool_events, mucool_events;

/*-- for mean Ip calculation --*/
static INT      *pring_buffer, *pwrite_ring_buffer;
static INT      nwrite_ring_buffer, ring_buffer_size;

/* to print scaler Totals and Rates in Analyzer window */
void disp_scaler(INT nscal);

/*-- INIT routine --------------------------------------------------*/

INT scaler_init(void)
{
  INT   nscaler, status, size;
  HNDLE hDB, hkey;

  cm_get_experiment_database(&hDB, NULL);

  nscaler = N_SCALER;
  status = db_find_key(hDB, 0, "/Equipment/Scaler/Variables/SCL0", &hkey);
  if (status == DB_SUCCESS )
        db_set_num_values(hDB, hkey, nscaler);
  else
        cm_msg(MERROR, "scaler_init", "Error setting array length of SCL0.");

  status = db_find_key(hDB, 0, "/Equipment/Scaler/Variables/SSUM", &hkey);
  if (status == DB_SUCCESS )
        db_set_num_values(hDB, hkey, nscaler);
  else
        cm_msg(MERROR, "scaler_init", "Error setting array length of SSUM.");

  size = sizeof(mucool_events);
  db_get_value(hDB, 0, "/Equipment/Trigger/VME_Statistics/SlowMuonEvents", &mucool_events, &size, TID_DOUBLE, FALSE);
  last_mucool_events = mucool_events;

  //setting up ring buffer for mean Ip rate calculation
  if (scaler_param.n_rate_average <= 0)
    ring_buffer_size = 100;
  else
    ring_buffer_size = scaler_param.n_rate_average;

  pring_buffer      = (INT*)malloc(ring_buffer_size*sizeof(INT));
  pwrite_ring_buffer = &pring_buffer[0];
  nwrite_ring_buffer = 0;
  memset(pring_buffer, 0x00, sizeof(pring_buffer));

  return SUCCESS;
}

/*-- BOR routine ---------------------------------------------------*/

INT scaler_clear(INT run_number)
{
 HNDLE hDB;
 INT   size;

 cm_get_experiment_database(&hDB, NULL);

 memset(scaler, 0, sizeof(scaler));
 memset(rate, 0, sizeof(rate));

 size = sizeof(mucool_events);
 db_get_value(hDB, 0, "/Equipment/Trigger/VME_Statistics/MuonEvents", &mucool_events, &size, TID_DOUBLE, FALSE);
 last_mucool_events = mucool_events;

 return SUCCESS;
}

/*-- EOR routine ---------------------------------------------------*/

INT scaler_eor(INT run_number)
{
  return SUCCESS;
}

/*-- event routine -------------------------------------------------*/
/*!
 * <p> Function <b>scaler_sum</b>:
 *
 * <p> locates SCL0 bank, accumulates SCL0 in case of an active
 *     run and calculates the rates.
*/
INT scaler_sum(EVENT_HEADER *pheader, void *pevent)
{
HNDLE     hDB;
INT       n, i, size, sum_Ip_rate, mean_Ip_rate, *pmean_rate;
double    norm=1.;
DWORD     *psclr, mask_scaler[N_SCALER];
double    *psum;
INT       *pd, *psratio;
RATE_BANK *prate;
SRAT_BANK *psrat;
double    new_mucool_events;

  cm_get_experiment_database(&hDB, NULL);

  /* look for SCL0 bank */
  n = bk_locate(pevent, "SCL0", &psclr);
  if (n == 0 || n > N_SCALER)
    return 1;

  for ( i=0; i<n; i++){
      if ( scaler_settings.input_mode_3 )
          mask_scaler[i] = psclr[i] & MASK24;
      else
          mask_scaler[i] = psclr[i];
  }

  /* --- create bank of scaler sums  --- */
  bk_create(pevent, "SSUM", TID_DOUBLE, (void**)&psum);

  for (i=0 ; i<n ; i++){
    if (runinfo.state == STATE_RUNNING ){
        scaler[i] += mask_scaler[i];
    }
    psum[i] = scaler[i];
  }

  bk_close(pevent, psum+n);

  /* --- create rate bank and calculate rates --- */
  bk_create(pevent, "RATE", TID_STRUCT, (void**)&prate);

  pd = &prate->ip;  // copy address of first element of structure to pointer pd
                           // this is useful if the structure contains n elements of same type,
                           // i.e. (double) in our case. If element names should be different
                           // then this offers a simple way to assign values to the elements
                           // without explicitly entering the element name.
                           // This will work with the statement
                           // *(pd+i) = rate[i]; see below
  for (i=0 ; i<n ; i++){
#ifdef HAVE_TEST_RUN
    norm = 10.;
    if ( i == scaler_param.channel_norm_time )
        rate[i] = 1000;
    else
        rate[i] = (int) rint(mask_scaler[i]/norm);
#else
    if ( mask_scaler[scaler_param.channel_norm_time] > 0 ){
        norm = (double)mask_scaler[scaler_param.channel_norm_time]/(double)scaler_param.rate_norm_time;
        rate[i] = (int) rint(mask_scaler[i]/norm);
    }
    else
        rate[i] = -1;
#endif
    //prate->rat0[i] = rate[i];
    *(pd+i) = rate[i];      // increment pointer address by the size of one double, assign
                            // value of rate[i] to new address,
                            // which is the ith entry of the structure prate
  }

  // get MuCool events from VME_STATS key in ODB
  new_mucool_events = 0.;
  size = sizeof(mucool_events);
  db_get_value(hDB, 0, "/Equipment/Trigger/VME_Statistics/MuonEvents", &mucool_events, &size, TID_DOUBLE, FALSE);
  new_mucool_events  = mucool_events - last_mucool_events;
  last_mucool_events = mucool_events;
  if (new_mucool_events < 0.) new_mucool_events = 0.; // this may happen at BOR
  if ( mask_scaler[scaler_param.channel_norm_time] > 0 )
    rate[i] = (int) rint(new_mucool_events/norm);
  else
    rate[i] = -1;
  *(pd+i) = rate[i];

  bk_close(pevent, prate+1);

/*----------------------------------------------------------------
        calculate scaler ratios
  ----------------------------------------------------------------
*/
  /* create calculated bank */
  bk_create(pevent, "SRAT", TID_STRUCT, (void**)&psrat);
  psratio = &psrat->ip;  // copy address of first element of structure to pointer psratio

  /*---- perform calculations --------------------------------------*/
  if  ( pd[scaler_param.ip_channel] > 10000 ){
//      psrat->ch1_ip   = IpSCALE * (float) prate->rat0[0] / (float) prate->rat0[scaler_analysis_param.ip_channel];
//      psrat->ch1_ip   = IpSCALE * (float) pd[0] / (float) pd[scaler_param.ip_channel];
    for (i=0; i<n+1; i++)
     *(psratio+i) = (int) rint(IpSCALE * (float) pd[i] / (float) pd[scaler_param.ip_channel]);
  }
  else{
    for (i=0; i<n+1; i++)
        *(psratio+i) = 0;
  }

  bk_close(pevent, psrat+1);

  //fill ring buffer for Ip
  if (pwrite_ring_buffer < &pring_buffer[ring_buffer_size])
    *pwrite_ring_buffer++ = rate[scaler_param.ip_channel];
  else{
    pwrite_ring_buffer = &pring_buffer[0];
    *pwrite_ring_buffer++ = rate[scaler_param.ip_channel];
  }
  if (nwrite_ring_buffer < ring_buffer_size)
    nwrite_ring_buffer++;

  //calculate mean Ip rate
  sum_Ip_rate = 0;
  for (i=0; i< ring_buffer_size; i++)
   sum_Ip_rate += pring_buffer[i];

  if (nwrite_ring_buffer < ring_buffer_size)
   mean_Ip_rate = sum_Ip_rate/nwrite_ring_buffer;
  else
   mean_Ip_rate = sum_Ip_rate/ring_buffer_size;

  // --- create bank and fill mean rates ---
  bk_create(pevent, "RAAV", TID_INT, (void**)&pmean_rate);
  *pmean_rate = mean_Ip_rate;
  bk_close(pevent, pmean_rate+1);

  //display scalers in terminal
  if ( runinfo.online_mode == 1)
        disp_scaler(N_SCALER);

  return SUCCESS;
}

/* --------------------------------------------------------------------------

        function to display scalers in analyzer info

   --------------------------------------------------------------------------
*/
void disp_scaler(INT nscal)
{
HNDLE  hDB, hKey;
const char   *state[] = {"", "Stopped", "Paused", "Running" };
const char   *line[]  =
{"--------------------------------------------------------------------------"};
const char   *empty[] =
{"                                                                          "};
const char   *top[]   =
{" #  Label                              Total                    Rate/s       "};
time_t  now, difftime;
INT     i;
//double  norm;
char    timestr[32];

  ss_clear_screen();
  ss_printf(0, 0, "%s", line[0]);

  ss_printf(0,1, " Run Number: %10d               started at: %s",
           runinfo.run_number, runinfo.start_time);

  if ( runinfo.state == STATE_STOPPED){
        ss_printf(0,2, " Run State : %10s              last update: %s",
        state[runinfo.state], runinfo.stop_time);
  }
  else{
        time(&now);
        ss_printf(0,2, " Run State : %10s              last update: %s",
        state[runinfo.state], ctime(&now));
  }
  ss_printf(0,3, "%s", empty[0]);
  ss_printf(0,4, "%s", top[0]);
  ss_printf(0,5, "%s", line[0]);
  ss_printf(0,6, "%s", empty[0]);

  for (i=0 ; i<nscal ; i++){
        if ( scaler[i] > 1e7 )
          ss_printf(0, i+7, "%2d  %-24s %15.5e\t\t %13d", i,
                              scaler_settings.names_scl0[i], scaler[i], rate[i]);
//        ss_printf(0, i+7, "%2d  %-24s 0x%08x\t\t 0x%08x", i,
//                    scaler_settings.names_scl0[i], scaler[i], rate[i]);

        else
          ss_printf(0, i+7, "%2d  %-24s %15.0f\t\t %13d", i,
                    scaler_settings.names_scl0[i], scaler[i], rate[i]);
  }
  ss_printf(0, 47, "%s", line[0]);
  if (runinfo.state == STATE_STOPPED)
        difftime = runinfo.stop_time_binary - runinfo.start_time_binary;
  else
        difftime = now - runinfo.start_time_binary;

  ss_printf(0, 49, "Elapsed time: %dh%02dm%02ds",
            difftime/3600, difftime%3600/60,difftime%60);

  /* -------------------------------------------------

        update "/Experiment/Run Parameters/Scaler Update Time"; this is used
        for Scaler display via mhttpd

     -------------------------------------------------
  */
   cm_get_experiment_database(&hDB, NULL);
   if ( db_find_key(hDB, 0, "/Experiment/Run Parameters/Scaler Update time", &hKey) != DB_SUCCESS ){
        if ( db_create_key(hDB, 0, "/Experiment/Run Parameters/Scaler Update time", TID_STRING) != DB_SUCCESS ){
          cm_msg(MERROR, "display_scaler", "Cannot create \"/Experiment/Run Parameters/Scaler Update time\" key in ODB");
          return;
        }
   }
   time(&now);
   timestr[0] = 0;
   sprintf(timestr, "%s", ctime(&now));
   timestr[24] = 0; /* delete new line \n */
   db_set_value(hDB, 0, "/Experiment/Run Parameters/Scaler Update time", &timestr, sizeof(timestr), 1, TID_STRING);

  return;
}
/* ----------------------------------------------------------------------------

        EOF scaler_rate_sum.c

   ----------------------------------------------------------------------------
*/
