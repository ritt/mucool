#####################################################################
#
#  Name:         	Makefile
#  Created by:   	Stefan Ritt
#  Modified for LEM by:	Thomas Prokscha, Andreas Suter
#  Contents:     	Makefile for MIDAS frontend
#
#--------------------------------------------------------------------
# The following lines contain specific switches for different UNIX
# systems. Find the one which matches your OS and outcomment the
# lines below.
#-----------------------------------------
# This is for OSF1
#LIBS = -lbsd -lzlib
#OSFLAGS = -DOS_OSF1 -Dextname
#FF = f77
#FFLAGS = -nofor_main -D 40000000 -T 20000000

#-----------------------------------------
# This is for Linux
LIBS = -lbsd -lm -lutil -lrt -lpthread -lz
OSFLAGS = -DOS_LINUX
# for f2c, uncomment following:
#LIBS = -lbsd -lm -lutil /usr/lib/libf2c.a
#FF = cc
# for egcs g77, use this
FF = g77

#-------------------------------------------------------------------
# The following lines define direcories. Adjust if necessary
#
MIDAS_WORK      = /home/MuCool/nemu/midas
MIDAS_PREFIX    = /usr/local
MIDASSYS        = /home/MuCool/midas
MSCB_DIR        = /home/MuCool/mscb
MXML_DIR        = /home/MuCool/mxml
BIN_DIR         = $(MIDAS_WORK)/bin
INC_DIR 	= $(MIDAS_PREFIX)/include
LIB_DIR 	= $(MIDAS_PREFIX)/lib
DRV_DIR	        = $(MIDASSYS)/drivers
LEM_DEV_DRV     = $(MIDAS_WORK)/drivers/device
LEM_BUS_DRV     = $(MIDAS_WORK)/drivers/bus
LEM_CLASS_DRV   = $(MIDAS_WORK)/drivers/class
LEM_DIV_DRV     = $(MIDAS_WORK)/drivers/divers
SIS3100_VER     = V2.13-9
SIS3100_DIR     = $(MIDAS_WORK)/drivers/kernel/sis1100/$(SIS3100_VER)
SIS3100_CALLS_DIR = $(SIS3100_DIR)/sis3100_calls
LEM_KERNEL_DRV  = $(MIDAS_WORK)/drivers/kernel
LEM_INC_DIR     = $(MIDAS_WORK)/experiment/nemu
#
KERNVER := $(shell uname -r)
KERNEL_INC_DIR  = -I/lib/modules/$(KERNVER)/build/
#-------------------------------------------------------------------
# Drivers needed by the frontend program
#
ifdef TestFrontend
DRIVERS = sis3820.o
SIS3100_DRIVERS = sis3100_vme_calls.o sharc_utils.o
CFLAGS += -DHAVE_TEST_RUN
else
DRIVERS   = sis3820.o
SIS3100_DRIVERS = sis3100_vme_calls.o sharc_utils.o
endif

ifdef TESTOUT
CFLAGS += -DTESTOUT
endif

#-----------------------------------------
# ROOT flags and libs
#
ifdef ROOTSYS
ROOTCFLAGS := $(shell  $(ROOTSYS)/bin/root-config --cflags)
ROOTCFLAGS += -DUSE_ROOT -I$(HOME)/analysis/root/classes
ROOTLIBS   := $(shell  $(ROOTSYS)/bin/root-config --libs)
ROOTLIBS   += -lThread -lTMusrRunHeader
else
CERNLIB_DIR = $(CERN_ROOT)/lib
CFLAGS    += -DHAVE_HBOOK
endif
#
#-------------------------------------------------------------------
# List of analyzer modules
#
MODULES 	= scaler_rate_sum.o decay_ana_module.o
#-------------------------------------------------------------------
####################################################################
# Lines below here should not be edited
####################################################################

# MIDAS library
LIB = $(LIB_DIR)/libmidas.a
ULIB = libsis3100.a

# compiler
CC = cc
GCC = gcc
CFLAGS += -g -I$(INC_DIR) -I$(DRV_DIR) -I$(LEM_DEV_DRV) -I$(LEM_BUS_DRV) -I$(LEM_CLASS_DRV) -I$(LEM_DIV_DRV) -I$(SIS3100_CALLS_DIR) -I$(LEM_KERNEL_DRV) -I$(LEM_INC_DIR) -I$(MSCB_DIR)

SIS_WFLAGS := -Wstrict-prototypes -Wmissing-prototypes \
              -Wmissing-declarations -Wimplicit -Wreturn-type -Wunused \
              -Wcomment -Wformat
SIS_CFLAGS := -I.. -I$(SIS3100_DIR) -g -ansi $(SIS_WFLAGS)

LDFLAGS +=

UFE = vme_fe

all: $(UFE) $(ULIB) analyzer sc_fe write_summary

$(UFE): $(LIB) $(ULIB) $(LIB_DIR)/mfe.o $(DRIVERS) $(UFE).c
	$(CC) $(CFLAGS) $(KERNEL_INC_DIR) $(OSFLAGS) -o $(BIN_DIR)/$(UFE) $(UFE).c \
	$(DRIVERS) $(LIB_DIR)/mfe.o $(ULIB) $(LIB) $(LDFEFLAGS) $(LIBS)

$(ULIB): $(SIS3100_DRIVERS)
	ar cr $@ $^

sc_fe: $(LIB) sc_fe.c
	$(CC) $(CFLAGS) $(OSFLAGS) -I$(MXML_DIR) -o $(MIDAS_WORK)/bin/sc_fe $(MSCB_DIR)/mscb.c sc_fe.c \
	$(MIDASSYS)/drivers/class/hv.c $(MIDASSYS)/drivers/class/multi.c \
	$(MIDASSYS)/drivers/device/mscbdev.c $(MIDASSYS)/drivers/device/mscbhvr.c $(LIB_DIR)/mfe.o \
	$(LIB) $(LDFLAGS) $(LIBS)

sis3100_vme_calls.o: $(SIS3100_CALLS_DIR)/sis3100_vme_calls.c
	$(GCC) $(SIS_CFLAGS) -c $^

sharc_utils.o: $(SIS3100_CALLS_DIR)/sharc_utils.c
	$(CC) $(SIS_CFLAGS) -c $^

sis3820.o: $(LEM_DIV_DRV)/sis3820.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@

camacnul.o: $(DRV_DIR)/bus/camacnul.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@

write_summary: $(LIB) write_summary.c
	$(CC) $(CFLAGS) $(OSFLAGS) -o $(MIDAS_WORK)/bin/write_summary write_summary.c \
	$(LIB) $(LDFLAGS) $(LIBS)

ifdef ROOTSYS
analyzer: $(LIB) $(LIB_DIR)/rmana.o analyzer.o $(MODULES)
	$(CXX) $(FFLAGS) -o $(BIN_DIR)/mucool_analyzer $(LIB_DIR)/rmana.o analyzer.o $(MODULES) \
	$(LIB) $(LDFLAGS) $(ROOTLIBS) $(LIBS)

%.o: %.c
	$(CXX) $(USERFLAGS) $(ROOTCFLAGS) $(CFLAGS) $(OSFLAGS) -o $@ -c $<
else
analyzer: $(LIB) $(LIB_DIR)/mana.o analyzer.o $(MODULES)
	$(FF) $(FFLAGS) -o $(BIN_DIR)/mucool_analyzer $(LIB_DIR)/hmana.o \
	analyzer.o $(MODULES) $(CERNLIB_DIR)/libpacklib.a \
	$(LIB) $(LDFLAGS) $(LIBS)
%.o: %.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $<
endif

clean:
	rm -f *.o *~ \#* .#* libsis3100.a

