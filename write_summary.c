/********************************************************************\

  midas/experiment/nemu/write_summary.c

  Name:         write_summary.c
  Created by:   Thomas Prokscha
  Date:         21-Oct-2005

  Contents:     Program to create Nemu summary file;
                It is called at Start-of-Run and end-of-run.

  $Id$

\********************************************************************/

/* standard includes */
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "midas.h"
#include "msystem.h"

#define MAXLINE                   160
#define N_FUG                      16
#define MODERATOR_HV_CHANNEL        4
#define SAMPLE_HV_CHANNEL          15
#define SPINROTATOR_VOLTAGE_CHANNEL 2
#define SPINROTATOR_CURRENT_CHANNEL 3

// ODB structures
#include "experim.h"

// LEM specific definitions
#include "mucool_experim.h"

#define N_ELEMENTS 6
char *odbElements[] = {"/Runinfo",
                       "/Experiment/Run Parameters",
                       "/Equipment/Trigger/Settings",
                       "/Equipment/Trigger/Vme_Statistics",
                       "/Equipment/Scaler/Settings",
                       "/Analyzer/Parameters/ScalerSumRate",
 };

typedef struct{
 RUNINFO               runinfo;
 EXP_PARAM             exp_param;
 TRIGGER_SETTINGS      trigger_settings;
 VME_STATS             vme_stats;
 SCALER_SETTINGS       scaler_settings;
 SCALERSUMRATE_PARAM   scaler_param;
} ODB;

ODB      odb;
void   *podb;

/* other global variable */
FILE   *file;
char   sumfile[256];
char   line[MAXLINE];
HNDLE  hDB, hKey;

/* function declarations */
INT   open_odb_records(void);
INT   create_sum_filename( char *filename, char *flag );
INT   write_sum_file(char *filename);
void  strcut(char *result, char *orgstr, char *cutstr);
INT   write_trigger(FILE *f);
INT   write_beamline(FILE *f);
INT   write_rates(FILE *f, double norm, double ip_norm, double scaler_sum, char* label);
INT   display_scaler(FILE *f);

/*-----------------------------------------------------------------*/
INT open_odb_records(void)
/* *****************************************************************\

  Name:         open_odb_records
  Author:       Thomas Prokscha
  Date:         09-Nov-2001

  Purpose:      open odb records for later extraction of data to
                be written to the summary file

  Function value:
    DB_SUCCESS              Successful completion
    status                  in case of error: value of db_ function.

\* ****************************************************************** */
{
  INT  status, size, size1=0, i;
  char odbkey[132];

  // get other records
  podb = &odb;
  for (i=0; i < N_ELEMENTS; i++){
    status = db_find_key(hDB, 0, odbElements[i], &hKey);
    if ( status != DB_SUCCESS ){
        cm_msg(MERROR,"open_odb_records", "Could not find %s key.", odbElements[i]);
        return status;
    }
    else{
        switch (i){
         case  0: size = sizeof(RUNINFO); break;
         case  1: size = sizeof(EXP_PARAM); break;
         case  2: size = sizeof(TRIGGER_SETTINGS); break;
         case  3: size = sizeof(VME_STATS); break;
         case  4: size = sizeof(SCALER_SETTINGS); break;
         case  5: size = sizeof(SCALERSUMRATE_PARAM); break;
	}
//	printf("%d %d\n", i, size);
        status = db_get_record(hDB, hKey, podb, &size, 0);
        podb+=size;
	size1+=size;
        if ( status != DB_SUCCESS){
                cm_msg(MERROR,"open_odb_records", "Could not open %s record.", odbElements[i]);
                return status;
        }
    }
  }
/* printf("%d %d\n", sizeof(odb), size1);
 printf("%s\n", odb.trigger_settings.events.comment);
 printf("%s\n", odb.exp_param.comment);
 printf("%f\n", odb.danfysik_spin_rot_event.output[3]);*/
  return DB_SUCCESS;
}

/* -----------------------------------------------------------------*/
INT create_sum_filename( char *filename, char *flag )
/* ***************************************************************** *\

  Name:         create_sum_filename
  Author:       Thomas Prokscha
  Date:         22-May-2001

  Purpose:      create filename for Run summary files of
                the Nemu experiment.
                Runs on Unix and Windows NT

  11-Sep-2001   TP:
                changed filename to ../summ/lem%02d_%04d.summ where
                %02d is replaced by the year modulo 100.

                Look for MIDAS_DATA variable ( == /data/nemu on PC3159),
                and build the filename of the summary file in the form
                MIDAS_DATA/summ/nemu_%04d.summ, where %04d is the four
                digit run number extracted from the ODB.
                If MIDAS_DATA is not defined the HOME path is taken as
                default location for the summary file. In this case a
                message is broadcasted to all MIDAS clients using cm_msg.
                The message is additionally written to the file midas.log
                (located in MIDAS_DATA/log on PC3159).

  Input:        char *flag: if flag == 'N' use current run number from ODB,
                otherwise use run number-1 (the previous run).


  Output:       char *filename, the name of the summary file


  Function value:
    SS_SUCCESS              Successful completion

\* ****************************************************************** */
{
  struct tm *tms;
  time_t now;
  char   dir[256], filebody[256];

  dir[0]   = 0;
  filebody[0] = 0;

  if ( getenv("MIDAS_DATA") ){
        strcpy(dir, getenv("MIDAS_DATA"));
        strcat(dir, DIR_SEPARATOR_STR);
        strcat(dir, "summ");
        strcat(dir, DIR_SEPARATOR_STR);
  }
  else{
#if defined (OS_UNIX)
        strcpy(dir, getenv("HOME"));
#elif defined (OS_WINNT)
        strcpy(dir, getenv("HOMEPATH"));
#endif
        strcat(dir, DIR_SEPARATOR_STR);
        cm_msg(MINFO, "create_sum_filename",
               "MIDAS_DATA path not defined, use home directory %s instead\n", dir);
  }

  time(&now);
  tms = localtime(&now);
  // tms->tm_year yields "Year - 1900"

  strcpy(filename, dir);
  if ( flag == "N")
        sprintf(filebody, "%04d/mucool%02d_%04d.summ", tms->tm_year+1900,
                tms->tm_year%100, odb.runinfo.run_number);
  else
        sprintf(filebody, "%04d/mucool%02d_%04d.summ", tms->tm_year+1900,
                tms->tm_year%100, odb.runinfo.run_number-1);

  strcat(filename, filebody);

  return SS_SUCCESS;
}

/* --------------------------------------------------------- */
INT write_sum_file(char *filename)
/* ***************************************************************** *\

  Name:         write_sum_filename
  Author:       Thomas Prokscha
  Date:         22-May-2001

  Purpose:      writes summary file of Nemu experiment.
                Runs on Unix and (Windows NT, not yet).

  Input:        char *filename, the name of the summary file


  Output:       None


  Function value:
    SS_SUCCESS              Successful completion
    -1                      Error opening new summary file


\* ****************************************************************** */
{
   FILE   *fprevious;
   char   beginComment[] =
          "#BUC---- B e g i n of User Comment ------ Do not edit this line";
   char   endComment[]   =
          "#EUC----   E n d   of User Comment ------ Do not edit this line";
   char   bucpattern[5] = "#BUC";
   char   eucpattern[5] = "#EUC";
   char   pattern[5], underline[MAXLINE], previousfile[256];
   char   cmd[256];
   BOOL   prevfile=FALSE, cont=FALSE, dowrite=FALSE, running=FALSE;
   int    i, len;
   INT    size;

   /* initialise */
   line[0]         = 0;
   pattern[0]      = 0;
   previousfile[0] = 0;
   cmd[0]          = 0;

   /* -----------------------------
      check if file already exists
      (that is we are at end of run...)
      -----------------------------*/
   file = fopen(filename, "r");
   if ( file != NULL){
        fclose(file);
        prevfile = TRUE;
        running  = TRUE;
        sprintf(previousfile,"%s.old", filename);
        sprintf(cmd, "cp %s %s", filename, previousfile);
        system(cmd);
        fprevious = fopen(previousfile, "r");
   }
   else{
   /* -------------------------------
      check if previous run summary file
      exists; if yes copy the user comment
      lines to the new file
      --------------------------------*/
        create_sum_filename(previousfile, "P");
        fprevious = fopen(previousfile, "r");
        if ( fprevious != NULL ){
                prevfile = TRUE;
                running  = FALSE;
        }
        else{
                prevfile = FALSE;
                running  = FALSE;
        }
   }

   /* ------------------------------
      open file in write mode
      ------------------------------*/
   file = fopen(filename, "w");
   if ( file == NULL ){ /* error opening file, return and send error message */
        cm_msg(MERROR, "write_sum_file", "Error opening summary file %s",
               filename);
        return -1;
   }

  /* ------------------------------
      write file header and comments
      ------------------------------*/
   sprintf(line, "%s Run %04d started.\n", odb.runinfo.start_time,
           odb.runinfo.run_number);
   fputs(line, file);
   
   if ( !running )
        sprintf(line, "%s Run %04d stopped.\n\n", odb.runinfo.stop_time,
                odb.runinfo.run_number-1);
   else
        sprintf(line, "%s Run %04d stopped.\n\n", odb.runinfo.stop_time,
                odb.runinfo.run_number);

   fputs(line, file);

   if ( strlen(odb.exp_param.comment) > 132 );
     odb.exp_param.comment[132] = 0;

   sprintf(line, " %s\n", odb.exp_param.comment);
   fputs(line, file);

   len = strlen(line);
   for ( i=0; i<len; i++)
        underline[i] = '=';
   underline[len] = 0;
   sprintf(line, " %s\n\n", underline);
   fputs(line, file);

   sprintf(line, "%s\n", beginComment);
   fputs(line, file);

   if ( prevfile ){
        cont   = TRUE;
        dowrite = FALSE;
        while ( cont ){
          if ( fgets(line, MAXLINE, fprevious) == NULL){
                dowrite = FALSE;
                cont = FALSE;
                break;
          }
          if ( dowrite )
                fputs(line, file);
          for ( i = 0; i<4; i++ )
                pattern[i] = line[i];
          pattern[4] = 0;
          if ( strcmp(pattern, bucpattern) == 0 )
                dowrite = TRUE;
          if ( strcmp(pattern, eucpattern) == 0 )
                cont = FALSE;
        }
        fclose(fprevious);
        if ( running )
                unlink(previousfile);
   }

   if ( !prevfile){
        sprintf(line, "\n\n%s\n\n\n", endComment);
        fputs(line, file);
   }
   else{
        sprintf(line, "\n\n\n");
        fputs(line, file);
   }
/* --------------------------------------------------

   now, header with comments are written, write other
   experiment parameters

   - transport system high voltage
   - trigger settings
   - the beamline settings
   - scaler sum and rates at the end of summary file

   --------------------------------------------------
*/
   write_trigger(file);
//    write_beamline(file);
   display_scaler(file);
/* --------------------------------------------------
   close file and return
   --------------------------------------------------
*/
   fclose(file);
   return SS_SUCCESS;
}
/*---------------------------------------------------------------*/
void strcut(char *result, char *orgstr, char *cutstr)
/*----------------------------------------------------

  Purpose:      look, if cutstr is at the beginning of orgstr.
                if yes, copy orgstr starting after cutstr to
                result. If not, copy orgstr to result.
                quicky version...
  -----------------------------------------------------*/
{
 int    len, j;
 char   tempstr[80];

 len = strlen(cutstr);
 strcpy(tempstr, orgstr);
 tempstr[len] = 0;
 if ( strcmp(tempstr, cutstr) == 0 ){
        j = 0;
        while ( (result[j] = orgstr[j+len]) != '\0')
                j++;
 }
 else
        strcpy(result, orgstr);
 return;
}
/*-----------------------------------------------------------------*/
INT write_trigger(FILE *f)
{
   sprintf(line,"\n ======================   E v e n t  definition   =========================\n\n");

   sprintf(line, " t0 Offset: %d\n", odb.trigger_settings.t0_offset);
   fputs(line, f);

   sprintf(line, "\n Time windows: \n");
   fputs(line,f);
   sprintf(line, " ============= \n");
   fputs(line, f);
   sprintf(line, " Event   2: Data Window: %7d\n",
     odb.trigger_settings.data_window);
   fputs(line,f);

   sprintf(line, "\n Software delays: \n");
   fputs(line,f);
   sprintf(line, " ================ \n");
   fputs(line, f);
   sprintf(line, " Event   2: MCounter    : %7d, e+: %7d\n",
     odb.trigger_settings.mcounter_delay,
     odb.trigger_settings.positron_delay
     );
   fputs(line,f);

   return SS_SUCCESS;
}
/*---------------------------------------------------------------------*/
// INT write_beamline(FILE *f)
// {
//    INT    i, nmax;
//    char   str0[32], str1[32];
//   
//    sprintf(line, "\nBeamline:\n");
//    fputs(line, f);
//    sprintf(line, "=========");
//    fputs(line, f);
//    sprintf(line, "\n Beamline settings: %s\n\n", info.beamline_settings);
//    fputs(line, f);
//    nmax = sizeof(odb.beamline_event.measured)/4;
// /*   nmax = odb.beamline_settings.channels.magnets +
//           odb.beamline_settings.channels.kv61 +
//           odb.beamline_settings.channels.kv62 +
//           odb.beamline_settings.channels.sep61;*/
//    for ( i=0; i<nmax-1; i++){
//         strcpy(str0, odb.beamline_settings.names[i]);
//         strcpy(str1, odb.beamline_settings.names[i+1]);
//         str0[6] = 0; //truncate
//         str1[6] = 0;
//         sprintf(line," %s\t DAC = %5.0f  ADC = %7.3f\t %s\t DAC = %5.0f  ADC = %7.3f\n",
//          str0, odb.beamline_event.demand[i], odb.beamline_event.measured[i],
//          str1, odb.beamline_event.demand[i+1], odb.beamline_event.measured[i+1]);
//         fputs(line, f);
//         i++;
//    }
//    //sprintf(line, "\t\t SEP \t\t  HV = %8.1f kV\n", info.separator.hv);
//    fputs(line, f);
// 
//    return SS_SUCCESS;
// }
// 

INT display_scaler(FILE *f)
/* ***************************************************************** *\

  Name:         display_scaler
  Author:       Thomas Prokscha
  Date:         28-May-2001

  Purpose:      reads the scaler sum and rates from the ODB and
                writes them at the end of the summary file.
                Use void function disp_scaler in $MIDAS_WORK/src/scaler.c
                as template.

  Input:        FILE *f pointer to a file which already must be opened
                in write mode.

  Output:       None


  Function value:
    SS_SUCCESS              Successful completion


\* ****************************************************************** */
{
  double ssum[N_SCALER], rate, rate_perIp, bc_sum;

  INT           scal_ch, ip_ch;
  double        scal_rate, norm, ip_norm, ip_rate, tdc_life;

  char   *state[]     = {"", "Stopped", "Paused", "Running" };
  char   *dashline[]  =
  {"--------------------------------------------------------------------------"};
  char   *top[]   =
  {" #  Label                  Total              Rate/mAs           Rate/s    "};
  time_t        now, difftime;
  INT           i, size;

  /* for calculating the mean rates */
  scal_ch   = odb.scaler_param.channel_norm_time;
  scal_rate = odb.scaler_param.rate_norm_time;
  ip_ch     = odb.scaler_param.ip_channel;
  ip_rate   = IpSCALE;

  sprintf(line, "\n%s\n", dashline[0]);
  fputs(line, f);

  sprintf(line, " Run Number: %10d               started at: %s\n",
           odb.runinfo.run_number, odb.runinfo.start_time);
  fputs(line, f);

  if ( odb.runinfo.state == STATE_STOPPED){
        sprintf(line, " Run State : %10s              last update: %s\n",
         state[odb.runinfo.state], odb.runinfo.stop_time);
  }
  else{
        time(&now);
        sprintf(line, " Run State : %10s              last update: %s\n",
         state[odb.runinfo.state], ctime(&now));
  }
  fputs(line, f);
  sprintf(line, "\n%s\n", top[0]);
  fputs(line, f);
  sprintf(line, "%s\n\n", dashline[0]);
  fputs(line, f);
  db_find_key(hDB, 0, "/Equipment/Scaler/Variables/SSUM", &hKey);
  size = sizeof(ssum);
  db_get_data(hDB, hKey, &ssum, &size, TID_DOUBLE);

  for (i=0 ; i<N_SCALER ; i++){
        //if ( runinfo.state == STATE_STOPPED )
        //{
      if ( ssum[i] > 1e36 || (ssum[i] < 1e-36 && ssum[i] != 0.))
        ssum[i] = -1.;  //something wrong in ODB
        norm    = ssum[scal_ch]/scal_rate;
        ip_norm = ssum[ip_ch]/ip_rate;
        if ( norm > 0. )
                rate = ssum[i]/norm;
        else
                rate = -1.;
        if ( ip_norm > 0. )
                 rate_perIp = ssum[i]/ip_norm;
        else
                 rate_perIp = -1.;

        //}
        if ( ssum[i] > 1e7 )
          sprintf(line, "%2d  %-12s %15.5e\t %13.1f %16.1f\n", i,
                  odb.scaler_settings.names_scl0[i], ssum[i], rate_perIp, rate);
        else
          sprintf(line, "%2d  %-12s %15.0f\t %13.1f %16.1f\n", i,
                  odb.scaler_settings.names_scl0[i], ssum[i], rate_perIp, rate);
        fputs(line, f);
  }

  // vme statistics
  sprintf(line, "--- vme stats ----------\n");
  fputs(line, f);

  write_rates(f, norm, ip_norm, odb.vme_stats.MuonEvents, " MuonEvents");
  write_rates(f, norm, ip_norm, odb.vme_stats.readcounts, " ReadMidasEvents");
  write_rates(f, norm, ip_norm, odb.vme_stats.mcounter_clean,   " Mcounter-clean");
  write_rates(f, norm, ip_norm, odb.vme_stats.mcounter_good,    " Mcounter-good");

  //-------------------------------
  if ( ssum[CLOCK] > 0 )
    tdc_life = (double) odb.vme_stats.channelCounts[CLOCK] / ssum[CLOCK] * 100.;
  else
    tdc_life = -1.;
  sprintf(line, " TDC LifeTime: %17.2f(%)   TDC Error counts: %15.0f\n", tdc_life, odb.vme_stats.tdc_error_counts);
  fputs(line,f);

  sprintf(line, " TDC Clock: %19.0f\n", odb.vme_stats.channelCounts[CLOCK]);
  fputs(line,f);


  sprintf(line, "%s\n", dashline[0]);
  fputs(line, f);

  if (odb.runinfo.state == STATE_STOPPED)
        difftime = odb.runinfo.stop_time_binary - odb.runinfo.start_time_binary;
  else
        difftime = now - odb.runinfo.start_time_binary;

  sprintf(line, "Elapsed time: %dh%02dm%02ds\n",
            difftime/3600, difftime%3600/60,difftime%60);
  fputs(line, f);
  sprintf(line, "\nRates are   a v e r a g e d   rates\n");
  fputs(line, f);

  return SS_SUCCESS;
}
/*-----------------------------------------------------------*/
INT write_rates(FILE *f, double norm, double ip_norm, double scaler_sum, char* label)
/*

*/
{
 double rate, rate_perIp;

 if ( norm > 0. )
   rate = scaler_sum / norm;
 else
   rate = -1.;
 if ( ip_norm > 0. )
   rate_perIp =  scaler_sum / ip_norm;
 else
   rate_perIp = -1.;

 if ( scaler_sum > 1e7 )
   sprintf(line, "%-16s: %14.5e\t %13.1f %16.1f\n",
                 label, scaler_sum, rate_perIp, rate);
  else
   sprintf(line, "%-16s: %14.0f\t %13.1f %16.1f\n",
                 label, scaler_sum, rate_perIp, rate);
 fputs(line, f);

 return SS_SUCCESS;
}
/* ----------------------------------------------------------

   m a i n  program

   ----------------------------------------------------------
*/
main(int argc, char *argv[])
{
  INT    status, i, size;
  char   host_name[HOST_NAME_LENGTH];
  char   expt_name[HOST_NAME_LENGTH];
  char   cmd[256];
  char   printcmd[32];

  /* initialise */
  host_name[0] = 0;
  expt_name[0] = 0;
  cmd[0]       = 0;
  printcmd[0]  = 0;

  /* get default from environment variables */
  cm_get_environment (host_name, sizeof(host_name), expt_name, sizeof(expt_name));

  /* parse command line parameters */
  for ( i=1; i<argc; i++){
        if ( argv[i][0] == '-' ){
                if ( i+1 >= argc || argv[i+1][0] == '-' )
                        goto usage;
                if ( argv[i][1] == 'e' )
                        strcpy(expt_name, argv[i+1]);
                else if ( argv[i][1] == 'h' )
                        strcpy(host_name, argv[i+1]);
                else{
usage:
                        printf("\n usage: write_summary [-h Hostname] [-e Experiment]\n\n");
                        return 1;
                }
        }
  }

/* ----------------
   connect to experiment expt_name on host host_name
   ----------------
*/
  status = cm_connect_experiment(host_name, expt_name, "write_summary", NULL);
  if ( status != CM_SUCCESS)
        return status;

/* -----------------
   connect to ODB
   -----------------
*/
  cm_get_experiment_database(&hDB, NULL);

/* ------------------
   open ODB records
   ------------------
*/
  ss_sleep(500);
  status = open_odb_records();

/* -------------------------------------------------------
   do the run summary job if no odb error was encountered
   -------------------------------------------------------
*/
  if ( status == DB_SUCCESS ){
        create_sum_filename(sumfile, "N");
        write_sum_file(sumfile);
        sprintf(cmd, "emacs %s", sumfile);
        /* here we'll wait until emacs finishes */
//      system(cmd);

/* ------------------
   send summary file to default printer ?
   only for Unix/Linux at the moment
   ------------------
*/
        size = sizeof(printcmd);
        if ( db_get_value(hDB, 0, "/Programs/print summary", &printcmd, &size, TID_STRING, FALSE)
                == DB_SUCCESS && odb.runinfo.state == 1 ){ /* print only when run is stopped */
#ifdef OS_UNIX
                if ( strstr( printcmd, "print") ){
                        sprintf(cmd, "%s %s &", printcmd, sumfile);
                        system(cmd);
                }
#endif
        }
  } /* if ( status == DB_SUCCESS ) */

  cm_disconnect_experiment();

}
