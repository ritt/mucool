/********************************************************************\

  Name:         mucool_experim.h
  Created by:   Thomas Prokscha

  Contents:     MuCool experiment include file for VME frontend and analyzer.

 Copyright (c) 2009-2014 by Paul Scherrer Institut, 5232 Villigen PSI, Switzerland 

\********************************************************************/

//! TDC channel assignments
#define IP          0	// proton current
#define CLOCK       1   // 10kHz clock
#define MCOUNTER    2
#define POS01       3
#define POS02       4
#define POS03       5
#define POS04       6
#define POS05       7
#define POS06       8
#define POS07       9
#define POS08      10
#define POS09      11
#define POS10      12
#define POS11      13
#define POS12      14
#define POS13      15
#define POS14      16
#define POS15      17
#define POS16      18
#define POS17      19
#define POS18      20
#define POS19      21
#define POS20      22
#define POS21      23
#define POS22      24
#define POS23      25
#define POS24      26
#define POS25      27
#define POS26      28
#define POS27      29
#define POS28      30
#define POS29      31

#define N_SCALER_MODULE   32             //!< number of channels of one scaler module
#define N_SCALER          32             //!< total number of scaler channels
#define N_TDC_CHANNELS    64             //!< number of TDC channels
#define N_HITS            128            //!< a single detector may have up to N_HITS hits for event evaluation
#define DATA_N_HITS       64             //!< max. of DATA_N_HITS in data file
#define NEW_EVENT_MASK    (0x7f<<24)
#define TDC_BIT_RANGE     524288         //!< 2^19 is the bit range of the TDC in 100ps and 200ps mode
#define N_DECAY_HISTS     29             //!< number of decay histograms
#define N_OFFSET_PPC_HISTOGRAMS 100      //!< ID offset of postpileup-rejected histograms

#define EVENT_0_TYPE     (1<<0)          //!< EVENT type 0 = BC-MCP1-(e+)
#define EVENT_1_TYPE     (1<<1)          //!< Event type 1 = (BC)-TD-MC2-(e+)
#define EVENT_2_TYPE     (1<<2)          //!< Event type 2 = TD-e+-(BC), LE-muSR
// -- scaling for rates / mA of proton current
#define IpSCALE          100000

/* -- bit masks -- */
#define SIS3820_USER_INPUT_2     0x20000000
#define SIS3820_USER_INPUT_3     0x40000000
#define SIS3820_CH_NUMBER        0x1F000000
#define MASK24                   0x00FFFFFF

typedef unsigned long long U_LONG;       //!< GNU C-Compiler 64bit integer

//! LEM data structure; the time data have 24 bit instead of the 19 bit of the TDC
//! This is to allow the event times to get larger than 2^19.
typedef struct {
 DWORD data:24;
 WORD  channel:7;
 WORD  filler:1;
} LEM_DATA;

// vme statistics structure, needed in vme_fe and write_summary
typedef struct {
 double run_time; //counts the runtime in TDC bins
 int    last_TDCtime;
 double MuonEvents;
 double mcounter_clean;  // no pre-pileup
 double mcounter_good;   // no pre- and post-pileup
 double channelCounts[N_TDC_CHANNELS];
 double frontend_loop_counts;
 double frontend_loop_readtdc;
 double poll_counts;
 double readcounts;
 double tdc_error_counts;
} VME_STATS;

#define VME_STATS_STR(_name) const char *_name[] = {\
"[.]",\
"RunTime = DOUBLE : 0",\
"LastTDCTime = INT : 0",\
"MuonEvents = DOUBLE : 0",\
"Mcounter Clean = DOUBLE : 0",\
"Mcounter Good = DOUBLE : 0",\
"channelCounts = DOUBLE[64] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"Frontend_Loop_Counts = DOUBLE : 0",\
"Frontend_Loop_TDC = DOUBLE : 0",\
"Poll_Counts = DOUBLE : 0",\
"ReadCounts = DOUBLE : 0",\
"TDCErrorCounts = DOUBLE : 0",\
"",\
NULL }
